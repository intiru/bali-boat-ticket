<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
function datepicker()
{
	$datepicker = "
        <script type=\"text/javascript\" src=\"".base_url()."assets/plugin/datepicker_zebra/js/zebra_datepicker.js\"></script>
        <script type=\"text/javascript\" src=\"".base_url()."assets/plugin/datepicker_zebra/js/core.js\"></script>
        <link rel=\"stylesheet\" href=\"".base_url()."assets/plugin/datepicker_zebra/css/metallic.css\" type=\"text/css\">
	";
	
	return $datepicker;
}