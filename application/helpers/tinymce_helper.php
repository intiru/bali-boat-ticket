<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function tinymce()
{
    $tinymce = "
	<script src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce_popup.js\"></script>
<script type=\"text/javascript\" src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>
<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>";
    return $tinymce;
}

function tinymce_all()
{
    $tinymce = "
	<script src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce_popup.js\"></script>
<script type=\"text/javascript\" src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>
<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>";
    return $tinymce;
}

function tinymce_2()
{
    $tinymce = "
	<script src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce_popup.js\"></script>
<script type=\"text/javascript\" src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>
<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>


<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_2\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>";
    return $tinymce;
}


function tinymce_3()
{
    $tinymce = "
	<script src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce_popup.js\"></script>
<script type=\"text/javascript\" src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>
<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>


<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_2\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>


<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_3\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>";
    return $tinymce;
}


function tinymce_5()
{
    $tinymce = "
	<script src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce_popup.js\"></script>
<script type=\"text/javascript\" src=\"" . base_url() . "assets/plugin/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>
<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>


<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_2\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>


<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_3\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>

<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_4\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>

<script language=\"javascript\" type=\"text/javascript\">
		tinyMCE.init({
			mode : \"exact\",
			elements : \"tinymce_5\",
			theme : \"advanced\",
			relative_urls : false,
			remove_script_host : false,
			convert_urls : false,
			file_browser_callback : \"myFileBrowser\",
			plugins : \"advimage,advlink,media,contextmenu,fullscreen,lists,style,layer,table,save,advhr,emotions,iespell,insertdatetime,preview,searchreplace,print,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,autosave,template\",
			theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
		theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
		theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
		theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
		theme_advanced_fonts : \"Andale Mono=andale mono,times;\"+
                \"Arial=arial,helvetica,sans-serif;\"+
                \"Arial Black=arial black,avant garde;\"+
                \"Book Antiqua=book antiqua,palatino;\"+
                \"Comic Sans MS=comic sans ms,sans-serif;\"+
                \"Courier New=courier new,courier;\"+
                \"Georgia=georgia,palatino;\"+
                \"Helvetica=helvetica;\"+
                \"Impact=impact,chicago;\"+
				\"PT Sans Narrow=sans narrow\"+
                \"Symbol=symbol;\"+
                \"Tahoma=tahoma,arial,helvetica,sans-serif;\"+
                \"Terminal=terminal,monaco;\"+
                \"Times New Roman=times new roman,times;\"+
                \"Trebuchet MS=trebuchet ms,geneva;\"+
                \"Verdana=verdana,geneva;\"+
                \"Webdings=webdings;\"+
                \"Wingdings=wingdings,zapf dingbats\",

			theme_advanced_toolbar_location : \"top\",
			theme_advanced_toolbar_align : \"left\",
			extended_valid_elements : \"hr[class|width|size|noshade]\",
			file_browser_callback : \"ajaxfilemanager\",
			theme_advanced_statusbar_location : \"bottom\",
			paste_use_dialog : true,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true,
			
			content_css : \"css/word.css\",
			
			template_external_list_url : \"lists/template_list.js\",
			external_link_list_url : \"lists/link_list.js\",
			external_image_list_url : \"lists/image_list.js\",
			media_external_list_url : \"lists/media_list.js\",
			
			template_replace_values : {
			username : \"Some User\",
			staffid : \"991234\"
		}
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\";
			switch (type) {
				case \"image\":
					break;
				case \"media\":
					break;
				case \"flash\": 
					break;
				case \"file\":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: \"" . base_url() . "/assets/plugin/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php\",
                width: 1200,
                height: 500,
                inline : \"yes\",
                close_previous : \"no\"
            },{
                window : win,
                input : field_name
            });
         
		}
</script>

";
    return $tinymce;
}

?>