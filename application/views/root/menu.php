<div id="content-judul">
    <span class="glyphicon glyphicon-th-list"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
    <div class="col-md-12">
        <div id="halaman">
            <?php
            $method = empty($method) ? '' : $method;
            if ($method == 'list') {
                ?>
                <table width="100%" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <td width="6%" align="center">No.</td>
                        <td width="84%">Menu Title</td>
                        <td width="10%" align="center" colspan="3">Menu</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($menu as $row) {
                        ?>
                        <tr id="row<?php echo $no; ?>">
                            <td align="center"><?php echo $no++ . '.'; ?></td>
                            <td><?php echo $row->artikel_title; ?></td>
                            <td align="center">
                                <?php echo anchor('www/menu/edit/' . $row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
                            </td>
                            <td align="center">
                                <?php
                                if ($row->link == 'open_trip') {
                                    echo anchor('www/open_trip', '<span class="glyphicon glyphicon-envelope picture" title="Open Trip" data-toggle="tooltip"></span>');
                                }
                                if ($row->link == 'travel_news') {
                                    echo anchor('www/news', '<span class="glyphicon glyphicon-envelope picture" title="news" data-toggle="tooltip"></span>');
                                }
                                if ($row->link == 'photo') {
                                    echo anchor('www/galeri', '<span class="glyphicon glyphicon-picture picture" title="news" data-toggle="tooltip"></span>');
                                }
                                if ($row->link == 'home1') {
                                    echo anchor('www/photo_profil', '<span class="glyphicon glyphicon-picture picture" title="Photo Profil" data-toggle="tooltip"></span>');
                                }
                                if ($row->link == 'partner_news') {
                                    echo anchor('www/partner_news', '<span class="glyphicon glyphicon-envelope picture" title="news" data-toggle="tooltip"></span>');
                                }
                                if ($row->link == 'team') {
                                    echo anchor('www/team_type', '<span class="glyphicon glyphicon-th picture" title="Team Type" data-toggle="tooltip"></span>');
                                    echo anchor('www/team', '<span class="glyphicon glyphicon-user picture" title="Team List" data-toggle="tooltip"></span>');
                                }
                                if ($row->link == 'activities') {
                                    echo anchor('www/category', '<span class="glyphicon glyphicon-list picture" title="Activities Area" data-toggle="tooltip"></span>');
                                }

                                ?>
                            </td>
                            <td>
                                <?php
                                    if($row->link == 'activities') {
                                        echo anchor('www/category_activities', '<span class="glyphicon glyphicon-list picture" title="Activities Category" data-toggle="tooltip"></span>');
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($this->session->userdata('level') == "Super Administrator") {
                        ?>
                        <tr style="background-color:#FFDFDF;">
                            <td align="center"><?php echo $no++ . '.'; ?></td>
                            <td><?php echo $menu_admin->artikel_title; ?></td>
                            <td align="center">
                                <?php echo anchor('www/menu/edit/' . $menu_admin->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php
            } elseif ($method == 'edit') {
                echo tinymce();
                echo form_open('www/menu/update/' . $edit->artikel_id, array('id' => 'update', 'title' => base_url() . 'www/menu'));
                ?>
                <table width="100%">
                    <tr>
                        <td width="149">Page Title</td>
                        <td width="347"><input type="text" name="title" class="form-control"
                                               value="<?php echo $edit->artikel_title; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td><textarea <?php echo $edit->link != 'connect' ? 'id="tinymce"' : NULL; ?>
                                    class="form-control" name="description" cols="50"
                                    rows="15"><?php echo $edit->artikel_isi; ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Meta Title</td>
                        <td><input type="text" class="form-control" name="meta_title"
                                   value="<?php echo $edit->meta_title; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Meta Description</td>
                        <td><input type="text" class="form-control" name="meta_description"
                                   value="<?php echo $edit->meta_description; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Meta Keywords</td>
                        <td><input type="text" class="form-control" name="meta_keywords"
                                   value="<?php echo $edit->meta_keywords; ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
                            <?php echo anchor('www/menu', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
                <?php
                echo form_close();
            }
            ?>
        </div>
    </div>
</div>