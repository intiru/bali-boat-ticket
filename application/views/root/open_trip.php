<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 

      <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
      <?php
      $method = (empty($method)) ? '' : $method;
//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
      if ($method == 'list') {
        echo anchor('www/open_trip/create', '<span class="glyphicon glyphicon-plus"></span> Add Open Trip', array('class' => 'btn btn-success'));
        ?>
        <br />
        <br />
        <table width="100%" class="table table-striped table-hover table-responsive">
          <thead>
            <tr>
              <td width="4%" align="center">No.</td>
              <td width="14%">Thumbnail</td>
              <td width="30%">Title</td>
              <td>Tour Date</td>
              <td width="18%">Date Modified</td>
              <td width="6%" colspan="3" align="center">Menu</td>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($list as $row) {
              ?>
              <tr  id="row<?php echo $no; ?>">
                <td align="center"><?php echo $no++ . '.'; ?></td>
                <td align="center"><?php if (!empty($row->artikel_gambar)) {
            echo img(array('src' => 'uploaded/content/' . $row->artikel_gambar, 'width' => '80', 'class' => 'img-thumbnail'));
          } ?></td>
                <td><?php echo $row->artikel_title; ?></td>
                <td><?php echo $row->open_trip_date; ?></td>
                <td><?php echo $row->artikel_waktu; ?></td>
                <td>
<?php echo anchor('www/remark_post/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-list-alt picture" title="Remark List" data-toggle="tooltip"></span>'); ?>
                  </td>
                </td>
                <td><?php echo anchor('www/open_trip/edit/' . $row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
                <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/open_trip/delete/<?php echo $row->artikel_id; ?>', '<?php echo $no - 1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php
//-------------------------------------------------------- CREATE ---------------------------------------------//
      } elseif ($method == 'create') {
        echo tinymce_3();
        echo form_open_multipart('www/open_trip/insert', array('id' => 'insert-file', 'title' => base_url() . 'www/open_trip'));
        ?>
        <table width="100%" border="0" >
          <tr>
            <td>Special ?</td>
            <td><label class="radio-inline">
                <input type="radio" name="special" value="no" id="special_0" checked>
                No</label>
              <label  class="radio-inline">
                <input type="radio" name="special" value="yes" id="special_1">
                Yes</label></td>
          </tr>
          <tr>
            <td>Tour Packages</td>
            <td><select name="open_trip_artikel_id" class="form-control" style="width:360px;">
                <?php foreach ($tour_packages as $row) { ?>
                  <option value="<?php echo $row->artikel_id; ?>"><?php echo $row->artikel_title; ?></option>
  <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td></td>
            <td><img src="" id="gambar" /></td>
          </tr>
          <tr>
            <td>Thumbnail</td>
            <td><input type="file" name="userfile" onchange="read_image(this)" /></td>
          </tr>
          <tr>
            <td>Title</td>
            <td><input type="text" name="title" class="form-control" /></td>
          </tr>
          <tr>
            <td>Tour Date</td>
            <td><input type="text" name="open_trip_date" class="datepicker" style="border:1px solid #CCC;border-radius:4px;padding:4px"/></td>
          </tr>
          <tr>
            <td>Description</td>
            <td><textarea name="description" id="tinymce" cols="" rows=""></textarea></td>
          </tr>
          <tr>
            <td>Meta Title</td>
            <td><input type="text" name="meta_title" class="form-control" /></td>
          </tr>
          <tr>
            <td>Meta Description</td>
            <td><input type="text" name="meta_description" class="form-control" /></td>
          </tr>
          <tr>
            <td>Meta Keywords</td>
            <td><input type="text" name="meta_keywords" class="form-control"/></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
  <?php echo anchor('www/open_trip', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
//------------------------------------------------------- EDIT ------------------------------------------//
      } elseif ($method == 'edit') {
        echo tinymce_3();
        echo form_open_multipart('www/open_trip/update/' . $edit->artikel_id, array('id' => 'update-file', 'title' => base_url() . 'www/open_trip'));
        ?>
        <input type="hidden" name="special" value="<?php echo $edit->special; ?>">
        <table width="100%" border="0">
          <tr>
            <td>Special ?</td>
            <td><label class="radio-inline">
                <input type="radio" name="special" value="no" id="special_0" <?php echo $edit->special == 'no' ? 'checked' : NULL; ?>>
                No</label>
              <label  class="radio-inline">
                <input type="radio" name="special" value="yes" id="special_1" <?php echo $edit->special == 'yes' ? 'checked' : NULL; ?>>
                Yes</label></td>
          </tr>
          <tr>
            <td>Tour Packages</td>
            <td><select name="open_trip_artikel_id" id="kategori" class="form-control" style="width:360px;">
                <?php foreach ($tour_packages as $row) { ?>
                  <option value="<?php echo $row->artikel_id; ?>" <?php echo $row->artikel_id == $edit->open_trip_artikel_id ? 'selected="selected"' : NULL; ?>><?php echo $row->artikel_title; ?></option>
  <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td></td>
            <td><img src="<?php echo base_url() . 'uploaded/content/' . $edit->artikel_gambar; ?>" id="gambar" class="img-thumbnail" style="max-width:250px;" /></td>
          </tr>
          <tr>
            <td>Thumbnail</td>
            <td><input type="file" name="userfile" onchange="read_image(this)" /></td>
          </tr>
          <tr>
            <td>Title</td>
            <td><input type="text" name="title" class="form-control" value="<?php echo $edit->artikel_title; ?>" /></td>
          </tr>
          <tr>
            <td>Tour Date</td>
            <td><input type="text" name="open_trip_date" class="datepicker" value="<?php echo $edit->open_trip_date ?>" style="border:1px solid #CCC;border-radius:4px;padding:4px"/></td>
          </tr>
          <tr>
            <td>Description</td>
            <td><textarea name="description" id="tinymce" cols="" rows=""><?php echo $edit->artikel_isi; ?></textarea></td>
          </tr>
          <tr>
            <td>Meta Title</td>
            <td><input type="text" name="meta_title" class="form-control" value="<?php echo $edit->meta_title; ?>"  /></td>
          </tr>
          <tr>
            <td>Meta Description</td>
            <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description; ?>" /></td>
          </tr>
          <tr>
            <td>Meta Keywords</td>
            <td><input type="text" name="meta_keywords" class="form-control" value="<?php echo $edit->meta_keywords; ?>" /></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
  <?php echo anchor('www/open_trip', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
      }
      ?>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/js/zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/js/core.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/css/metallic.css" type="text/css">

<script type="text/javascript">
  $(document).ready(function() {
    
    var kategori_id = $('select[name="kategori"]').val();
    if(kategori_id == 21) {
      $('.star, .spec').hide();
    } else {
      $('.star, .spec').show();
    }
    
    
    $('select[name="kategori"]').change(function() {
      var val = $(this).val();
      if(val == 21) {
        $('.star, .spec').fadeOut();
      } else {
        $('.star, .spec').fadeIn();
      }
    });
  });
</script>