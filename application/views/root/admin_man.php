<div id="content-judul">
	<span class="glyphicon glyphicon-user"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = (empty($method))?'':$method;

// ------------------------------- TUBUH ADMIN MANAJEMEN ---------------------------------------- //

if($method == 'list'){
	echo  anchor('www/admin/create', '<span class="glyphicon glyphicon-plus"></span> Add User', array('class'=>'btn btn-success'));
?>
<br /><br />
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td align="center">No.</td>
    <td>Username</td>
    <td>Email</td>
    <td>Level</td>
    <td colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($admin as $row){
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->username; ?></td>
    <td><?php echo $row->email; ?></td>
    <td><?php echo $row->level; ?></td>
    <td align="center"><?php echo anchor('www/admin/edit/'.$row->username, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
    <td align="center">
    	<span onclick="hapus('<?php base_url(); ?>admin/delete/<?php echo $row->username; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
  	<?php } ?>
</tbody>
</table>
<?php
}elseif($method=='create'){
	echo form_open('www/admin/insert', array('id'=>'admin', 'title'=>base_url().'www/admin'));
?>
<table width="100%" border="0">
  <tr>
    <td width="22%">Username</td>
    <td width="29%">
    	<input type="text" name="username" class="form-control" autofocus>
    </td>
    <td width="47%"><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Password</td>
    <td>
    	<input type="text" name="password" class="form-control">
    </td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Password Confirmation</td>
    <td><input type="password" name="password_conf" class="form-control"></td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Email</td>
    <td>
    	<input type="text" name="email" class="form-control">
    </td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Level</td>
    <td>
    	<select name="level" style="width:200px;" class="form-control">
    	  <option value="Super Administrator">Super Administrator</option>
    	  <option value="Administrator">Administrator</option>
    	</select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
        <?php echo anchor('www/admin', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
<?php	
echo form_close();
}elseif($method=='edit'){
	echo form_open('www/admin/update/'.$this->uri->segment(4), array('id'=>'admin', 'title'=>base_url().'www/admin'));
?>
<table width="100%" border="0">
  <tr>
    <td width="22%">Username</td>
    <td width="29%">
    	<input type="text" name="username" class="form-control" autofocus value="<?php echo $edit->username; ?>">
    </td>
    <td width="47%"><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Password</td>
    <td>
    	<input type="text" name="password" class="form-control" value="<?php echo set_value('password'); ?>">
    </td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Password Confirmation</td>
    <td><input type="password" name="password_conf" class="form-control"></td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Email</td>
    <td>
    	<input type="text" name="email" class="form-control" value="<?php echo $edit->email; ?>">
    </td>
    <td><span id="form_error"></span></td>
  </tr>
<?php
	if($this->session->userdata('level') == "Super Administrator"){
?>
  <tr>
    <td>Level</td>
    <td>
<?php
	$level = array(
				'Super Administrator'=>'Super Administrator',
				'Administrator'=>'Administrator'
			);
	$attr = 'style="width:200px;"  class="form-control"';
	echo form_dropdown('level', $level, $edit->level, $attr);
?>
    </td>
    <td>&nbsp;</td>
  </tr>
<?php } ?>
  <tr>
    <td>&nbsp;</td>
    <td>
    	<button class="btn btn-success"><i class="icon-white icon-ok"></i> Update</button>
<?php
	if($this->session->userdata('level')=="Super Administrator")
		echo anchor('www/admin', '<i class="icon-white icon-share-alt"></i> Cancel', array('class'=>'btn btn-warning'));
	else
		echo anchor('www/home', '<i class="icon-white icon-share-alt"></i> Cancel', array('class'=>'btn btn-warning'));

?>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
<?php
echo form_close();
}
?>
		</div>
	</div>
</div>