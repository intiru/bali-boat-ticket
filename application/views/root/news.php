<div id="content-judul">
    <span class="glyphicon glyphicon-envelope"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
    <div class="col-md-12">
        <div id="halaman">
            <?php
            $method = (empty($method)) ? '' : $method;
            // ------------------------------- TUBUH CATEGORY MANAJEMEN ---------------------------------------- //

            if ($method == 'list') {
                echo anchor('www/news/create', '<span class="glyphicon glyphicon-plus"></span> Add News', array('class' => 'btn btn-success'));
                echo '&nbsp;&nbsp;';
                echo anchor('www/menu', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class' => 'btn btn-warning'));
                ?>
                <br/><br/>
                <table width="100%" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <td width="6%" align="center">No.</td>
                        <td>News Title</td>
                        <td>Thumbnail</td>
                        <td width="25%">Date Modified</td>
                        <td width="8%" colspan="3" align="center">Menu</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($news as $row) {
                        ?>
                        <tr id="row<?php echo $no; ?>">
                            <td align="center"><?php echo $no++ . '.'; ?></td>
                            <td><?php echo $row->artikel_title; ?></td>
                            <td><img src="<?php echo base_url('uploaded/content/'.$row->artikel_gambar) ?>" class="img-thumbnail" width="200"></td>
                            <td><?php echo $row->artikel_waktu; ?></td>
                            <td><?php echo anchor('www/galeri_artikel/list/' . $row->artikel_id . '/NULL/news', '<span class="glyphicon glyphicon-picture picture" title="edit" data-toggle="tooltip"></span>'); ?></td>
                            <td align="center">
                                <?php echo anchor('www/news/edit/' . $row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
                            <td align="center">
                                <span onclick="hapus('<?php echo base_url(); ?>www/news/delete/<?php echo $row->artikel_id; ?>', '<?php echo $no - 1; ?>')"><span
                                            class="glyphicon glyphicon-remove remove" title="delete"
                                            data-toggle="tooltip"></span></span>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>


                <?php

//---------------------------------------------------- CREATE -------------------------------------//

            } elseif ($method == 'create') {
                echo tinymce();
                echo form_open_multipart('www/news/insert', array('id' => 'insert-file', 'title' => base_url() . 'www/news'));
                ?>
                <table width="100%" border="0">
                    <tr>
                        <td width="199">News Title</td>
                        <td width="835"><input type="text" name="title" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><img src="" id="gambar"/></td>
                    </tr>
                    <tr>
                        <td>Thumbnail</td>
                        <td><input type="file" name="userfile" onchange="read_image(this)"/></td>
                    </tr>
                    <tr>
                        <td height="47" valign="top">Content</td>
                        <td><textarea name="content" id="tinymce" cols="" rows=""></textarea></td>
                    </tr>
                    <tr>
                        <td>Meta Title</td>
                        <td><input type="text" name="meta_title" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Meta Description</td>
                        <td><input type="text" name="meta_description" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Meta Keywords</td>
                        <td><input type="text" name="meta_keywords" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td height="40" valign="bottom"></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
                            <?php echo anchor('www/news', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
                <?php
                echo form_close();
            } elseif ($method == 'edit') {

                echo tinymce();
                echo form_open_multipart('www/news/update/' . $edit->artikel_id, array('id' => 'update-file', 'title' => base_url() . 'www/news'));
                ?>
                <table width="100%" border="0">
                    <tr>
                        <td width="200">News Title</td>
                        <td width="834"><input type="text" name="title" class="form-control"
                                               value="<?php echo $edit->artikel_title; ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><img id="gambar" src="<?php echo base_url('uploaded/content/'.$edit->artikel_gambar) ?>" width="300" class="img-thumbnail"/></td>
                    </tr>
                    <tr>
                        <td>Thumbnail</td>
                        <td><input type="file" name="userfile" onchange="read_image(this)"/></td>
                    </tr>
                    <tr>
                        <td height="47" valign="top">Content</td>
                        <td><textarea name="content" id="tinymce" cols=""
                                      rows=""><?php echo $edit->artikel_isi; ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Meta Title</td>
                        <td><input type="text" name="meta_title" class="form-control"
                                   value="<?php echo $edit->meta_title; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Meta Description</td>
                        <td><input type="text" name="meta_description" class="form-control"
                                   value="<?php echo $edit->meta_description; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Meta Keywords</td>
                        <td><input type="text" name="meta_keywords" class="form-control"
                                   value="<?php echo $edit->meta_keywords; ?>"/></td>
                    </tr>
                    <tr>
                        <td height="40" valign="bottom"></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
                            <?php echo anchor('www/news', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
                <?php
                echo form_close();
            }
            ?>
        </div>
    </div>
</div>