<div id="content-judul"> <span class="glyphicon glyphicon-random"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
<!--<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="<?php echo base_url(); ?>www/team">Team Packages</a></li>
    <li><a href="<?php echo base_url(); ?>www/team_porto">Team Portofolio</a></li>
  </ul>
  <Br /><Br />-->
    
      <?php

$method = (empty($method))?'':$method;

// ------------------------------- TUBUH CATEGORY MANAJEMEN ---------------------------------------- //



if($method=='list'){

	echo anchor('www/team/create', '<span class="glyphicon glyphicon-plus"></span> Add Team', array('class'=>'btn btn-success'));
  echo '&nbsp;';
  echo anchor('www/menu', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning')); 

?>
      <br />
      <br />
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td width="6%" align="center">No.</td>
            <td width="28%">Team Type</td>
            <td width="28%">Nama</td>
            <td width="28%">Thumbnail</td>
            <td width="10%" colspan="2" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php
	$no = 1;
	foreach($team as $row){

?>
          <tr id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td><?php echo $row->label; ?></td>
            <td><?php echo $row->nama; ?></td>
            <td><?php echo img(array('src'=>'uploaded/content/'.$row->thumbnail, 'width'=>'100px', 'class'=>'img-thumbnail'))?></td>
            <td align="center"><?php echo anchor('www/team/edit/'.$row->id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/team/delete/<?php echo $row->id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
          </tr>
          <?php
	}
?>
        </tbody>
      </table>
      <?php



//---------------------------------------------------- CREATE -------------------------------------//

	

}

elseif($method=='create')

{

	echo tinymce();

	echo form_open_multipart('www/team/insert', array('id'=>'insert-file', 'title'=>base_url().'www/team', 'class'=>'normal'));

?>
      <table width="100%" border="0">
        <tr class="gambar">
          <td></td>
          <td><img src="" id="gambar"></td>
        </tr>
        <tr class="gambar">
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
        </tr>
        <tr>
          <td width="199">Team Type</td>
          <td width="835">
            <select name="team_type_id" class="form-control">
              <?php foreach($type as $r) { ?>
              <option value="<?php echo $r->id ?>"><?php echo $r->label ?></option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td height="47" valign="top">Team Name</td>
          <td><input type="text" name="nama" class="form-control"></td>
        </tr>
        <tr>
          <td height="47" valign="top">Team Description</td>
          <td><input type="text" name="keterangan" class="form-control"></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/team', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

	echo form_close();

}

elseif($method=='edit')

{

	echo tinymce();

	echo form_open_multipart('www/team/update/'.$edit->id,  array('id'=>'update-file', 'title'=>base_url().'www/team', 'class'=>'normal'));

?>
      <table width="100%" border="0">
        <tr class="gambar">
          <td></td>
          <td><img src="<?php echo base_url(); ?>uploaded/content/<?php echo $edit->thumbnail; ?>" id="gambar" width="150" class="img-thumbnail"></td>
        </tr>
        <tr class="gambar">
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
        </tr>
        <tr>
          <td width="199">Team Type</td>
          <td width="835">
            <select name="team_type_id" class="form-control">
              <?php foreach($type as $r) { ?>
              <option value="<?php echo $r->id ?>" <?php echo $r->id == $edit->team_type_id ? 'selected':'' ?>><?php echo $r->label ?></option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td height="47" valign="top">Team Name</td>
          <td><input type="text" name="nama" class="form-control" value="<?php echo $edit->nama ?>"></td>
        </tr>
        <tr>
          <td height="47" valign="top">Team Description</td>
          <td><input type="text" name="keterangan" class="form-control" value="<?php echo $edit->keterangan ?>"></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/team', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

echo form_close();

}

?>
    </div>
  </div>
</div>
