<!DOCTYPE html>

<html lang="en">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

    <title>Root - Web Management</title>

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/validation/core/admin.css" />

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap_admin.css" />

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.css" />

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png" />

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/js/zebra_datepicker.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/css/default.css">
    <!--<script src="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.6/dist/zebra_datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/zebra_datepicker@latest/dist/css/default/zebra_datepicker.min.css">-->

  </head>

  <body>

    <noscript>

    <div class="noscript-2">

      <div class="noscript">Don't do something bad broo, Please Reactived Your Javascript!!</div>

    </div>

    </noscript>

    <div id="loading"></div>

    <div id="success"><span class="glyphicon glyphicon-ok"></span></div>

    <div id="error"><span class="glyphicon glyphicon-remove"></span></div>

    <div id="status-success"></div>

    <div id="status-error"></div>

    <div class="progress-bar-admin-base progress-bar-admin"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">

      <span id="percent"></span>

    </div>

    <div class="col-md-2 hide" id="sidebar">

      <div id="komputer" onClick="alert('MaWar Admin Desain 2.0.1 5-1-2014')">

        <img src="<?php echo base_url(); ?>assets/img/template/logo-detail.png" /><br /><br />

      </div>

      <div id="goco-bingkai">

        <a href="#" id="collapseAll" class="goco-menu"><span class="glyphicon glyphicon-chevron-up"></span></a>

        <a href="#" id="expandAll" class="goco-menu"><span class="glyphicon glyphicon-chevron-down"></span></a>

        <div id="bersih"></div>

      </div>

      <ul id="demo1" class="nav-goco">

        <li><a href="<?php echo base_url(); ?>www/home"><span class="glyphicon glyphicon-home"></span> Home</a></li>

        <li><a href=""><span class="glyphicon glyphicon-align-center"></span> Posts</a>

          <ul>

            <li><a href="<?php echo base_url(); ?>www/tour/create"><span class="glyphicon glyphicon-asterisk"></span> New Post</a></li>

            <li><a href="<?php echo base_url(); ?>www/tour"><span class="glyphicon glyphicon-align-justify"></span> All Posts</a></li>

            <li><a href="<?php echo base_url(); ?>www/category"><span class="glyphicon glyphicon-random"></span> Manage <!--Top--> Category</a></li><!--

            <li><a href="<?php echo base_url(); ?>www/sub_category"><span class="glyphicon glyphicon-random"></span> Manage Sub Category</a></li>-->

          </ul>

        </li>

        <li><a href="#"><span class="glyphicon glyphicon-globe"></span> Web Connection</a>

          <ul>

            <li><a href="<?php echo base_url(); ?>www/email"><span class="glyphicon glyphicon-cloud-download"></span> Manage Inbox Email</a></li>

            <li><a href="<?php echo base_url(); ?>www/customer"><span class="glyphicon glyphicon-earphone"></span> Manage Customer Service</a></li>

          </ul>

        </li>

        <li><a href=""><span class="glyphicon glyphicon-wrench"></span> Web Configuration</a>

          <ul>

            <li><a href="<?php echo base_url(); ?>www/menu"><span class="glyphicon glyphicon-th-list"></span> Edit Top & Bottom Menu</a></li>

            <li><a href="<?php echo base_url(); ?>www/media"><span class="glyphicon glyphicon-inbox"></span> Manage Media Library</a></li>

            <li><a href="<?php echo base_url(); ?>www/header"><span class="glyphicon glyphicon-picture"></span> Manage Images Header</a></li>

            <li><a href="<?php echo base_url(); ?>www/testimonial"><span class="glyphicon glyphicon-comment"></span> Manage Comment Guest</a></li><!--

            <li><a href="<?php //echo base_url();  ?>www/galeri"><span class="glyphicon glyphicon-picture"></span> Manage Gallery Images</a></li>-->

            <li><a href="<?php echo base_url(); ?>www/others"><span class="glyphicon glyphicon-cog"></span> Others Setting</a></li>

          </ul>

        </li>

        <li><a href="<?php echo base_url(); ?>www/admin"><span class="glyphicon glyphicon-user"></span>

            <?php echo $this->session->userdata('level') == 'Super Administrator' ? 'Manage Admin' : 'Edit Profil' ?></a></li>

      </ul>

    </div>

    <div class="col-md-12" id="content">

      <div id="panel" class="menu-desktop">

          <a href="<?php echo base_url() ?>" class="menu kiri site logo" target="_blank" style="background-color: white">
              <img src="<?php echo base_url() ?>assets/template/images/logo-bali-boat-ticket.png" width="90">
          </a>

       

        <div class="kiri">

          <nav class="navbar navbar-default" style="background-color: rgba(0,0,0,0); border: none">

            <div class="container-fluid">

              <!-- Brand and toggle get grouped for better mobile display -->

              <div class="navbar-header desktop">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

                  MENU

                </button>

              </div>



              <!-- Collect the nav links, forms, and other content for toggling -->

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav">

                  <li><a href="<?php echo base_url('www') ?>">HOME <span class="sr-only">(current)</span></a></li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">POST <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/tour/create">Add New Post</a></li>
                      <li><a href="<?php echo base_url(); ?>www/tour">Manage All Posting</a></li>
                      <!--<li><a href="<?php /*echo base_url(); */?>www/open_trip">Manage Open Trip</a></li>-->
                      <li><a href="<?php echo base_url(); ?>www/category">Manage Categories</a></li>
                      <li><a href="<?php echo base_url(); ?>www/destination">Manage Destination</a></li>
                      <!--<li><a href="<?php /*echo base_url(); */?>www/remark">Manage Remark</a></li>-->

                    </ul>

                  </li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">WEB CONNECTION <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/email">Manage Incoming Mail</a></li>

                      <li><a href="<?php echo base_url(); ?>www/customer">Manage Social Media</a></li>

                    </ul>

                  </li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">WEB CONFIGURATION <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/menu">Edit Top & Bottom Menu</a></li>

                      <li><a href="<?php echo base_url(); ?>www/media">Manage Media Library</a></li>

                      <li><a href="<?php echo base_url(); ?>www/header">Manage Header Sideshow</a></li>

                      <!--<li><a href="<?php /*echo base_url(); */?>www/partners">Manage Boat Partners</a></li>-->

                      <!--<li><a href="<?php /*echo base_url(); */?>www/testimonial">Manage Guest Comment</a></li>-->
                      
                      <!--<li><a href="<?php /*echo base_url(); */?>www/banner">Manage Banner</a></li>-->

                      <li><a href="<?php echo base_url(); ?>www/admin"><?php echo $this->session->userdata('level') == 'Super Administrator' ? 'Manage Admin' : 'Edit Profil' ?></a></li><!--

                      <li><a href="<?php //echo base_url();  ?>www/galeri"><span class="glyphicon glyphicon-picture"></span> Manage Gallery Images</a></li>-->

                      <li><a href="<?php echo base_url(); ?>www/others">Others Setting</a></li>

                    </ul>

                  </li>
                  
<!--                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">REPORT <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php /*echo base_url(); */?>www/booking/listall">All Bookings</a></li>
                        <li><a href="<?php /*echo base_url(); */?>www/boat_contact">Boat Contact</a></li>
                    </ul>
                  </li>-->
                  
                </ul>

              </div><!-- /.navbar-collapse -->

            </div><!-- /.container-fluid -->

          </nav>

        </div>

        <?php echo anchor('www/logout', '<span class="glyphicon glyphicon-off"></span> Sign Out', array('class' => 'menu kanan logout')); ?>

        <div class="menu kanan user dashboard-user"><span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata['username'] . " Dashboard"; ?></div>

       	<div id="bersih"></div>

      </div>

      

      <div id="panel" class="menu-mobile">

          <nav class="navbar navbar-default" style="background-color: rgba(0,0,0,0); border: none;width: 100% !important">

            <div class="container-fluid" style="">

              <!-- Brand and toggle get grouped for better mobile display -->

              <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false" style="color: white; margin-top: 12px;">

                  Menu

                </button>

                <a class="navbar-brand" href="<?php echo base_url('www') ?>" style="margin-top: -6px;">

                  <img src="<?php echo base_url('assets/template/images/logo-bali-boat-ticket.png'); ?>" height="50">

                </a>

              </div>



              <!-- Collect the nav links, forms, and other content for toggling -->

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                <ul class="nav navbar-nav">

                  <li><a href="<?php echo base_url('www') ?>">HOME <span class="sr-only">(current)</span></a></li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">POST <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/tour/create">Add New Post</a></li>
                      <li><a href="<?php echo base_url(); ?>www/tour">Manage All Posting</a></li>
                      <li><a href="<?php echo base_url(); ?>www/open_trip">Manage Open trip</a></li>
                      <li><a href="<?php echo base_url(); ?>www/category">Manage Categories</a></li>
                      <li><a href="<?php echo base_url(); ?>www/destination">Manage Destination</a></li>
                      <li><a href="<?php echo base_url(); ?>www/remark">Manage Remark</a></li>

                    </ul>

                  </li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">WEB CONNECTION <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/email">Manage Incoming Mail</a></li>

                      <li><a href="<?php echo base_url(); ?>www/customer">Manage Social Media</a></li>

                    </ul>

                  </li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">WEB CONFIGURATION <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/menu">Edit Top & Bottom Menu</a></li>

                      <li><a href="<?php echo base_url(); ?>www/media">Manage Media Library</a></li>

                      <li><a href="<?php echo base_url(); ?>www/header">Manage Header Sideshow</a></li>

                      <li><a href="<?php echo base_url(); ?>www/partners">Manage Boat Partners</a></li>

                      <li><a href="<?php echo base_url(); ?>www/testimonial">Manage Guest Comment</a></li>
                      
                      <li><a href="<?php echo base_url(); ?>www/banner">Manage Banner</a></li>

                      <li><a href="<?php echo base_url(); ?>www/admin"><?php echo $this->session->userdata('level') == 'Super Administrator' ? 'Manage Admin' : 'Edit Profil' ?></a></li><!--

                      <li><a href="<?php //echo base_url();  ?>www/galeri"><span class="glyphicon glyphicon-picture"></span> Manage Gallery Images</a></li>-->

                      <li><a href="<?php echo base_url(); ?>www/others">Others Setting</a></li>
                      
                    </ul>

                  </li>
                  
                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">REPORT <span class="caret"></span></a>

                    <ul class="dropdown-menu">

                      <li><a href="<?php echo base_url(); ?>www/booking/listall">All Bookings</a></li>
                    </ul>
                  </li>
                  
                  <li><a href="<?php echo base_url('www/logout') ?>">LOGOUT </a></li>

                </ul>

              </div><!-- /.navbar-collapse -->

            </div><!-- /.container-fluid -->

        </div>

       	<div id="bersih"></div>

      </div>

      <?php echo $content; ?>

    </div>

    <div id="totop" data-toggle="popover" data-content="Click arrow for back to top">

      <span class="glyphicon glyphicon-chevron-up"></span>

    </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.cookie.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/default.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/ajax_upload/jquery.form.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/totop.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popover.js"></script>

<script src="path/to/"></script>
    <span id="base_url" title="<?php echo base_url(); ?>"></span>

  </body>

</html>