<div id="content-judul">
	<span class="glyphicon glyphicon-comment"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;

if($method=='list')
{
?>
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td width="6%">No.</td>
    <td width="20%">Name</td>
    <td width="40%">Comment</td>
    <td width="24%">Date</td>
    <td width="10%" colspan="3">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($testimonial as $row){
?>

<div class="modal fade" id="myModal<?php echo $no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Buku Tamu</h4>
      </div>
      <div class="modal-body">
        <h3><?php echo anchor($row->comment_url, $row->comment_name, array('target'=>'_blank')); ?></h3>
        <br />
        Waktu Komentar : <div style="font-size:10px;"><?php echo $row->comment_date; ?></div>
        <?php echo $row->comment_comment; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


  <tr id="row<?php echo $no; ?>">
    <td><?php echo $no++,'.'; ?></td>
    <td><?php echo $row->comment_name; ?></td>
    <td>
		<?php echo substr(strip_tags($row->comment_comment),0,60)." ... "; ?>&nbsp;
    </td>
    <td><?php echo $row->comment_date; ?></td>
    <td>
		<?php $no_e = $no-1; echo $row->comment_publish=='no'?'
		<span id="eye'.$no_e.'">
			<span class="glyphicon glyphicon-eye-close eye-close" onclick="publish(\'testimonial/update/\', \''.$row->comment_id.'\', \'/yes\', \''.$no_e.'\')"></span>
		</span>
		':'
		<span id="eye'.$no_e.'">
			<span class="glyphicon glyphicon-eye-open eye-open" onclick="publish(\'testimonial/update/\', \''.$row->comment_id.'\', \'/no\', \''.$no_e.'\')"></span>
		</span>'; ?>
    </td>
    <td>
         <a data-toggle="modal" href="#myModal<?php echo $no-1; ?>"><span class="glyphicon glyphicon-fullscreen zoom"  title="read more" data-toggle="tooltip"></span></a>
    </td>
    <td>
    	<span onclick="hapus('<?php base_url(); ?>testimonial/delete/<?php echo $row->comment_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove"title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
}
?>
</tbody>
</table>

		</div>
    </div>
</div>