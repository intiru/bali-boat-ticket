<div id="content-judul">
	<span class="glyphicon glyphicon-random"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = (empty($method))?'':$method;
// ------------------------------- TUBUH CATEGORY MANAJEMEN ---------------------------------------- //

if($method=='list'){
	echo anchor('www/sub_category/create', '<span class="glyphicon glyphicon-plus"></span> Add Sub Category', array('class'=>'btn btn-success'));
?>
<br /><br />
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td width="6%" align="center">No.</td>
    <td width="28%">Sub Category</td>
    <td width="28%">Top Category</td>
    <td width="10%" colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no = 1;
	foreach($category as $row){
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->sub_nama; ?></td>
    <td><?php echo $row->kategori_nama; ?></td>
    <td align="center">
	<?php echo anchor('www/sub_category/edit/'.$row->sub_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
    <td align="center">
    	<span onclick="hapus('<?php echo base_url(); ?>www/sub_category/delete/<?php echo $row->sub_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>






<?php

//---------------------------------------------------- CREATE -------------------------------------//
	
}
elseif($method=='create')
{
	echo tinymce();
	echo form_open('www/sub_category/insert', array('id'=>'insert', 'title'=>base_url().'www/sub_category'));
?>
<table width="100%" border="0">
  <tr>
    <td width="199">Top Category</td>
    <td width="835">
    	<select name="kategori" class="form-control">
<?php	foreach($kategori as $row){ ?>
        	<option value="<?php echo $row->kategori_id ?>"><?php echo $row->kategori_nama; ?></option>
<?php 	}?>
        </select>
    </td>
  </tr>
  <tr>
    <td width="199">Sub Category Name</td>
    <td width="835"><input type="text" name="name" class="form-control" /></td>
  </tr>
  <tr>
    <td height="47" valign="top">Content</td>
    <td><textarea name="content" id="tinymce" cols="" rows=""></textarea></td>
  </tr>
  <tr>
    <td>Meta Title</td>
    <td><input type="text" name="meta_title" class="form-control" /></td>
  </tr>
  <tr>
    <td>Meta Description</td>
    <td><input type="text" name="meta_description" class="form-control"/></td>
  </tr>
  <tr>
    <td>Meta Keywords</td>
    <td><input type="text" name="meta_keywords" class="form-control"/></td>
  </tr>
  <tr>
  	<td height="40" valign="bottom"></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
        <?php echo anchor('www/sub_category', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>  
    </td>
  </tr>
</table>
<?php
	echo form_close();
}
elseif($method=='edit')
{
	echo tinymce();
	echo form_open('www/sub_category/update/'.$this->uri->segment(4),  array('id'=>'update', 'title'=>base_url().'www/sub_category'));
?>

<table width="100%" border="0">
  <tr>
    <td width="199">Top Category</td>
    <td width="835">
    	<select name="kategori" class="form-control">
<?php	foreach($kategori as $row){ ?>
        	<option value="<?php echo $row->kategori_id ?>" <?php echo $edit->kategori_id==$row->kategori_id?'selected':NULL; ?>><?php echo $row->kategori_nama; ?></option>
<?php 	}?>
        </select>
    </td>
  </tr>
  <tr>
    <td width="200">Category Name</td>
    <td width="834"><input type="text" name="name" class="form-control" value="<?php echo $edit->sub_nama; ?>" /></td>
  </tr>
  <tr>
    <td height="47" valign="top">Content</td>
    <td><textarea name="content" id="tinymce" cols="" rows=""><?php echo $edit->sub_isi; ?></textarea></td>
  </tr>
  <tr>
    <td>Meta Title</td>
    <td><input type="text" name="meta_title" class="form-control" value="<?php echo $edit->meta_title;?>" /></td>
  </tr>
  <tr>
    <td>Meta Description</td>
    <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description;?>" /></td>
  </tr>
  <tr>
    <td>Meta Keywords</td>
    <td><input type="text" name="meta_keywords" class="form-control"  value="<?php echo $edit->meta_keywords;?>"/></td>
  </tr>
  <tr>
  	<td height="40" valign="bottom"></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
        <?php echo anchor('www/sub_category', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
    </td>
  </tr>
</table>
<?php
echo form_close();
}
?>
		</div>
    </div>
</div>