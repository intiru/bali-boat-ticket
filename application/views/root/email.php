<div id="content-judul">
	<span class="glyphicon glyphicon-cloud-download"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;

if($method=='list')
{
	echo anchor('www/email/create', '<span class="glyphicon glyphicon-plus"></span> Add Email', array('class'=>'btn btn-success'));
?>
	<br /><br />
<table  width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td align="center" width="10%">No</td>
    <td width="70%">Email</td>
    <td width="10%">Use</td>
    <td width="10%" colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($email as $row){
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->config_akun; ?></td>
    <td><?php echo $row->config_publish; ?></td>
    <td align="center">
		<?php echo anchor('www/email/edit/'.$row->config_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php base_url(); ?>email/delete/<?php echo $row->config_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>

<?php	
}
elseif($method=='create')
{
	echo form_open('www/email/insert', array('id'=>'email', 'title'=>base_url().'www/email'));
?>
<table>
  <tr>
    <td width="178">Account Name</td>
    <td width="283">Email</td>
  </tr>
  <tr>
    <td>Email Address</td>
    <td><input type="text" name="email" class="form-control" /></td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Use Email</td>
    <td>
    	<label class="radio-inline">
    		<input type="radio" name="use" value="yes" id="use_0" />Yes
        </label>
        <label class="radio-inline">
        	<input type="radio" name="use" value="no" id="use_1" checked="checked" />No
        </label>
    </td>
    <td></td>
  </tr>
  <tr>
  	<td height="60"></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
<?php echo anchor('www/email', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?> 
    </td>
    <td></td>
  </tr>
</table>
<?php
	echo form_close();
}

elseif($method=='edit')
{
	echo form_open('www/email/update/'.$edit->config_id, array('id'=>'email', 'title'=>base_url().'www/email'));
?>
<table>
  <tr>
    <td width="178">Account Name</td>
    <td width="283">Email</td>
  </tr>
  <tr>
    <td>Email Address</td>
    <td><input type="text" name="email" class="form-control" value="<?php echo $edit->config_akun; ?>"/></td>
    <td><span id="form_error"></span></td>
  </tr>
  <tr>
    <td>Use Email</td>
    <td>
    	<label class="radio-inline">
    		<input type="radio" name="use" value="yes" id="use_0" <?php echo ($edit->config_publish=='yes')?"checked='checked'":''; ?> />
        Yes
        </label>
        <label class="radio-inline">
        <input type="radio" name="use" value="no" id="use_1" <?php echo ($edit->config_publish=='no')?"checked='checked'":''; ?> />
        	No
        </label>
    </td>
    <td></td>
  </tr>
  <tr>
  	<td height="60"></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
<?php echo anchor('www/email', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?> 
    </td>
    <td></td>
  </tr>
</table>
<?php	
	echo form_close();
}
?>
</div>