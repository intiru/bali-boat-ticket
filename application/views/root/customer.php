<div id="content-judul">
	<span class="glyphicon glyphicon-earphone"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;
		
//--------------------------------------- TUBUH PROGRAM ---------------------------------------//
if($method=='list'){
	echo anchor('www/customer/create', '<span class="glyphicon glyphicon-plus"></span> Add Customer Service', array('class'=>'btn btn-success'));
?>
<br /><br />
<table  width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td width="6%" align="center">No</td>
    <td width="20%">Position</td>
    <td width="30%">Title</td>
    <td width="30%">URL</td>
    <td width="6%">Publish</td>
    <td>Images</td>
    <td width="8%" colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($customer as $row){
		$images = array(
					'src'=>'./uploaded/content/'.$row->config_images,
					'width'=>'30',
					'class'=>'img-thumbnail',
					'style'=>'background-color:#ccc'
				  );
		$image = ($row->config_images)?img($images):"";
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->config_posisi; ?></td>
    <td><?php echo $row->config_title; ?></td>
    <td><?php echo $row->config_akun; ?></td>
    <td><?php echo $row->config_publish; ?></td>
    <td><?php if($row->config_posisi == 'Link'){echo $image;} ?></td>
    <td align="center">
		<?php echo anchor('www/customer/edit/'.$row->config_id, '<span class="glyphicon glyphicon-pencil pencil"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php base_url(); ?>customer/delete/<?php echo $row->config_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove"></span></span>
  </tr>
<?php
	}
?>
</tbody>
</table>





<?php

//-------------------------------------------------------- CREATE --------------------------------------//

}elseif($method=='create'){
	echo form_open_multipart('www/customer/insert', array('id'=>'insert-file', 'class'=>'normal', 'title'=>base_url().'www/customer'));
?>
<table width="576">
  <!--<input type="hidden" name="posisi" value="Link">-->
 	<tr>
    <td width="211">Type</td>
    <td width="353">
      <label class="radio-inline">
        <input type="radio" name="posisi" value="Yahoo" id="menu" checked="checked" onclick="document.getElementById('images').disabled = true;" />
        Yahoo</label>
      <label class="radio-inline">
        <input type="radio" name="posisi" value="Skype" id="menu" onclick="document.getElementById('images').disabled = true;" />
        Skype</label>
      <label class="radio-inline">
        <input type="radio" name="posisi" value="Link" id="menu" onclick="document.getElementById('images').disabled = false;" checked/>
        Link</label>
    </td>
  </tr>
  <tr>
    <td>Account ID / URL</td>
    <td><input type="text" name="id_akun" class="form-control" /></td>
  </tr>
  <tr>
    <td>Account Title</td>
    <td><input type="text" name="title" class="form-control" /></td>
  </tr>
  <tr>
    <td></td>
    <td><img id="gambar" src="" /></td>
  </tr>
  <tr>
    <td>Images</td>
    <td><input name="userfile" id="images" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image_customer(this);" /></td>
  </tr>
  <tr>
    <td>Publish</td>
    <td>
    	<label class="radio-inline">
    		<input type="radio" name="publish" value="yes" />Yes
        </label>
        <label class="radio-inline">
        	<input type="radio" name="publish" value="no" checked="checked" />No
        </label>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
		<?php echo anchor('www/customer', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?> 
    </td>
  </tr>
</table>
<?php
echo form_close();
}elseif($method=='edit'){
	echo form_open_multipart('www/customer/update/'.$edit->config_id, array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/customer'));
?>
  <!--<input type="hidden" name="posisi" value="Link">-->
<table width="576">
	<tr>
    <td width="211">Type</td>
    <td width="353">
      <label class="radio-inline">
        <input type="radio" name="posisi" value="Yahoo" id="menu" onclick="document.getElementById('images').disabled = true;" <?php echo ($edit->config_posisi=='Yahoo')?"checked='checked'":''; ?> />
        Yahoo</label>
      <label class="radio-inline">
        <input type="radio" name="posisi" value="Skype" id="menu" onclick="document.getElementById('images').disabled = true;" <?php echo ($edit->config_posisi=='Skype')?"checked='checked'":''; ?>  />
        Skype</label>
      <label class="radio-inline">
        <input type="radio" name="posisi" value="Link" id="menu" onclick="document.getElementById('images').disabled = false;" <?php echo ($edit->config_posisi=='Link')?"checked='checked'":''; ?> checked />
        Link</label>
    </td>
  </tr>
  <tr>
    <td>Account ID / URL</td>
    <td><input type="text" name="id_akun" class="form-control" value="<?php echo $edit->config_akun; ?>" /></td>
  </tr>
  <tr>
    <td>Account Title</td>
    <td><input type="text" name="title" class="form-control"  value="<?php echo $edit->config_title ?>"/></td>
  </tr>
  <tr>
  	<td></td>
    <td><?php if($edit->config_posisi == 'Link'){echo img(array('src'=>'uploaded/content/'.$edit->config_images, 'id'=>'gambar', 'style'=>'max-width:250px;'));} ?></td>
  </tr>
  <tr>
    <td>Images</td>
    <td><input name="userfile" id="images" type="file" accept="image/jpeg, image/jpg, image/png"  <?php echo ($edit->config_posisi=='Link')?"":"disabled='disabled'"; ?> onchange="read_image_customer(this);"  /></td>
  </tr>
  <tr>
    <td>Publish</td>
    <td>
    	<label class="radio-inline">
    		<input type="radio" name="publish" value="yes"  <?php echo ($edit->config_publish=='yes')?"checked='checked'":''; ?> />Yes
        </label>
        <label class="radio-inline">
        	<input type="radio" name="publish" value="no"  <?php echo ($edit->config_publish=='no')?"checked='checked'":''; ?>  />No
        </label>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
		<?php echo anchor('www/customer', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?> 
    </td>
  </tr>
</table>
<?php	
}
?>
</div>