<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/validation/core/admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap_admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.css" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
<div id="content-judul">
	<span class="glyphicon glyphicon-home"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;
		
//--------------------------------------- TUBUH PROGRAM ---------------------------------------//
if($method=='list'){
	echo anchor('www/room/create/'.$artikel_id, '<span class="glyphicon glyphicon-plus"></span> Add Room Type', array('class'=>'btn btn-success'));
	//echo '&nbsp;&nbsp;&nbsp;';
	//echo anchor('www/hotel', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning'));
?>
<br /><br />
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td align="center" width="6%">No</td>
    <td width="24%">Room Type</td>
    <td width="20%">Low Seasson</td>
    <td width="20%">High Seasson</td>
    <td width="20%">Peak Seasson</td>
    <td width="10%" colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($list as $row){
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->room_tipe; ?></td>
    <td><?php echo $row->low_seasson; ?></td>
    <td><?php echo $row->high_seasson; ?></td>
    <td><?php echo $row->peak_seasson; ?></td>
    <td align="center">
		<?php echo anchor('www/room/edit/'.$artikel_id.'/'.$row->room_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php echo base_url(); ?>www/room/delete/delete/<?php echo $row->room_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>

<?php

//-------------------------------------------------------- CREATE --------------------------------------//

}
elseif($method=='create')
{
	echo form_open('www/room/insert/'.$artikel_id);
?>
<table width="513" border="0">
  <tr>
    <td width="165">Room Type</td>
    <td width="338"><input type="text" name="name" class="form-control" /></td>
  </tr>
  <tr>
    <td width="165">Low Seasson</td>
    <td width="338"><input type="text" name="low" class="form-control" /></td>
  </tr>
  <tr>
    <td width="165">High Seasson</td>
    <td width="338"><input type="text" name="high" class="form-control" /></td>
  </tr>
  <tr>
    <td width="165">Peak Seasson</td>
    <td width="338"><input type="text" name="peak" class="form-control" /></td>
  </tr>
  <tr>
  	<td height="40" valign="bottom"></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
  		<?php echo anchor('www/room/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>  
    </td>
  </tr>
</table>

<?php
	echo form_close();
}
elseif($method=='edit')
{
	echo form_open('www/room/update/'.$artikel_id.'/'.$edit->room_id);
?>
  <table width="513" border="0">
  <tr>
    <td width="165">Room Type</td>
    <td width="338"><input type="text" name="name" class="form-control" value="<?php echo $edit->room_tipe; ?>" /></td>
  </tr>
  <tr>
    <td width="165">Low Seasson</td>
    <td width="338"><input type="text" name="low" class="form-control" value="<?php echo $edit->low_seasson; ?>" /></td>
  </tr>
  <tr>
    <td width="165">High Seasson</td>
    <td width="338"><input type="text" name="high" class="form-control"  value="<?php echo $edit->high_seasson; ?>"/></td>
  </tr>
  <tr>
    <td width="165">Peak Seasson</td>
    <td width="338"><input type="text" name="peak" class="form-control"  value="<?php echo $edit->peak_seasson; ?>"/></td>
  </tr>
  <tr>
  	<td height="40" valign="bottom"></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
  		<?php echo anchor('www/room/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>  
    </td>
  </tr>
</table>
<?php
	echo form_close();
}
?>
		</div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/default.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/ajax_upload/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/totop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popover.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blocked.js"></script>
<span id="base_url" title="<?php echo base_url(); ?>"></span>