<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 

      <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
      <?php
      $method = (empty($method)) ? '' : $method;
//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
      if ($method == 'list') {
        echo anchor('www/itinerary/create/'.$artikel_id, '<span class="glyphicon glyphicon-plus"></span> Add Itinerary', array('class' => 'btn btn-success'));
        echo '&nbsp&nbsp';
        echo anchor('www/booking/list/'.$artikel_id, '<span class="glyphicon glyphicon-book"></span> Bookings', array('class' => 'btn btn-success'));
        echo '&nbsp&nbsp';
        echo anchor('www/tour', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class' => 'btn btn-warning'));
        ?>
        <br />
        <br />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.min.css') ?>">
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap-switch-master/dist/js/bootstrap3/bootstrap-switch.min.js') ?>"></script>


        <table width="100%" class="table table-striped table-hover table-responsive">
          <thead>
            <tr>
              <td width="4%" align="center">No.</td>
              <td>Itinerary Title</td>
              <td>Publish</td>
              <td width="15%" colspan="5" align="center">Menu</td>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($list as $row) {
              ?>
              <tr  id="row<?php echo $no; ?>">
                <td align="center"><?php echo $no++ . '.'; ?></td>
                <td><?php echo $row->artikel_title; ?></td>
                <td>
                    <?php echo $row->publish == 'yes' ? '
                    <span id="eye' . $row->artikel_id . '">
			<span class="glyphicon glyphicon-eye-open eye-open" onclick="publish(\''.base_url().'www/tour/publish/\', \'' . $row->artikel_id . '\', \'/no\', \'' . $row->artikel_id . '\')"></span>
		</span>
		' : '
		<span id="eye' . $row->artikel_id . '">
			<span class="glyphicon glyphicon-eye-close eye-close" onclick="publish(\''.base_url().'www/tour/publish/\', \'' . $row->artikel_id . '\', \'/yes\', \'' . $row->artikel_id . '\')"></span>
		</span>
		'; ?>


                </td>
                <td>
                    <?php echo anchor('www/booking/create/' .$artikel_id.'/'. $row->artikel_id, '<span class="glyphicon glyphicon-book picture" title="Book Now" data-toggle="tooltip"></span>'); ?>
                </td>
                <td>
                    <?php echo anchor('www/icalendar/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-calendar picture" title="Available Itenary" data-toggle="tooltip"></span>'); ?>
                </td>
                <td>
                  <a href="<?php echo base_url('www/payment/list/'.$row->artikel_id) ?>"><img src=" <?php echo base_url('assets/img/root/icon-dollar-2.png') ?>" width="24"></a></td>
                <td><?php echo anchor('www/itinerary/edit/' .$artikel_id.'/'. $row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
                <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/itinerary/delete/<?php echo $artikel_id.'/'.$row->artikel_id; ?>', '<?php echo $no - 1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php
//-------------------------------------------------------- CREATE ---------------------------------------------//
      } elseif ($method == 'create') {
        echo tinymce_3();
        echo form_open_multipart('www/itinerary/insert/'.$artikel_id, array('id' => 'insert-file', 'title' => base_url() . 'www/itinerary/list/'.$artikel_id));
        ?>
          <input type="hidden" name="publish" value="yes">
        <table width="100%" border="0" >
          <tr>
            <td>Title</td>
            <td><input type="text" name="title" class="form-control" /></td>
          </tr>
          <tr>
            <td>Description</td>
            <td><textarea name="description" id="tinymce" cols="" rows=""></textarea></td>
          </tr>
          <tr class="hide">
            <td height="47" valign="top">Remark</td>
            <td><textarea name="remark" id="tinymce_2" cols="" rows=""></textarea></td>
          </tr>
          <tr class="spec hide">
            <td height="47" valign="top">Specification</td>
            <td><textarea name="spec" id="tinymce_3" cols="" rows=""></textarea></td>
          </tr>
          <tr class="hide">
            <td>Meta Title</td>
            <td><input type="text" name="meta_title" class="form-control" /></td>
          </tr>
          <tr class="hide">
            <td>Meta Description</td>
            <td><input type="text" name="meta_description" class="form-control" /></td>
          </tr>
          <tr class="hide">
            <td>Meta Keywords</td>
            <td><input type="text" name="meta_keywords" class="form-control"/></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
        <?php echo anchor('www/itinerary/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
//------------------------------------------------------- EDIT ------------------------------------------//
      } elseif ($method == 'edit') { 
        echo tinymce_3();
        echo form_open_multipart('www/itinerary/update/' . $artikel_id.'/'. $edit->artikel_id, array('id' => 'update-file', 'title' => base_url() . 'www/itinerary/list/'.$artikel_id));
        ?>
        <table width="100%" border="0">
          <tr>
            <td>Title</td>
            <td><input type="text" name="title" class="form-control" value="<?php echo $edit->artikel_title; ?>" /></td>
          </tr>
          <tr>
            <td>Description</td>
            <td><textarea name="description" id="tinymce" cols="" rows=""><?php echo $edit->artikel_isi; ?></textarea></td>
          </tr>
          <tr class="hide">
            <td height="47" valign="top">Remark</td>
            <td><textarea name="remark" id="tinymce_2" cols="" rows=""><?php echo $edit->artikel_remark; ?></textarea></td>
          </tr>
          <tr class="spec hide">
            <td height="47" valign="top">Specification</td>
            <td><textarea name="spec" id="tinymce_3" cols="" rows=""><?php echo $edit->artikel_spec; ?></textarea></td>
          </tr>
          <tr class="hide">
            <td>Meta Title</td>
            <td><input type="text" name="meta_title" class="form-control" value="<?php echo $edit->meta_title; ?>"  /></td>
          </tr>
          <tr class="hide">
            <td>Meta Description</td>
            <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description; ?>" /></td>
          </tr>
          <tr class="hide">
            <td>Meta Keywords</td>
            <td><input type="text" name="meta_keywords" class="form-control" value="<?php echo $edit->meta_keywords; ?>" /></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
        <?php echo anchor('www/itinerary/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
      }
      ?>
    </div>
  </div>
</div>