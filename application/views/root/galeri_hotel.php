<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/validation/core/admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap_admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.css" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
<div id="loading"></div>
<div id="success"><span class="glyphicon glyphicon-ok"></span></div>
<div id="error"><span class="glyphicon glyphicon-remove"></span></div>
<div id="status-success"></div>
<div id="status-error"></div>
<div class="progress-bar-admin-base progress-bar-admin"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
	<span id="percent"></span>
</div>

<div id="content-judul">
	<span class="glyphicon glyphicon-picture"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;
		
//--------------------------------------- TUBUH PROGRAM ---------------------------------------//
if($method=='list'){
	echo anchor('www/galeri_hotel/create/'.$artikel_id, '<span class="glyphicon glyphicon-plus"></span> Add Images Gallery', array('class'=>'btn btn-success'));
	//echo '&nbsp;&nbsp;&nbsp;';
	//echo anchor('www/hotel', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning'));
?>
<br /><br />
<div style="overflow:auto; width:100%; height: 340px">
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td align="center" width="6%">No</td>
    <td width="42%">Images Title</td>
    <td width="42%">Images View</td>
    <td width="10%" colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($galeri as $row){
		$images = array(
					'src'=>'./uploaded/galeri/'.$row->galeri_nama,
					'width'=>'120',
					'class'=>'img-thumbnail'
				  );
		$image  = $row->galeri_nama?img($images):NULL;
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->galeri_judul; ?></td>
    <td><?php echo $image; ?></td>
    <td align="center">
		<?php echo anchor('www/galeri_hotel/edit/'.$artikel_id.'/'.$row->galeri_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php echo base_url(); ?>www/galeri_hotel/delete/<?php echo $row->galeri_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>
</div>
<?php
//-------------------------------------------------------- CREATE --------------------------------------//

}
elseif($method=='create')
{
	echo form_open_multipart('www/galeri_hotel/insert/'.$artikel_id, array('id'=>'insert-file', 'class'=>'normal', 'title'=>base_url().'www/galeri_hotel'));
?>
<table width="100%">
    <tr>
      <td width="89">Title</td>
      <td width="943"><input name="title" type="text" class="form-control" /></td>
    </tr>
    <tr>
    	<td></td>
        <td><img id="gambar" src="" /></td>
    </tr>
    <tr>
      <td>Images </td>
      <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);" /></td>
    </tr>
    <tr>
      <td></td>
      <td>
      	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
      	<?php echo anchor('www/galeri_hotel/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
      </td>
    </tr>
  </table>
<?php
	echo form_close();
}
elseif($method=='edit')
{
	echo form_open_multipart('www/galeri_hotel/update/'.$edit->galeri_id, array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/galeri_hotel'));
?>
  <table>
    <tr>
      <td width="156">Title</td>
      <td width="317"><input name="title" type="text" class="form-control" value="<?php echo $edit->galeri_judul; ?>" /></td>
    </tr>
    <tr>
    	<td></td>
        <td><?php echo img(array('src'=>'uploaded/galeri/'.$edit->galeri_nama, 'width'=>'300', 'class'=>'img-thumbnail', 'id'=>'gambar')); ?></td>
    </tr>
    <tr>
      <td>Images </td>
      <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);"/></td>
    </tr>
    <tr>
      <td></td>
      <td>
      	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
      	<?php echo anchor('www/galeri_hotel/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
      </td>
    </tr>
  </table>
<?php
	echo form_close();
}
?>
		</div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/default.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/ajax_upload/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/totop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popover.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blocked.js"></script>
<span id="base_url" title="<?php echo base_url(); ?>"></span>