<div id="content-judul">
	<span class="glyphicon glyphicon-picture"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;
		
//--------------------------------------- TUBUH PROGRAM ---------------------------------------//
if($method=='list'){
	echo anchor('www/galeri/create', '<span class="glyphicon glyphicon-plus"></span> Add Images Gallery', array('class'=>'btn btn-success'));
	echo '&nbsp;&nbsp;&nbsp;';
	echo anchor('www/menu', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning'));
?>
<br /><br />
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td align="center" width="6%">No</td>
    <td width="42%">Images Title</td>
    <td width="42%">Images View</td>
    <td width="10%" colspan="2" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no=1;
	foreach($galeri as $row){
		$images = array(
					'src'=>'./uploaded/galeri/'.$row->galeri_nama,
					'width'=>'120',
					'class'=>'img-thumbnail'
				  );
		$image  = $row->galeri_nama?img($images):NULL;
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td><?php echo $row->galeri_judul; ?></td>
    <td><?php echo $image; ?></td>
    <td align="center">
		<?php echo anchor('www/galeri/edit/'.$row->galeri_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php base_url(); ?>galeri/delete/<?php echo $row->galeri_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>





<?php

//-------------------------------------------------------- CREATE --------------------------------------//

}
elseif($method=='create')
{
	echo form_open_multipart('www/galeri/insert', array('id'=>'insert-file', 'class'=>'normal', 'title'=>base_url().'www/galeri'));
?>
<table width="100%">
    <tr>
      <td width="89">Title</td>
      <td width="943"><input name="title" type="text" class="form-control" /></td>
    </tr>
    <tr>
    	<td></td>
        <td><img id="gambar" src="" /></td>
    </tr>
    <tr>
      <td>Images </td>
      <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);" /></td>
    </tr>
    <tr>
      <td></td>
      <td>
      	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
      	<?php echo anchor('www/galeri', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
      </td>
    </tr>
  </table>
<?php
	echo form_close();
}
elseif($method=='edit')
{
	echo form_open_multipart('www/galeri/update/'.$edit->galeri_id, array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/galeri'));
?>
  <table>
    <tr>
      <td width="156">Title</td>
      <td width="317"><input name="title" type="text" class="form-control" value="<?php echo $edit->galeri_judul; ?>" /></td>
    </tr>
    <tr>
    	<td></td>
        <td><?php echo img(array('src'=>'uploaded/galeri/'.$edit->galeri_nama, 'width'=>'300', 'class'=>'img-thumbnail', 'id'=>'gambar')); ?></td>
    </tr>
    <tr>
      <td>Images </td>
      <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);"/></td>
    </tr>
    <tr>
      <td></td>
      <td>
      	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
      	<?php echo anchor('www/galeri', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
      </td>
    </tr>
  </table>
<?php
	echo form_close();
}
?>
		</div>
    </div>
</div>