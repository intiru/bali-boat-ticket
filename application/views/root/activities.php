<div id="content-judul"><span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
    <div class="col-md-12">
        <div id="halaman">

            <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
            <?php
            $method = (empty($method)) ? '' : $method;
            //------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
            if ($method == 'list') {
                echo anchor('www/activities/create', '<span class="glyphicon glyphicon-plus"></span> Add New Entri', array('class' => 'btn btn-success btn-plus-tour'));
                ?>

            <link rel="stylesheet" type="text/css"
                  href="<?php echo base_url('assets/plugin/datatables/datatables.min.css'); ?>">
                <script type="text/javascript"
                        src="<?php echo base_url('assets/plugin/datatables/datatables.min.js'); ?>"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#example').DataTable({
                            "order": [[1, "asc"]],
                            language: {
                                searchPlaceholder: "Search your boat name"
                            },
                            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                            "iDisplayLength": <?php echo $config->config_show_entry ?>,
                            "fnDrawCallback": function (oSettings) {
                                //alert( 'DataTables has redrawn the table' );
                            }
                        });
                    });
                </script>

                <style type="text/css">
                    select[name="example_length"] {
                        background-color: #2A9992 !important;
                        color: white !important;
                        border-radius: 4px;
                        padding: 4px 12px;
                        border: 4px solid #2A9992 !important;
                    }

                    .dataTables_filter input {
                        background-color: #2A9992 !important;
                        border: 1px solid #2A9992 !important;
                        padding: 6px 12px;
                        color: white;
                        border-radius: 4px;
                    }

                    .dataTables_filter input::placeholder {
                        color: white;
                    }

                    #example thead {
                        background-color: #446CB3;
                        color: white;
                    }

                    .dataTables_length {
                        margin-left: 200px;
                    }

                    .btn-plus-tour {
                        margin-bottom: -88px;
                        z-index: 99999999 !important;
                        position: relative
                    }
                </style>


            <br/>
            <br/>
                <div class="table-wrapper" style="overflow: auto;">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th width="20">No.</th>
                            <th width="140">Feature Image</th>
                            <th>Activities Name</th>
                            <th>Publish</th>
                            <th>Popular</th>
                            <th>Price</th>
                            <!--                            <th>Departure</th>
                                                        <th>Destination</th>-->
                            <!--     <th>Boat Categories</th>-->
                            <th width="200">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($list as $row) {
                            /*           $departure_id = json_decode($row->departure_id, TRUE);
                                       $departure_id = count($departure_id) == 0 ? array() : $departure_id;
                                       $departure = $this->db
                                           ->select('destination')
                                           ->where_in('id', $departure_id)
                                           ->get('tb_destination')
                                           ->result();

                                       $kategori_id = json_decode($row->kategori_id, TRUE);
                                       $kategori = $this->db->where_in('kategori_id', $kategori_id)->get('tb_kategori')->result();*/
                            ?>
                            <tr id="row<?php echo $no; ?>" data-artikel-id="<?php echo $row->artikel_id ?>"
                            >
                                <td align="center"><?php echo $no++ . '.'; ?></td>
                                <td align="center"><?php
                                    if (!empty($row->artikel_gambar)) {
                                        echo img(array('src' => 'uploaded/content/' . $row->artikel_gambar, 'width' => '150', 'class' => 'img-thumbnail'));
                                    }
                                    ?></td>
                                <td><?php echo $row->artikel_title; ?></td>
                                <td><?php echo $row->publish; ?></td>
                                <td><?php echo $row->artikel_star; ?></td>
                                <td>USD <?php echo number_format($row->artikel_harga); ?></td>
                                <!--                                <td>
                                    <?php /*foreach($departure as $r) {
                                        echo $r->destination.', ';
                                    } */ ?>
                                </td>
                                <td>
                                    <?php
                                /*                                    if($row->destination_id != "null" && $row->return_status == 'yes') {
                                                                        $destination_id = json_decode($row->destination_id, TRUE);
                                                                        $destination = $this->db
                                                                            ->select('destination')
                                                                            ->where_in('id', $destination_id)
                                                                            ->get('tb_destination')
                                                                            ->result();
                                                                        foreach($destination as $r) {
                                                                            echo $r->destination.', ';
                                                                        }
                                                                    } else {
                                                                        echo '-';
                                                                    } */ ?>
                                </td>-->
                                <!--                  <td><?php
                                /*                                    if($row->kategori_id != '') {
                                                                        foreach ($kategori as $r) {
                                                                            echo $r->kategori_nama . ', ';
                                                                        }
                                                                    }*/ ?></td>-->
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <!--                                            <td width="10%">
                                                <div>
                                                    <?php /*echo anchor('www/remark_post/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-list-alt picture" title="Remark List" data-toggle="tooltip"></span>'); */ ?>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <div <?php /*echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; */ ?>>
                                                    <?php /*echo anchor('www/booking/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-book picture" title="Booking Records" data-toggle="tooltip"></span>'); */ ?>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <div <?php /*echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; */ ?>>
                                                    <?php /*echo anchor('www/calendar/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-calendar picture" title="Available Kapal" data-toggle="tooltip"></span>'); */ ?>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <div <?php /*echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; */ ?>>
                                                    <?php /*echo anchor('www/itinerary/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-folder-open picture" title="Itinerary" data-toggle="tooltip"></span>'); */ ?>
                                                </div>
                                            </td>
                                            <td class="hide">
                                                <div <?php /*echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; */ ?>>
                                                    <?php /*echo anchor('www/policy/edit/' . $row->artikel_id, '<span class="glyphicon glyphicon-briefcase picture" title="Policy" data-toggle="tooltip"></span>'); */ ?>
                                                </div>
                                            </td>
                                            <td width="10%">

                                                <?php /*if ($partner_id != '') { */ ?>

                                                    <div <?php /*echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; */ ?>>
                                                        <?php /*echo anchor('www/partners/edit/' . $partner_id, '<span class="glyphicon glyphicon-user picture" title="Setting Profil" data-toggle="tooltip"></span>'); */ ?>
                                                    </div>

                                                <?php /*} */ ?>
                                            </td>
                                            <td width="10%">

                                                <?php /*if ($partner_id != '') { */ ?>
                                                    <div <?php /*echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; */ ?>>
                                                        <span class="glyphicon glyphicon-search pencil partner-detail"
                                                              title="detail" data-toggle="tooltip"></span>
                                                    </div>
                                                <?php /*} */ ?>
                                            </td>-->
                                            <td width="10%">
                                                <?php echo $row->a_publish == 'yes' ? '
		<span id="eye' . $row->artikel_id . '">
			<span class="glyphicon glyphicon-eye-open eye-open" onclick="publish(\'tour/publish/\', \'' . $row->artikel_id . '\', \'/no\', \'' . $row->artikel_id . '\')"></span>
		</span>
		' : '
		<span id="eye' . $row->artikel_id . '">
			<span class="glyphicon glyphicon-eye-close eye-close" onclick="publish(\'tour/publish/\', \'' . $row->artikel_id . '\', \'/yes\', \'' . $row->artikel_id . '\')"></span>
		</span>
		'; ?></td>
                                            <!--<td width="10%"><?php /*echo $row->show == 'yes' ? '
		 <span id="show' . $row->artikel_id . '">
			<span class="glyphicon glyphicon-ok eye-open" title="Publish" data-toggle="tooltip" onclick="show(\'tour/show/\', \'' . $row->artikel_id . '\', \'/no\', \'' . $row->artikel_id . '\')"></span>
		</span>
		' : '
		<span id="show' . $row->artikel_id . '">
			<span class="glyphicon glyphicon-remove eye-close" title="Unpublish" data-toggle="tooltip" onclick="show(\'tour/show/\', \'' . $row->artikel_id . '\', \'/yes\', \'' . $row->artikel_id . '\')"></span>
		</span>'; */ ?></td>-->
<!--                                            <td width="10%">--><?php //echo anchor('www/harga/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-briefcase picture" title="departure destination price" data-toggle="tooltip"></span>'); ?><!--</td>-->
                                            <td width="10%"><?php echo anchor('www/galeri_artikel/list/' . $row->artikel_id.'/NULL/activities', '<span class="glyphicon glyphicon-picture picture" title="galeri" data-toggle="tooltip"></span>'); ?></td>
                                            <td width="10%"><?php echo anchor('www/activities/edit/' . $row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
                                            <td width="10%" align="center"><span
                                                        onclick="hapus('<?php echo base_url(); ?>www/activities/delete/<?php echo $row->artikel_id; ?>', '<?php echo $no - 1; ?>')"><span
                                                            class="glyphicon glyphicon-remove remove" title="delete"
                                                            data-toggle="tooltip"></span></span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>


                <div class="modal fade" id="modal-partner-detail" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Partner Detail</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr class="hide">
                                        <td>Login Number</td>
                                        <td class="prt_login_number"></td>
                                    </tr>
                                    <tr>
                                        <td>Username</td>
                                        <td class="prt_username"></td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td class="prt_password"></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td class="prt_nama"></td>
                                    </tr>
                                    <tr>
                                        <td>Photo</td>
                                        <td><img src="" class="prt_photo img-thumbnail" width="300"></td>
                                    </tr>
                                    <tr>
                                        <td>Company Name</td>
                                        <td class="prt_company_name"></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td class="prt_address"></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td class="prt_phone"></td>
                                    </tr>
                                    <tr>
                                        <td>WhatsApp</td>
                                        <td class="prt_wa"></td>
                                    </tr>
                                    <tr>
                                        <td>Fax Number</td>
                                        <td class="prt_fax"></td>
                                    </tr>
                                    <tr>
                                        <td>License Number</td>
                                        <td class="prt_license"></td>
                                    </tr>
                                    <tr>
                                        <td>Website</td>
                                        <td><a href="" class="prt_website" target="_blank">Link Website</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td class="prt_email"></td>
                                    </tr>
                                    <tr>
                                        <td>Bank Name</td>
                                        <td class="prt_bank_name"></td>
                                    </tr>
                                    <tr>
                                        <td>Account Number</td>
                                        <td class="prt_bank_account"></td>
                                    </tr>
                                    <tr>
                                        <td>Beneficiary</td>
                                        <td class="prt_bank_account_beneficiary"></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td class="prt_status"></td>
                                    </tr>
                                    <tr>
                                        <td>Register Date</td>
                                        <td class="prt_date_register"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


            <?php
            //-------------------------------------------------------- CREATE ---------------------------------------------//
            } elseif ($method == 'create') {
            echo tinymce_5();
            echo form_open_multipart('www/activities/insert', array('id' => 'insert-file', 'title' => base_url() . 'www/activities'));
            ?>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('.select2').select2();
                    });
                </script>

            <input type="hidden" name="special" value="no">
                <table border="0">
                    <tr>
                        <td></td>
                        <td><img src="" id="gambar"/></td>
                    </tr>
                    <tr>
                        <td>Feature Image</td>
                        <td><input type="file" name="userfile" onchange="read_image(this)"/></td>
                    </tr>
                    <tr>
                        <td>Activities Name</td>
                        <td><input type="text" style="width: 400px" name="title" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Activities Area</td>
                        <td>
                            <select name="kategori_id" id="kategori" class="form-control select2" style="width:380px;">
                                <?php foreach ($kategori as $row_kategori) { ?>
                                    <option value="<?php echo $row_kategori->kategori_id; ?>"><?php echo $row_kategori->kategori_nama; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Activities Category</td>
                        <td>
                            <select name="kategori_activities_id" id="kategori" class="form-control select2"
                                    style="width:380px;">
                                <?php foreach ($kategori_activities as $row_kategori) { ?>
                                    <option value="<?php echo $row_kategori->kategori_id; ?>"><?php echo $row_kategori->kategori_nama; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="270">Show on Activities Page ?</td>
                        <td><label class="radio-inline">
                                <input type="radio" name="publish" value="no" id="special_0" checked>
                                No</label>
                            <label class="radio-inline">
                                <input type="radio" name="publish" value="yes" id="special_1">
                                Yes</label></td>
                    </tr>
                    <tr>
                        <td width="270">Popular Activities ?</td>
                        <td><label class="radio-inline">
                                <input type="radio" name="popular" value="no" id="special_0" checked>
                                No</label>
                            <label class="radio-inline">
                                <input type="radio" name="popular" value="yes" id="special_1">
                                Yes</label></td>
                    </tr>
                    <tr>
                        <td>Activities Time From</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="departure_time_from" value="09:30">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Activities Time To</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="departure_time_to" value="11:30">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Activities Price</td>
                        <td><input type="number" name="harga" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Activities Description</td>
                        <td><textarea name="general_description" id="tinymce" cols="" rows=""></textarea></td>
                    </tr>
                    <tr>
                        <td>Place Description</td>
                        <td><textarea name="boat_spesification" id="tinymce_2" cols="" rows=""></textarea></td>
                    </tr>
                    <tr>
                        <td>Iframe Google Maps</td>
                        <td><textarea name="diving_activities" class="form-control"  cols="" rows=""></textarea></td>
                    </tr>
                    <tr>
                        <td>Meta Title</td>
                        <td><input type="text" style="width: 400px" name="meta_title" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Meta Description</td>
                        <td><input type="text" name="meta_description" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Meta Keywords</td>
                        <td><input type="text" name="meta_keywords" class="form-control"/></td>
                    </tr>






                    <!--                    <tr>
                                            <td>Special ?</td>
                                            <td><label class="radio-inline">
                                                    <input type="radio" name="special" value="no" id="special_0" checked>
                                                    No</label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="special" value="yes" id="special_1">
                                                    Yes</label></td>
                                        </tr>-->
                    <!--<tr>
                        <td>Category</td>
                        <td><select name="kategori_paket" id="kategori" class="form-control" style="width:180px;">
                                <option value="Board">Liveaboard</option>
                                <option value="Activities">Diving Activities</option>
                        </select></td>
                    </tr>-->
                    <tr class="hide">
                        <td>Email Pemilik Kapal</td>
                        <td>
                            <input type="text" style="width: 400px" value="1" name="email_pemilik_kapal"
                                   class="form-control"/>
                        </td>
                    </tr>
                    <tr class="hide">
                        <td>Komisi Boat Charter Komodo</td>
                        <td>
                            <input type="text" style="width: 80px" name="komisi_platform"
                                   class="form-control pull-left"/>
                            <label style="margin: 10px 0 0 12px;" class="pull-left">
                                <input type="checkbox" name="komisi_platform_rate" value="1" class="pull-left"
                                       value="yes"> Based Contract Rate
                            </label>
                        </td>
                    </tr>
                    <!--                    <tr class="destination">
                        <td>Destination</td>
                        <td><select name="destination_id[]" id="kategori-2" class="form-control select2"
                                    style="width:360px;" multiple="">
                                <?php /*foreach ($destination as $r) { */ ?>
                                    <option value="<?php /*echo $r->id; */ ?>"><?php /*echo $r->destination; */ ?></option>
                                <?php /*} */ ?>
                            </select></td>
                    </tr>-->
                    <!--<tr>
                        <td>Departure Place</td>
                        <td>
                            <select name="departure_id[]" class="form-control select2" multiple>
                                <?php /*foreach ($destination as $r) { */ ?>
                                    <option value="<?php /*echo $r->id */ ?>"><?php /*echo $r->destination */ ?></option>
                                <?php /*} */ ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Departure Time From</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="departure_time_from" value="09:30">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Departure Time To</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="departure_time_to" value="11:30">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Return Status ?</td>
                        <td>
                            <label class="radio-inline">
                                <input type="radio" name="return_status" value="yes"> Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="return_status" value="no" checked> No
                            </label>
                        </td>
                    </tr>
                    <tr class="return_status">
                        <td>Destination Place</td>
                        <td>
                            <select name="destination_id[]" class="form-control select2" multiple>
                                <?php /*foreach ($destination as $r) { */ ?>
                                    <option value="<?php /*echo $r->id */ ?>"><?php /*echo $r->destination */ ?></option>
                                <?php /*} */ ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="return_status">
                        <td>Destination Time From</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="destination_time_from" value="09:30">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr class="return_status">
                        <td>Destination Time To</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="destination_time_to" value="11:30">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>-->
                    <!--                    <tr>
                                            <td>Title</td>
                                            <td><input type="text" style="width: 400px" name="operated" class="form-control"/></td>
                                        </tr>-->
                    <!--        <tr>
                              <td>Days</td>
                              <td><input type="text" name="day" class="form-control" /></td>
                            </tr>-->

                    <!--
         <tr>
           <td>Duration</td>
           <td><input type="text" name="duration" class="form-control" /></td>
         </tr>-->
                    <!--                    <tr class="star">-->
                    <!--                        <td>Star</td>-->
                    <!--                        <td><input type="number" style="width:60px;" name="star" class="form-control" value="0" min="0"-->
                    <!--                                   max="5"/></td>-->
                    <!--                    </tr>-->
                    <!--<tr>
                        <td>Year</td>
                        <td>
                            <div class="pull-left" style="padding-right: 20px">
                                <input type="text" style="width:150px;" name="year" class="form-control" value="<?php /*echo date('Y') */ ?>"/>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Cabin/Seat</td>
                        <td>
                            <div class="pull-left" style="padding-right: 20px">
                                <input type="number" style="width:60px;" name="cabin" class="form-control" min="0"/>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Total Passenger</td>
                        <td>
                            <div class="pull-left" style="padding-right: 20px">
                                <input type="number" style="width:60px;" name="passanger" class="form-control" min="0"/>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>-->

                    <!--                    <tr>
                                            <td>Price Start</td>
                                            <td>
                                                <div class="pull-left" style="padding-right: 20px">
                                                    <input type="text" style="width:150px;" name="harga" class="form-control" min="0"/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </td>
                                        </tr>-->
                    <!--<tr>
                        <td>Double Bed</td>
                        <td>
                            <div class="pull-left" style="padding-right: 20px">
                                <input type="number" style="width:60px;" name="double_bed" class="form-control"
                                       value="0" min="0"/>
                            </div>
                            <div class="pull-left">
                                <input type="text" style="width:500px;" name="double_bed_text" class="form-control"/>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Diving Activities</td>
                        <td>
                            <div class="pull-left" style="padding-right: 20px">
                                <select name="diving_activities" class="form-control" style="width: 70px">
                                    <option value="yes">
                                        Yes
                                    </option>
                                    <option value="no">
                                        No
                                    </option>
                                </select>
                            </div>
                            <div class="pull-left">
                                <input type="text" style="width:500px;" name="diving_activities_text"
                                       class="form-control"/>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Fishing Activities</td>
                        <td>
                            <div class="pull-left" style="padding-right: 20px">
                                <select name="fishing_activities" class="form-control" style="width: 70px">
                                    <option value="yes">
                                        Yes
                                    </option>
                                    <option value="no">
                                        No
                                    </option>
                                </select>
                            </div>
                            <div class="pull-left">
                                <input type="text" style="width:500px;" name="fishing_activities_text"
                                       class="form-control"/>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>-->

                    <!--                    <tr>-->
                    <!--                        <td>General Description</td>-->
                    <!--                        <td><textarea name="general_description" id="tinymce" cols="" rows=""></textarea></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr>
                                            <td>Iframe</td>
                                            <td>
                                                <input type="text" name="iframe" class="form-control"/>
                                            </td>
                                        </tr>-->
                    <!--                    <tr>-->
                    <!--                        <td height="47" valign="top">Boat Spesification</td>-->
                    <!--                        <td><textarea name="boat_spesification" id="tinymce_2" cols="" rows=""></textarea></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr class="spec">-->
                    <!--                        <td height="47" valign="top">Pickup Info</td>-->
                    <!--                        <td><textarea name="pickup_info" id="tinymce_3" cols="" rows=""></textarea></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr class="spec">-->
                    <!--                        <td height="47" valign="top">Schedule and Route</td>-->
                    <!--                        <td><textarea name="schedule_route" id="tinymce_4" cols="" rows=""></textarea></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr class="spec">-->
                    <!--                        <td height="47" valign="top">Short Description</td>-->
                    <!--                        <td><textarea name="short_description" id="tinymce_5" cols="" rows=""></textarea></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr>-->
                    <!--                        <td>Meta Title</td>-->
                    <!--                        <td><input type="text" style="width: 400px" name="meta_title" class="form-control"/></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr>-->
                    <!--                        <td>Meta Description</td>-->
                    <!--                        <td><input type="text" name="meta_description" class="form-control"/></td>-->
                    <!--                    </tr>-->
                    <!--                    <tr>-->
                    <!--                        <td>Meta Keywords</td>-->
                    <!--                        <td><input type="text" name="meta_keywords" class="form-control"/></td>-->
                    <!--                    </tr>-->
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
                            <?php echo anchor('www/activities', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
            <?php
            echo form_close();
            //------------------------------------------------------- EDIT ------------------------------------------//
            } elseif ($method == 'edit') {
            echo tinymce_5();
            echo form_open_multipart('www/activities/update/' . $edit->artikel_id, array('id' => 'update-file', 'title' => base_url() . 'www/activities'));
            ?>


                <script type="text/javascript">
                    $(document).ready(function () {
                        $('.select2').select2();
                    });
                </script>
                <table width="100%" border="0">

                    <tr>
                        <td></td>
                        <td><img src="<?php echo base_url('uploaded/content/'.$edit->artikel_gambar) ?>" width="200" id="gambar"/></td>
                    </tr>
                    <tr>
                        <td>Feature Image</td>
                        <td><input type="file" name="userfile" onchange="read_image(this)"/></td>
                    </tr>
                    <tr>
                        <td>Activities Name</td>
                        <td><input type="text" style="width: 400px" name="title" value="<?php echo $edit->artikel_title ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Activities Area</td>
                        <td>
                            <select name="kategori_id" id="kategori" class="form-control select2" style="width:380px;">
                                <?php foreach ($kategori as $row_kategori) { ?>
                                    <option <?php echo $row_kategori->kategori_id == $edit->kategori_id ? 'selected':'' ?> value="<?php echo $row_kategori->kategori_id; ?>">
                                        <?php echo $row_kategori->kategori_nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Activities Category</td>
                        <td>
                            <select name="kategori_activities_id" id="kategori" class="form-control select2"
                                    style="width:380px;">
                                <?php

                                foreach ($kategori_activities as $row_kategori) { ?>
                                    <option value="<?php echo $row_kategori->kategori_id; ?>" <?php echo $row_kategori->kategori_id == $edit->kategori_activities_id ? 'selected':'' ?>>
                                        <?php echo $row_kategori->kategori_nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="270">Show on Activities Page ?</td>
                        <td><label class="radio-inline">
                                <input type="radio" name="publish" value="no" <?php echo $edit->publish == 'no' ? 'checked':'' ?> id="special_0" checked>
                                No</label>
                            <label class="radio-inline">
                                <input type="radio" name="publish" value="yes" <?php echo $edit->publish == 'yes' ? 'checked':'' ?> id="special_1">
                                Yes</label></td>
                    </tr>
                    <tr>
                        <td width="270">Popular Activities ?</td>
                        <td><label class="radio-inline">
                                <input type="radio" name="popular" value="no" <?php echo $edit->artikel_star == 'no' ? 'checked':'' ?> id="special_0" checked>
                                No</label>
                            <label class="radio-inline">
                                <input type="radio" name="popular" value="yes" <?php echo $edit->artikel_star == 'yes' ? 'checked':'' ?> id="special_1">
                                Yes</label></td>
                    </tr>
                    <tr>
                        <td>Activities Time From</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="departure_time_from" value="<?php echo $edit->departure_time_from ?>">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Activities Time To</td>
                        <td>
                            <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" name="departure_time_to" value="<?php echo $edit->departure_time_to ?>">
                                <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Activities Price</td>
                        <td><input type="number" name="harga" value="<?php echo $edit->artikel_harga ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Activities Description</td>
                        <td><textarea name="general_description" id="tinymce" cols="" rows=""><?php echo $edit->general_description ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Place Description</td>
                        <td><textarea name="boat_spesification" id="tinymce_2" cols="" rows=""><?php echo $edit->boat_spesification ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Iframe Google Maps</td>
                        <td><textarea name="diving_activities" class="form-control" cols="" rows=""><?php echo $edit->diving_activities ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Meta Title</td>
                        <td><input type="text" style="width: 400px" name="meta_title" value="<?php echo $edit->meta_title ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Meta Description</td>
                        <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description ?>" /></td>
                    </tr>
                    <tr>
                        <td>Meta Keywords</td>
                        <td><input type="text" name="meta_keywords" class="form-control" value="<?php echo $edit->meta_keywords ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
                            <?php echo anchor('www/activities', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
                <?php
                echo form_close();
            }
            ?>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/plugin/select2/css/select2.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('/assets/plugin/select2/js/select2.js') ?>"></script>

<link rel="stylesheet" type="text/css"
      href="<?php echo base_url('/assets/plugin/timepicker/dist/bootstrap-clockpicker.css') ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url('/assets/plugin/timepicker/dist/jquery-clockpicker.css') ?>">
<script type="text/javascript"
        src="<?php echo base_url('/assets/plugin/timepicker/dist/bootstrap-clockpicker.js') ?>"></script>
<script type="text/javascript"
        src="<?php echo base_url('/assets/plugin/timepicker/dist/jquery-clockpicker.js') ?>"></script>


<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    $(document).ready(function () {

        $('.select2').select(2);
        $('.clockpicker').clockpicker();

        return_status_change();

        $('[name="return_status"]').change(function () {
            return_status_change();
        });


        $('.partner-detail').click(function (e) {
            e.preventDefault();
            var partner_id = $(this).parents('tr').data('partner-id');
            $.ajax({
                url: '<?php echo base_url() ?>www/setting_profile/detail/' + partner_id,
                success: function (data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    $('#modal-partner-detail').modal('show');
                    $.each(json.field, function (key, val) {

                        if (key == 'prt_photo') {
                            $('.' + key).attr('src', '<?php echo base_url() ?>uploaded/content/' + val);
                        } else if (key == 'prt_website') {
                            $('.' + key).attr('href', val);
                        } else {
                            $('.' + key).html(val);
                        }
                    });
                }
            });
        });

        var kategori_id = $('select[name="kategori"]').val();
        if (kategori_id == 21) {
            $('.star, .spec, .destination').fadeOut();
        } else {
            $('.star, .spec, .destination').fadeIn();
        }


        $('select[name="kategori"]').change(function () {
            var val = $(this).val();
            if (val == 21) {
                $('.star, .spec, .destination').fadeOut();
            } else {
                $('.star, .spec, .destination').fadeIn();
            }
        });

        $('select[name="kategori_paket"]').change(function () {
            var val = $(this).val();
            if (val == 'Tour') {
                $('[name="email_pemilik_kapal"], [name="komisi_platform"]').prop('disabled', true);
            } else {
                $('[name="email_pemilik_kapal"], [name="komisi_platform"]').prop('disabled', false);
            }
        });

        /*        function return_status_change() {
                    var return_status = $('[name="return_status"]:checked').val();

                    if (return_status == 'yes') {
                        $('.return_status').show();
                    } else {
                        $('.return_status').hide();
                    }
                }*/

    });
</script>