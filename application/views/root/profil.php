<div id="content-judul">
	<span class="glyphicon glyphicon-cog"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
	$img = array(
			'src'=>'uploaded/content/'.$profil,
			'width'=>'240',
			'id'=>'gambar',
			'class'=>'img-thumbnail'
			);
	echo form_open_multipart('www/photo_profil/update', array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/photo_profil'));
?>
<table width="90%" border="0">
  <tr>
  	<td></td>
    <td valign="middle"><?php echo img($img); ?></td>
  </tr>
  <tr>
    <td>Profil Photo</td>
    <td><input type="file" name="userfile" onchange="read_image(this);"></td>
  </tr>
  <tr>
    <td height="54">&nbsp;</td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
<?php	echo anchor('www', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
    </td>
  </tr>
</table>
<?php
 echo form_close();
?>
		</div>
    </div>
</div>