<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 
      <?php
//------------------------------------------------------- EDIT ------------------------------------------//
      if ($method == 'edit') {
        echo tinymce();
        echo form_open_multipart('www/policy/update/' . $edit->partner_id, array('id' => 'update-file', 'title' => base_url() . 'www/policy/edit/' . $edit->partner_id));
        ?>
        <table width="100%" border="0">
          <tr>
            <td></td>
            <td><img src="<?php echo base_url('assets/img/root/pdf.jpeg') ?>"></td>
          </tr>
          <tr>
            <td>Policy File</td>
            <td>
              <input type="file" name="userfile" onchange="read_image(this)" />
              <br />
              <?php if ($edit->policy_file != '') { ?>
                <div class="row">
                  <div class="col-xs-1" style="padding-left: 10px;">
                    <a href="<?php echo base_url('uploaded/content/' . $edit->policy_file); ?>" target="_blank">
                      <img src="<?php echo base_url('assets/img/root/pdf.jpeg') ?>">
                    </a>
                  </div>
                  <div class="col-xs-11">
                    <a href="<?php echo base_url('www/policy/delete/' . $edit->partner_id); ?>" class="btn btn-danger">Delete</a>
                  </div>
                </div>                
                <a href="<?php echo base_url('uploaded/content/' . $edit->policy_file); ?>" target="_blank">
                  <?php echo $edit->policy_file ?>
                </a>
                
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td>Policy Desc</td>
            <td><textarea name="policy_text" id="tinymce" cols="" rows=""><?php echo $edit->policy_text; ?></textarea></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
              <?php echo anchor('www/partners', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
      }
      ?>
    </div>
  </div>
</div>