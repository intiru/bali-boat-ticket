<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Partners Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugin/validation/system/login.css">
  </head>
  <body>
    <noscript>
    <div class="noscript-2">
      <div class="noscript">Don't do something bad broo, Please Reactived Your Javascript!!</div>
    </div>
    </noscript>
    <div id="loading"></div>
    <div id="success"><span class="glyphicon glyphicon-ok"></span></div>
    <div id="error"><span class="glyphicon glyphicon-remove"></span></div>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-box">
            <div class="login-title">
              Sign In <span class="glyphicon glyphicon-warning-sign" style="float:right"></span> 
            </div>
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <?php echo form_open('partners/login', array('class' => 'form-send')); ?>
<!--                <div class="input-group">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-barcode"></span>
                  </span>
                  <input type="text" name="login_number" class="form-control" placeholder="Login Number ...">
                </div>
                <br />-->
                <div class="input-group">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-user"></span>
                  </span>
                  <input type="text" name="username" class="form-control" placeholder="Username ...">
                </div>
                <br />
                <div class="input-group">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                  </span>
                  <input type="password" name="password" class="form-control" placeholder="Password ...">
                </div>
                <br />
                <div class="btn-group btn-block">
                  <button type="submit" class="btn btn-success" style="width: 50%">Login</button>
                  <a href="<?php echo base_url('partners/forgot'); ?>" class="btn btn-primary" style="width: 50%">Forgot Password ?</a>
                </div>
                <div class="clearfix"></div>
                <br />
                <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/partners.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blocked.js"></script>
    <span class="base-value" data-base-url="<?php echo base_url(); ?>"></span>
  </body>
</html>
<!-- Created by : Mahendra Wardana Desain : 081934364063 (mahendra.adi.wardana@gmail.com)-->
