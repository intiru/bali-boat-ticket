<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 

      <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
      <?php
      $method = (empty($method)) ? '' : $method;
//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
      if ($method == 'list') {
        ?>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugin/datatables/datatables.min.css'); ?>">
        <script type="text/javascript" src="<?php echo base_url('assets/plugin/datatables/datatables.min.js'); ?>"></script>
        <div class="row">
          <div class="col-md-4">
            <?php
            echo anchor('partners/itinerary/list/'.$artikel_id, '<span class="glyphicon glyphicon-folder-open"></span> Itinerary', array('class' => 'btn btn-success'));
            echo '&nbsp&nbsp';
            ?>
          </div>
          <div class="col-md-8">
            <div class="pull-right">
              <form name="search" method="POST">
                <select name="s_type">
                  <option value="all" <?php echo ($search['s_type']=='all') ? 'selected':''; ?> >All Booking</option>
                  <option value="agent" <?php echo ($search['s_type']=='agent') ? 'selected':''; ?> >Travel Agent</option>
                  <option value="direct" <?php echo ($search['s_type']=='direct') ? 'selected':''; ?> >Personal / Direct</option>
                </select>
                  From <input type="text" name="s_from" id="datepicker-range-start" value="<?php echo $search['s_from'];?>">
                  To <input type="text" name="s_end" id="datepicker-range-end" value="<?php echo $search['s_end'];?>" >
                  <input type="submit" name="search" class="btn btn-success" value="Search">
              </form>
            </div>
          </div>
        </div>

        <br />
        <br />
        <script type="text/javascript">
          $(document).ready(function() {
            $('#datepicker-range-start').Zebra_DatePicker({
                pair: $('#datepicker-range-end')
            });
             
            $('#datepicker-range-end').Zebra_DatePicker({
                direction: 1
            });

            $('#table_id').DataTable({
              "paging":   true,
              "ordering": false,
              "searching":     false
            });

          });
        </script>

        <div class="table-wrapper" style="overflow: auto;">
        <table width="100%" data-page-length='25' id="table_id" class="display table table-striped table-hover table-responsive">
          <thead>
            <tr>
              <th width="4%" align="center">No.</th>
              <th>Tour Package</th>
              <th>Charter Date</th>
              <th>Contact Person</th>
              <th>Num Guests</th>
              <th width="10%" align="center">Menu</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($list as $row) {
              ?>
              <tr  id="row<?php echo $no; ?>">
                <td align="center"><?php echo $no++ . '.'; ?></td>
                <td><?php echo $row->artikel_title; ?></td>
                <td><?php echo date_format(date_create($row->charter_from),"d-m-Y"); ?> to <?php echo date_format(date_create($row->charter_end),"d-m-Y"); ?></td>
                <td><?php echo $row->full_name; ?></td>
                <td><?php echo $row->adults; ?> adults - <?php echo $row->childs; ?> childs - <?php echo $row->infants; ?> infants</td>
                <td align="center">
                  <?php echo anchor('partners/booking/edit/' .$artikel_id.'/'. $row->booking_id, '<span class="glyphicon glyphicon-pencil pencil" title="Detail Booking" data-toggle="tooltip"></span>'); ?>
                
                  <span onclick="hapus('<?php echo base_url(); ?>partners/booking/delete/<?php echo $artikel_id.'/'.$row->booking_id; ?>', '<?php echo $no - 1; ?>')">
                  <span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
//-------------------------------------------------------- CREATE ---------------------------------------------//
      } elseif ($method == 'create') {
        echo tinymce_3();
        echo form_open_multipart('partners/booking/insert/'.$artikel_id.'/'.$itinerary->artikel_id, array('id' => 'insert-file', 'title' => base_url() . 'partners/itinerary/list/'.$artikel_id));
        ?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
        <script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>

        <script>
          $(document).ready(function () {
            $('#calendar-from').fullCalendar({
            header: {
                  right: 'prev,next today',
                  center: 'title',
                  left: ''
                  //right: 'month,listMonth'
            },
              defaultDate: '<?php echo date('Y-m-d'); ?>',
              navLinks: false, // can click day/week names to navigate views
              businessHours: true, // display business hours
              editable: false,
              dayClick: function(date, jsEvent, view) {
                $('#charter_from').val(date.format());
                $('#modal-boat-available-from').modal('hide');
                $('#calendar').fullCalendar('unselect');
              },
              
              viewRender: function(currentView){
                  var minDate = moment();
                  // Past
                  if (minDate >= currentView.start && minDate <= currentView.end) {
                      $(".fc-prev-button").addClass('hide'); 
                  }
                  else {
                      $(".fc-prev-button").removeClass('hide'); 
                  }

              }
            });

            $('#calendar-end').fullCalendar({
            header: {
                  right: 'prev,next today',
                  center: 'title',
                  left: ''
                  //right: 'month,listMonth'
            },
              defaultDate: '<?php echo date('Y-m-d'); ?>',
              navLinks: false, // can click day/week names to navigate views
              businessHours: true, // display business hours
              editable: false,
              dayClick: function(date, jsEvent, view) {
                $('#charter_end').val(date.format());
                $('#modal-boat-available-end').modal('hide');
                $('#calendar').fullCalendar('unselect');
                
              },
              viewRender: function(currentView){
                  var minDate = moment();
                  // Past
                  if (minDate >= currentView.start && minDate <= currentView.end) {
                      $(".fc-prev-button").addClass('hide'); 
                  }
                  else {
                      $(".fc-prev-button").removeClass('hide'); 
                  }

              }
            });
          });
        </script>

        <style>

          #calendar {
            max-width: 900px;
            margin: 0 auto;
          }
          
          .fc-today-button {
            display: none;
          }
          
          .fc-day-number {
            font-weight: bold;
          }

          .fc-prev-button, .fc-next-button {
            font-weight: bold !important;
            text-transform: capitalize !important;
          }
          
          h1#title {
            margin-top: 0;
          }
        </style>
<div class="table-wrapper" style="overflow: auto;">
        <table width="100%" border="0">
             <tr>
               <td colspan="2">&nbsp;</td>
             </tr>
             <tr>
               <td colspan="2"><strong><font color="#FF0000">ONLINE BOOKING DATA</font></strong></td>
             </tr>
             <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="15%">Tour Package</td>
            <td>
              <?php echo $itinerary->artikel_title; ?>
            </td>
          </tr>
          <tr>
            <td>Charter Date</td>
            <td>
              <div style="float:left; ">
                <input type="text" id="charter_from" name="charter_from" class="form-control input-date-reserv-from" value="" style="width: 110px;" />
              </div> 
              <div style="float:left; padding:10px; "> to </div>
              <div style="float:left; ">
                <input type="text" id="charter_end" name="charter_end" class="form-control input-date-reserv-end" value="" style="width: 110px; float:left;" />
              </div>
            </td>
          </tr>
          <tr>
            <td>Agent Name</td>
            <td>
              <input type="text" name="agent_name" class="form-control" value="" style="width: 255px; float: left;" />
              <input type="checkbox" name="direct_booking" value="1" style="margin: 15px 0 0 15px;"> Personal / Direct
            </td>
          </tr>
          <tr>
            <td>Contact Person</td>
            <td>
              <input type="text" name="full_name" class="form-control" value="" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Address</td>
            <td>
              <input type="text" name="address" class="form-control" value="" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>
              <input type="text" name="phone" class="form-control" value="" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Email</td>
            <td>
              <input type="text" name="email" class="form-control" value="" style="width: 255px;" />
            </td>
          </tr>

          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><strong><font color="#FF0000">PICK UP INFORMATION</font></strong></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          
          
          <tr>
            <td colspan="2">1. Airport Pickup</td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Pickup Time</td>
            <td>
              <input type="text" name="ap_time" class="form-control timepicker" value="" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Flight Name</td>
            <td>
              <input type="text" name="ap_flight_name" class="form-control" value="" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td colspan="2">2. Hotel Pickup</td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Pickup Time</td>
            <td>
              <input type="text" name="hp_time" class="form-control timepicker" value="" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Hotel Name</td>
            <td>
              <input type="text" name="hp_name" class="form-control" value="" style="width: 230px;" />
            </td>
          </tr>
          
          
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><strong><font color="#FF0000">PASSENGER DETAILS</font></strong></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          

          <tr>
            <td colspan="2">

<table id="tbl">
    <tr>
        <td>Name</td>
        <td>Age Type</td> 
        <td>ID Number</td> 
        <td>Nationality</td> 
        <td>Stay (Hotel Name)</td> 
        <td>Flight</td> 
        <td></td>
    </tr>
    <tr>
        <td><input type="text" name="p_name[]" style="width: 230px;" /></td>
        <td>
          <select name="p_age_type[]">
            <option value="adult">Adult</option>
            <option value="child">Child</option>
            <option value="infant">Infant</option>
          </select>
        </td> 
        <td><input type="text" name="p_id[]" style="width: 150px;" /></td> 
        <td>
          <input type="text" name="nationality[]" style="width: 150px;" />
        </td>
        <td>
          <input type="text" name="p_stay[]" style="width: 180px;" />
        </td>
        <td>
          <input type="text" name="p_flight[]" style="width: 150px;" />
        </td>
        <td><input type="button" class="button" value="Clone" onclick="addField(this);" /></td>          
    </tr>
</table>

            </td>
          </tr>



          


          <tr class="spec hide">
            <td height="47" valign="top">Specification</td>
            <td><textarea name="spec" id="tinymce_3" cols="" rows=""></textarea></td>
          </tr>
          <tr>
            <td></td>
            <td>
              <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
              <?php echo anchor('partners/itinerary/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
            </td>
          </tr>
        </table>
</div>
        <div class="modal" id="modal-boat-available-from" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Date Avaibility</h4>
              </div>
              <div class="modal-body">
                <div id="calendar-from"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal" id="modal-boat-available-end" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Date Avaibility</h4>
              </div>
              <div class="modal-body">
                <div id="calendar-end"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          $(document).ready(function() {
            $('.input-date-reserv-from').click(function() {
              $('#modal-boat-available-from').modal('show');
              $('.modal-backdrop').hide();
              $('.fc-today-button').click();
              $('.fc-next-button').text('NEXT MONTH');
              $('.fc-prev-button').text('PREVIOUS MONTH');
            });

            $('.input-date-reserv-end').click(function() {
              $('#modal-boat-available-end').modal('show');
              $('.modal-backdrop').hide();
              $('.fc-today-button').click();
              $('.fc-next-button').text('NEXT MONTH');
              $('.fc-prev-button').text('PREVIOUS MONTH');
            });

          });

          function addField(n)
          {
            var tr = n.parentNode.parentNode.cloneNode(true);
            document.getElementById('tbl').appendChild(tr);
          }
        </script>
<script>
    $(document).ready(function(){
        $('input.timepicker').timepicker({});
    });
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <?php
        echo form_close();
//------------------------------------------------------- EDIT ------------------------------------------//
      } elseif ($method == 'edit') { 
        echo tinymce_3();
        echo form_open_multipart('partners/booking/update/' . $artikel_id .'/'. $edit->itinerary_id .'/'.$edit->booking_id, array('id' => 'update-file', 'title' => base_url() . 'partners/booking/list/'.$artikel_id));
        ?>
        
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        
<div class="table-wrapper" style="overflow: auto;">
         <table width="100%" border="0">
             <tr>
               <td colspan="2"><strong><font color="#FF0000">ONLINE BOOKING DATA</font></strong></td>
             </tr>
             <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="15%">Tour Package</td>
            <td><?php echo $edit->artikel_title; ?></td>
          </tr>
          <tr>
            <td>Charter Date</td>
            <td><?php echo date_format(date_create($edit->charter_from),"d-m-Y"); ?> to <?php echo date_format(date_create($edit->charter_end),"d-m-Y"); ?></td>
          </tr>
          <tr>
            <td>Agent Name</td>
            <td>
              <input type="text" name="agent_name" class="form-control" value="<?php echo $edit->agent_name; ?>" style="width: 255px; float: left;" />
              <input type="checkbox" name="direct_booking" value="1" <?php echo ($edit->direct_booking==1)? 'checked':''; ?> style="margin: 15px 0 0 15px;"> Personal / Direct
            </td>
          </tr>
          <tr>
            <td>Contact Person</td>
            <td>
              <input type="text" name="full_name" class="form-control" value="<?php echo $edit->full_name; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Address</td>
            <td>
              <input type="text" name="address" class="form-control" value="<?php echo $edit->address; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>
              <input type="text" name="phone" class="form-control" value="<?php echo $edit->phone; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Email</td>
            <td>
              <input type="text" name="email" class="form-control" value="<?php echo $edit->email; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>Adult</td>
            <td>
              <?php echo $edit->adults; ?>
            </td>
          </tr>
          <tr>
            <td>Child</td>
            <td>
              <?php echo $edit->childs; ?>
            </td>
          </tr>
          <tr>
            <td>Infant</td>
            <td>
              <?php echo $edit->infants; ?>
            </td>
          </tr>

          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><strong><font color="#FF0000">PICK UP INFORMATION</font></strong></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          
          
          <tr>
            <td colspan="2">1. Airport Pickup</td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Pickup Time</td>
            <td>
              <input type="text" name="ap_time" class="form-control timepicker" value="<?php echo $edit->ap_time; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Flight Name</td>
            <td>
              <input type="text" name="ap_flight_name" class="form-control" value="<?php echo $edit->ap_flight_name; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td colspan="2">2. Hotel Pickup</td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Pickup Time</td>
            <td>
              <input type="text" name="hp_time" class="form-control timepicker" value="<?php echo $edit->hp_time; ?>" style="width: 255px;" />
            </td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Hotel Name</td>
            <td>
              <input type="text" name="hp_name" class="form-control" value="<?php echo $edit->hp_name; ?>" style="width: 230px;" />
            </td>
          </tr>
          
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><strong><font color="#FF0000">PASSENGER DETAILS</font></strong></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr>
            <td colspan="2">

<table id="tbl">
    <tr>
        <td>Name</td>
        <td>Age Type</td> 
        <td>ID Number</td> 
        <td>Nationality</td>
        <td>Stay (Hotel Name)</td>
        <td>Flight</td> 
        <td></td>
    </tr>
    <?php 

      $passangers_data = json_decode($edit->passangers, true); 
      
      foreach ($passangers_data as $passanger) {

    ?>
    <tr>
        <td><input type="text" name="p_name[]" style="width: 230px;" value="<?php echo $passanger['name'];?>" /></td>
        <td>
          <select name="p_age_type[]">
            <option value="adult" <?php echo $passanger['age']=='adult' ? 'selected=selected' : '';?> >Adult</option>
            <option value="child" <?php echo $passanger['age']=='child' ? 'selected' : '';?> >Child</option>
            <option value="infant" <?php echo $passanger['age']=='infant' ? 'selected' : '';?> >Infant</option>
          </select>
        </td> 
        <td><input type="text" name="p_id[]" style="width: 150px;" value="<?php echo $passanger['id'];?>" /></td> 
        <td>
          <input type="text" name="nationality[]" style="width: 150px;" value="<?php echo $passanger['nationality'];?>" />
        </td>
        <td>
          <input type="text" name="p_stay[]" style="width: 180px;" value="<?php echo $passanger['stay'];?>" />
        </td>
        <td><input type="text" name="p_flight[]" style="width: 150px;" value="<?php echo $passanger['flight'];?>" /></td>
        <td><input type="button" class="button" value="Clone" onclick="addField(this);" /></td>          
    </tr>

        <script type="text/javascript">
          

          function addField(n)
          {
            var tr = n.parentNode.parentNode.cloneNode(true);
            document.getElementById('tbl').appendChild(tr);
          }
        </script>

    <?php
      }
    ?>

</table>

            </td>
          </tr>

          <!--<tr class="spec hide">
            <td height="47" valign="top">Specification</td>
            <td><textarea name="spec" id="tinymce_3" cols="" rows=""></textarea></td>
          </tr>-->
          <tr>
            <td></td>
            <td>
              <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
              <?php echo anchor('partners/booking/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
            </td>
          </tr>
        </table>
    </div>
    <script>
    $(document).ready(function(){
        $('input.timepicker').timepicker({});
    });
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

        <?php
        echo form_close();
      }
      ?>
    </div>
  </div>
</div>