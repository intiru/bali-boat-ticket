<?php
$find    = array(' ', '/', '&', '\\');
$replace = array('-', '-', '-', '-');
?>

<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/basic/jquery.qtip.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/basic/jquery.qtip.min.css" />

<script>

  $(document).ready(function() {

    $('#calendar').fullCalendar({
      header: {
        right: 'prev,next',
        center: 'title',
        left: 'myCustomButton'
      },
      defaultDate: '<?php echo date("Y-m-d");?>',
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: [
        <?php foreach ($list as $value) {
          $description = "<table><tr><td>Agent Name</td><td>".$value->agent_name."</td></tr><tr><td>Address</td><td>".$value->address."</td></tr><tr><td>Email</td><td>".$value->email."</td></tr><tr><td>Phone</td><td>".$value->phone."</td></tr></table>";

          $p_data = json_decode($value->passangers, true);
          $passangers = "<table border=1><tr><td>No.</td><td>Guest Name</td><td>Pass.Number</td><td>Age</td><td>Nationality</td></tr>";
          $i=1;
          foreach ($p_data as $passanger) {
            $passangers .= "<tr><td>".$i."</td><td>".$passanger['name']."</td><td>".$passanger['id']."</td><td>".$passanger['age']."</td><td>".$passanger['nationality']."</td></tr>";
            $i=$i+1;
          }
          $passangers .= "</table>";
          ?>
          {
            title: '<?php echo $value->artikel_title; ?>',
            start: '<?php echo $value->charter_from; ?>',
            end: '<?php echo $value->charter_end; ?>',
            description: '<?php echo $description.$passangers;?>',
            textEscape: false
          },
          <?php
        } ?>
      ],

      eventRender: function(eventObj, $el) {
        $el.qtip({
          content: eventObj.description,
          position: {
            my: 'bottom center',
            at: 'top center',
            target: 'mouse',
            viewport: $('#fullcalendar'),
            adjust: {
              mouse: false,
              scroll: false
            }
          },
        });
      },
      
      eventAfterAllRender: function(){
        $('.fc-next-button').text('NEXT MONTH');
        $('.fc-prev-button').text('PREVIOUS MONTH');
      },
      viewRender: function(currentView){
        var minDate = moment();
        // Past
        if (minDate >= currentView.start && minDate <= currentView.end) {
          $(".fc-prev-button").addClass('hide');
        } else {
          $(".fc-prev-button").removeClass('hide');
        }
      },
    });
  });

</script>

<?php
if (empty($level_tipe)) {
  $level_tipe = 'partner';
}
?>

<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

  .back {
    max-width: 900px;
  }

  .fc-day-number {
    font-weight: bold;
  }


  <?php if ($level_tipe == 'partner') { ?>
    .fc-myCustomButton-button {
      display: none
    } 
  <?php } else { ?>
    .fc-myCustomButton-button {
      background-image: none;
      background-color: red;
      color: white
    } 
  <?php } ?>

</style>


      <div id="content-judul">
        <span class="glyphicon glyphicon-calendar"></span> <?php echo $title; ?>
      </div>
      <div id="content-isi">
        <div class="col-md-12">
          <div id="halaman">
            <?php
              echo anchor('partners/calendar/'.$artikel_id, '<span class="glyphicon glyphicon-calendar"></span> BOAT CALENDAR', array('class' => 'btn btn-success'));
              echo '&nbsp&nbsp';
              echo anchor('partners/booking/list/'.$artikel_id, '<span class="glyphicon glyphicon-book"></span> BOOKING HISTORY', array('class' => 'btn btn-success'));
              echo '&nbsp&nbsp';
              echo anchor('partners/bookingcalendar/'.$artikel_id, '<span class="glyphicon glyphicon-book"></span> BOOKING CALENDAR', array('class' => 'btn btn-success'));
            ?>
            <br />
            <br />
            <div class="row">
              <div class="col-xs-12 col-sm-12" style="padding: 10px">
                <div id='calendar'></div>
              </div>
            </div>
            
            </div>
          </div>
        </div>
      </div>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker/css/bootstrap-datepicker.min.css">
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
      <script type="text/javascript">
                $(document).ready(function() {
              $('.datepicker').datepicker();
              });
      </script>

