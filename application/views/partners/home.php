<div id="content-judul">
  <span class="glyphicon glyphicon-th"></span> News
</div>
<div id="content-isi">
  <div class="col-md-9">
    <div id="content-menu">
      <h4><?php  echo $news->artikel_title ?></h4>
      <?php echo $news->artikel_isi ?>
    </div>
  </div>
  <div class="col-md-3 hide">
    <div id="info-bingkai">
      <div id="info-title">
        <span class="glyphicon glyphicon-list"></span> Root Contents
      </div>
      <div id="info-isi">
        <table width="100%" class="info">
          <tr>
            <td><?php //echo anchor('www/post', $this->mod_root->select_by_id('tb_artikel', 'posisi', 'product')->num_rows());  ?></td>
            <td><?php //echo anchor('www/post', 'Products');  ?></td>
          </tr>
          <tr>
            <td><?php //echo anchor('www/member', $this->mod_root->select('tb_member')->num_rows());  ?></td>
            <td><?php //echo anchor('www/member', 'Member');  ?></td>
          </tr>
          <tr>
            <td><?php //echo anchor('www/customer', $this->mod_root->select_by_id('tb_config', 'config_posisi', 'Yahoo')->num_rows());  ?></td>
            <td><?php //echo anchor('www/customer', 'Customer Service');  ?></td>
          </tr>
          <tr>
            <td><?php //echo anchor('www/galeri', $this->mod_root->select('tb_galeri')->num_rows());  ?></td>
            <td><?php //echo anchor('www/galeri', 'Photo Gallery');  ?></td>
          </tr>
          <tr>
            <td><?php //echo anchor('www/header', $this->mod_root->select('tb_gambar')->num_rows());  ?></td>
            <td><?php //echo anchor('www/header', 'Images Header');  ?></td>
          </tr>
          <tr>
            <td><?php //echo anchor('www/testimonial', $this->mod_root->select('tb_comment')->num_rows());  ?></td>
            <td><?php //echo anchor('www/testimonial', 'Guest Comment');  ?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div id="bersih"></div>
</div>