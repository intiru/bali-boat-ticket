<script type="text/javascript">
$(document).ready(function() {
  $('.category-title').addClass('hide');
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paypal.css') ?>">
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 content-2">
    <div class="row">

        <div class="col-xs-6 col-md-6">

            <div class="reserv-title">

                <img src="<?php echo base_url('assets/img/template/Personal.png'); ?>">ONLINE RESERVATION

            </div>

        </div>

        <div class="col-xs-6 col-md-6 text-right" style="padding-right: 15px">

            <div class="reserv-phone">

                <img src="<?php echo base_url('assets/img/template/Chatting.png'); ?>"> +62.81236016914

            </div>

        </div>

    </div>

    <br />

    <div class="reserv-judul">YOUR PAYMENT SUCCESS</div>
    Dear <?php echo $booking->title.' '.$booking->first_name.' '.$booking->last_name ?>.<br /><br />

    Warmest greeting form Boat Charter Komodo,<Br /><br />

    Thank you for reservation <?php echo $booking->artikel_nama ?>, package : <?php echo $booking->itinerary_title ?> using Boat Charter Komodo. Your detail tours, payment receipt and next invoice for your travel document has been sending to your email, Please check your email as soon as possible.<br /><br />

    Regard<br /><br />

    Marcelino Sunjaya<br />
    Operation Manager
    <br /><br />
        <img src="<?php echo base_url('assets/img/template/ssl-site.png') ?>">
  <ul class="nav nav-tabs hide" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Payment Receipt</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Invoice Next Payment</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Detail Tour</a></li>
  </ul>
  <div class="tab-content hide">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="row">
        <div class="col-xs-9">
          <br /><br />
          <img src="<?php echo base_url('assets/img/template/bck-text.png') ?>"><br />
          Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />
          Phone : +62.81239922222 Whatsapp: +62.81236016914<br/> 
          sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />
        </div>
        <div class="col-xs-3 text-center">
          <img src="<?php echo base_url('assets/img/template/BCK.png'); ?>" width="150">
        </div>
      </div>
      <center>
        <img src="<?php echo base_url('assets/img/template/separate.png'); ?>">
      </center>
      <div class="row">
        <div class="col-xs-12">
          <br />
          <center>
            <img src="<?php echo base_url('assets/img/template/payment_receipt.png') ?>">
          </center>
          <br />
          <hr class="separate" />
          <br />
          <div class="row">
            <div class="col-xs-6">
              <strong>Invoice ID</strong> : <?php echo $booking->payment_id ?><br />
              <strong>Date of Issued</strong> : <br />
              <strong>Athorization by</strong> : Admin
            </div>
            <div class="col-xs-6">
              <strong>Customer Name</strong> : <?php echo $booking->title.' '.$booking->first_name.' '.$booking->last_name ?><br />
              <strong>Phone</strong> : <?php echo $booking->telephone ?><br />
              <strong>Email</strong> : <?php echo $booking->email ?>
            </div>
          </div>
          <br /><br /><br />
        </div>
        <div class="col-xs-12">
          <img src="<?php echo base_url('assets/img/template/payment_info.png') ?>">
          <hr class="separate" />
          <strong>Payment Status</strong> : Pre-Payment (<?php echo $booking->persen_dp ?>%)<br />
          <strong>Payment Method</strong> : Paypal
          <br /><br /><br /><br />
          <img src="<?php echo base_url('assets/img/template/payment_details.png') ?>">
          <hr class="separate" />
          <strong>Booking </strong> : <?php echo $booking->artikel_nama ?><br />
          <table width="100%" border="1" class="reserv-table table-blue">
            <tr style="background-color: #ADE6F9">
              <td>Charter Date</td>
              <td>Package</td>
              <td>Adult</td>
              <td>Children</td>
              <td>Infant</td>
              <td>Amount</td>
            </tr>
            <tr>
              <td><?php echo $booking->date_in ?></td>
              <td><?php echo $booking->itinerary_title ?></td>
              <td><?php echo $booking->adult ?></td>
              <td><?php echo $booking->children ?></td>
              <td><?php echo $booking->infant ?></td>
              <td><?php echo strtoupper($booking->price_tipe).' '.$booking->total ?></td>
            </tr>
          </table>
          <br />
          <div style="text-align: right">
            <strong>Total Receipt</strong> : <?php echo strtoupper($booking->total) ?>
          </div>
        </div>
      </div>
          <br /><br />
          <div class="separate-2"></div>
          <center>
            BOAT CHARTER KOMODO | www.boatcharterkomodo.com | Phone: +62.812399.22222 | WA: +6281236016914
          </center>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <div class="row">
        <div class="col-xs-9">
          <br /><br />
          <img src="<?php echo base_url('assets/img/template/bck-text.png') ?>"><br />
          Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />
          Phone : +62.81239922222 Whatsapp: +62.81236016914<br/> 
          sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />
        </div>
        <div class="col-xs-3 text-center">
          <img src="<?php echo base_url('assets/img/template/BCK.png'); ?>" width="150">
        </div>
      </div>
      <center>
        <img src="<?php echo base_url('assets/img/template/separate.png'); ?>">
      </center>
      <div class="row">
        <div class="col-xs-12">
          <br />
          <center>
            <img src="<?php echo base_url('assets/img/template/invoice.png') ?>">
          <br /><br />
        </div>
        <div class="col-xs-12">
          <img src="<?php echo base_url('assets/img/template/invoice_info.png') ?>">
          <hr class="separate" />
        </div>
        <div class="col-xs-12 col-sm-6" style="padding-right: 20px">
          <strong>To:</strong><br />
          <?php echo $booking->title.' '.$booking->first_name.' '.$booking->last_name ?><br />
          <?php echo $booking->address ?><br />
          <?php echo $booking->country ?><br />
          Phone: <?php echo $booking->telephone ?><br />
          Email: <?php echo $bookin->email ?>
        </div>
        <div class="col-xs-12 col-sm-3">
          Invoice No<br />
          Invoice Date<br />
          Cut Of Date
        </div>
        <div class="col-xs-12 col-sm-3">
          : BCK-<?php echo $booking->payment_id ?><br />
          : <?php echo $booking->date_insert ?><br />
          : <?php echo $plus_date ?>
        </div>
        <div class="col-xs-12">
          <br />
          <img src="<?php echo base_url('assets/img/template/payment_info.png') ?>">
          <hr class="separate" />
        </div>
        <div class="col-xs-12 col-sm-12">
          <strong>Booking Status : </strong> Pending Payment<br />
          <strong>Payment Method : </strong> Paypal<br />
        </div>
        <div class="col-xs-12">
          <br />
          <img src="<?php echo base_url('assets/img/template/invoice_details.png') ?>">
          <hr class="separate" />
        </div>
        <div class="col-xs-12 col-sm-12">
          <strong>Booking : </strong> <?php echo $booking->artikel_nama ?><br /><br />
          <table width="100%" border="1" class="reserv-table">
            <tr style="background-color: #ADE6F9">
              <td>Charter Date</td>
              <td>Package</td>
              <td>Adult</td>
              <td>Children</td>
              <td>Infant</td>
              <td>Amount</td>
            </tr>
            <tr>
              <td><?php echo $booking->date_in ?></td>
              <td><?php echo $booking->itinerary_title ?></td>
              <td><?php echo $booking->adult ?></td>
              <td><?php echo $booking->children ?></td>
              <td><?php echo $booking->infant ?></td>
              <td><?php echo strtoupper($booking->price_tipe).' '.$booking->total ?></td>
            </tr>
          </table>
          <br />
          <div style="text-align: right">
            <strong>Required Deposit</strong> : <?php echo strtoupper($booking->price_tipe).' '.$booking->pay_now ?><br />
            <strong>Next Payment</strong> : <?php echo strtoupper($booking->price_tipe).' '.$booking->next_payment ?><br />
          </div>
        </div>
        <div class="col-xs-12">
          Payment Info / Bank Transfer<br /><br />
          <strong>Mandiri Bank</strong><br />
            Bank Name : PT. Bank Mandiri<br />
          Ac. Number : 145-000-4686-040<br />
          Bank Address : Jl. Udayana Denpasar - Bali - Indonesia<br />
          Beneficiary : Edelbertus Harianto<br />
          Swift Code : BMRIIDJA<br />
          <strong>Note:</strong>
          <ol>
          <li>Price not include international Bank Transfer</li>
          <li>Please send us the remittance of the transfer by email or WhatsApp : +62.81236016914</li>
          </ol>
          <br /><br />
          <div class="separate-2"></div>
          <center>
            BOAT CHARTER KOMODO | www.boatcharterkomodo.com | Phone: +62.812399.22222 | WA: +6281236016914
          </center>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
      <div class="row">
        <div class="col-xs-9">
          <br /><br />
          <img src="<?php echo base_url('assets/img/template/bck-text.png') ?>"><br />
          Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />
          Phone : +62.81239922222 Whatsapp: +62.81236016914<br/> 
          sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />
        </div>
        <div class="col-xs-3 text-center">
          <img src="<?php echo base_url('assets/img/template/BCK.png'); ?>" width="150">
        </div>
      </div>
      <center>
        <img src="<?php echo base_url('assets/img/template/separate.png'); ?>">
      </center>
      <div class="row">
        <div class="col-xs-12">
          <br />
          <center>
            <img src="<?php echo base_url('assets/img/template/tour_program.png') ?>">
          </center>
          <br />
          <div class="separate-2"></div>
          Boat : <?php echo $booking->artikel_nama ?><br />
          Package : <?php echo $booking->itinerary_artikel ?><Br />
          Periode : <?php echo $booking->date_in ?><br />
          Passanger : <?php echo 'Adult : '.$booking->adult.' Children : '.$booking->children.' Infant : '.$booking->infant ?><br /><br />

          <?php echo $artikel->artikel_isi; ?>
          <br />
          <?php echo $itinerary->artikel_isi; ?>
          <br />
          <div class="separate-2"></div>
          <center>
            BOAT CHARTER KOMODO | www.boatcharterkomodo.com | Phone: +62.812399.22222 | WA: +6281236016914
          </center>
        </div>
      </div>
    </div>
  </div>
</div>

<?php echo $sidebar ?>