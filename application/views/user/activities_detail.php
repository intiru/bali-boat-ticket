<div class="container clearfix" style="margin-top: -20px">


    <div class="pull-right">
        <button type="button" class="btn btn-primary btn-activity-filter-show">
            <i class="icon-search"></i> Change Search
        </button>
        <br/><br/>
    </div>

    <div class="hidden activities-filter">
        <?php echo $activities_filter ?>
    </div>
    <div id="jssor_1"
         style="position:relative;margin:0 auto;top:0px;left:0px;width:1100px;height:480px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin"
             style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                 src="../svg/loading/static-svg/spin.svg"/>
        </div>
        <div data-u="slides"
             style="cursor:default;position:relative;top:0px;left:0px;width:1100px;height:380px;overflow:hidden;">
            <div>
                <img data-u="image" src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"/>
                <img data-u="thumb" src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"/>
            </div>
            <?php foreach ($galeri as $r) { ?>
                <div>
                    <img data-u="image" src="<?php echo base_url('uploaded/galeri/' . $r->galeri_nama) ?>"/>
                    <img data-u="thumb" src="<?php echo base_url('uploaded/galeri/' . $r->galeri_nama) ?>"/>
                </div>
            <?php } ?>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort101"
             style="position:absolute;left:0px;bottom:0px;width:1100px;height:100px;background-color:#000;"
             data-autocenter="1" data-scale-bottom="0.75">
            <div data-u="slides">
                <div data-u="prototype" class="p" style="width:190px;height:84px;">
                    <div data-u="thumbnailtemplate" class="t"></div>
                    <svg viewBox="0 0 16000 16000" class="cv">
                        <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                        <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                        <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                    </svg>
                </div>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;"
             data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;"
             data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
            </svg>
        </div>
    </div>
    <br/>

    <div class="activities-card">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <h1 class="no-margin"><?php echo $post->artikel_title ?></h1>
                <div class="">
                    <i class="icon-location"></i> <?php echo $post->kategori_nama ?> &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="icon-time"></i> &nbsp;<?php echo $post->departure_time_from ?> - <?php echo $post->departure_time_to ?> &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="icon-eye"></i> <?php echo number_format($post->artikel_view) ?> views
                </div>
                <br/>
                <span class="badge badge-secondary"><?php echo $post->kategori_activities_nama ?></span>
            </div>
            <div class="col-xs-12 col-md-3 text-center">
                Starting From
                <br/>
                <span style="font-size: 26px">USD <?php echo number_format($post->artikel_harga) ?></span> /pax
                <br/>
                <br/>
                <button type="button" class="btn btn-primary btn-block btn-activities-book"><i class="icon-check-sign"></i> Fill Form then
                    Book
                </button>
            </div>
        </div>
        <div class="wrapper-activities-book hidden">
            <hr/>
            <form class="row" style="margin-top: 10px;">
                <div class="col-md-9">
                    <div class="form-row travel-date-group">
                        <div class="form-group col-md-4">
                            <label for="inputEmail4">Adults</label>
                            <input type="number" name="adult" class="form-control" min="1" value="1">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Childs</label>
                            <input type="number" name="child" class="form-control" value="0">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Visit Date</label>
                            <input type="text" name="date" value="<?php echo date('m-d-Y'); ?>" class="form-control tleft datepicker" placeholder=DD-MM-YYYY" style="height: 38px">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="btn-group btn-block" style="margin-top: 30px">
                        <button type="submit" value="reservation" class="btn btn-primary"><i class="icon-ok"></i> Book And Payment</button>
                        <button type="button" class="btn btn-default btn-activities-book-back">Back <i class="icon-forward"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="activities-card">
        <?php echo $post->general_description ?>
    </div>

    <div class="activities-card">
        <h3 class="no-margin">Transportation & Venue Info</h3>
        <?php echo $post->boat_spesification ?>
        <?php echo $post->diving_activities ?>
    </div>
</div>