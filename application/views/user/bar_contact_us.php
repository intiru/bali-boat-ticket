<div class="promo promo-light promo-full bottommargin-sm header-stick notopborder">
    <div class="container clearfix">
        <h3>Call us today at
            <span>
                <a href="https://api.whatsapp.com/send?phone=6282145997124&text=Hello,,%20i%20ask%20you%20something%20about%20Bali%20Boat%20Ticket"
                    target="_blank">
                    <?php echo $no_hp ?>
                </a>
            </span> or Email us at
            <span>
                <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></span></h3>
        <span>For fast response, you can call us with click the right side button.</span>
        <a href="<?php echo base_url('contact-us') ?>" class="button button-dark button-xlarge button-rounded">Contact Us</a>
    </div>
</div>