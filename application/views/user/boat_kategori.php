<?php echo $bar_contact_us ?>
<div class="container clearfix">
    <h1><?php echo $post->kategori_nama ?></h1>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <img src="<?php echo base_url('uploaded/content/'.$post->kategori_gambar) ?>" alt="<?php echo $post->kategori_nama ?>" title="<?php echo $post->kategori_nama ?>">
        </div>
        <div class="col-xs-12 col-md-9">
            <?php echo $post->artikel_isi ?>
        </div>
    </div>
    <hr />
    <h2 align="center"><?php echo $post->kategori_nama ?> / Boat List</h2>
    <div class="row">
        <?php foreach ($boats as $r) { ?>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 boat-item-wrapper"
                 style="margin-bottom:30px;padding:8px;">
                <a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>" class="boat-item">
                    <img class="gambar"
                         src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>"
                         height="200">
                    <h3 align="center"><?php echo $r->artikel_title ?></h3>
                    <button type="button" class="button button-3d button-rounded button-aqua"><i class="icon-info-sign"></i>Detail Info & Photos</button>
                    <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>Book Now</button>
                    <br/><br/>
                </a>
            </div>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        var top = parseInt($(".promo-light").offset().top) - parseInt(100);

        $('html, body').animate({
            scrollTop: top
        }, 1000);
    });
</script>