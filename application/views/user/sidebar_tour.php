<?php
$find = array(' ', '/', '&', '\\');
$replace = array('-', '-', '-', '-');
?>
<aside>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-tour">
      <div class="sidebar-title">Related Tour Packages</div>
      <br />
      <?php foreach($related_tour as $r) { ?>
      <div class="row">
        <div class="col-xs-3">
          <a href="<?php echo base_url(str_replace($find, $replace, strtolower($r->artikel_title))); ?>">
            <img src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar); ?>" class="img-responsive" />
          </a>
        </div>
        <div class="col-xs-7" style="padding-left:14px;font-size:14px;">
          <a href="<?php echo base_url(str_replace($find, $replace, strtolower($r->artikel_title))); ?>" style="color:black;">
            <?php echo $r->artikel_title; ?>
          </a>
          <br />
          <a href="<?php echo base_url(str_replace($find, $replace, strtolower($r->artikel_title))); ?>" style="color:#F64708;font-size:12px">DETAIL</a>
        </div>
        <div class="col-xs-10">
          <hr />
        </div>
      </div>
      <?php } ?>
    </div>
  </aside>