<div class="container clearfix" style="margin-top: -20px">

    <div class="row">
        <div class="col-xs-12 col-md-8">

            <form action="<?php echo base_url('nias/reservation_activity_send') ?>" method="post" class="form-send">
                <input type="hidden" name="date" value="<?php echo $date ?>">
                <input type="hidden" name="total_visitor" value="<?php echo $total_visitor ?>">
                <input type="hidden" name="total_price" value="<?php echo $total_price ?>">
                <input type="hidden" name="adult" value="<?php echo $adult ?>">
                <input type="hidden" name="child" value="<?php echo $child ?>">
                <input type="hidden" name="artikel_title" value="<?php echo $activity->artikel_title ?>">
                <input type="hidden" name="artikel_harga" value="<?php echo $activity->artikel_harga ?>">
                <div class="activities-card">
                    <h4>Contact Details (for E-ticket/Voucher)</h4>
                    <hr/>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input type="text" name="phone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Message</label>
                        <textarea class="form-control" name="message"></textarea>
                    </div>
                    <div class="form-group">
                        <label></label>
                        <?php echo $captcha ?>
                    </div>
                    <div class="form-group">
                        <label>Security Code</label>
                        <input type="text" class="form-control" name="captcha">
                    </div>
                </div>

                <div class="activities-card">
                    <h4>Visitor / Traveler Detail</h4>
                    <hr/>
                    <?php for ($i = 1; $i <= $adult; $i++) { ?>
                        <h3 class="no-margin">Adult <?php echo $i ?></h3>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="inputEmail4">Title</label>
                                <select name="adult_title[]" class="form-control select2" required>
                                    <option value="Mr.">Mr.</option>
                                    <option value="Ms.">Ms.</option>
                                    <option value="Mrs.">Mrs.</option>
                                </select>
                            </div>
                            <div class="form-group col-md-7">
                                <label for="inputPassword4">Name</label>
                                <input type="text" name="adult_name[]" class="form-control" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputPassword4">Nationality</label>
                                <select name="adult_nationality[]" class="form-control select2" required>
                                    <?php require('negara_list.php') ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                    <?php for ($i = 1; $i <= $child; $i++) { ?>
                        <h3 class="no-margin">Child <?php echo $i ?></h3>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Name</label>
                                <input type="text" name="child_name[]" class="form-control" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputPassword4">Nationality</label>
                                <select class="form-control select2" name="child_nationality[]" required>
                                    <?php require('negara_list.php') ?>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputPassword4">Birthday</label>
                                <input type="text"
                                       name="child_birthday[]"
                                       value="<?php echo date('m-d-Y'); ?>"
                                       class="form-control tleft datepicker" placeholder=DD-MM-YYYY"
                                       style="height: 38px"
                                       required>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-activities-book">
                    <i class="icon-ok"></i> Send Reservation
                </button>
            </form>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="activities-card">
                <h4>Activities Ticket</h4>
                <br/>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Activities Name</td>
                        <td><?php echo $activity->artikel_title ?></td>
                    </tr>
                    <tr>
                        <td>Visit Date</td>
                        <td><?php echo $date; ?></td>
                    </tr>
                    <tr>
                        <td>Total Visitor</td>
                        <td>
                            <strong><?php echo $total_visitor ?> Pax</strong>
                            <br/>
                            = <?php echo $adult ?> Adults & <?php echo $child ?> Child
                        </td>
                    </tr>
                    <tr style="background-color: #1E88E5; font-weight: bold; font-size: 22px; color: white">
                        <td colspan="2">Total Price : USD <?php echo number_format($total_price) ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>