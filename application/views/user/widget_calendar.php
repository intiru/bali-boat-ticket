<html>
<head>
	<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#calendar').fullCalendar({
				header: {
					right: 'prev,next',
					center: 'title',
					left: 'myCustomButton'
				    //right: 'month,listMonth'
				},
				defaultDate: '<?php echo date('Y-m-d'); ?>',
				    navLinks: true, // can click day/week names to navigate views
				    businessHours: true, // display business hours
				    editable: false,
				    navLinkDayClick: function(date, jsEvent) {
					    console.log('day', date.format()); // date is a moment
					    console.log('coords', jsEvent.pageX, jsEvent.pageY);
						},
					events: [
							<?php
							foreach ($date as $k => $v) {
								if ($v['status'] == 'available') {
									?>
									{
										title: 'AVAILABLE',
										start: '<?php echo $k ?>',
										color: '#2196f3'
									},
									<?php } else if ($v['status'] == 'open_trip') { ?>
										{
											title: 'OPENTRIP',
											start: '<?php echo $k ?>',
											color: '#f4b042'
										},
										<?php } else { ?>
											{
												title: 'SOLD',
												start: '<?php echo $k ?>',
												color: '#ffcdd2'
											},
											<?php } ?>
											<?php } ?>
					],
					eventRender: function (event, element, view) {
						  	element.find('span.fc-title').html(element.find('span.fc-title').text());
						  	element.find('.fc-list-item-title.fc-widget-content a').html(element.find('.fc-list-item-title.fc-widget-content a').text());
						  },
					eventAfterAllRender: function(){
						$('.fc-next-button').text('NEXT MONTH');
						$('.fc-prev-button').text('PREVIOUS MONTH');
					},
  					viewRender: function(currentView){
  						var minDate = moment();
			            // Past
			            if (minDate >= currentView.start && minDate <= currentView.end) {
			            	$(".fc-prev-button").addClass('hide');
			            }
			            else {
			            	$(".fc-prev-button").removeClass('hide');
			            }

        			},
    		});
		});
	</script>
</head>

<body>

	<div id="content-isi">
		<div class="col-md-12">
			<div id="halaman">
				<div class="row" style="text-align: center">
					<!--<h1><?php echo $title; ?></h1>-->
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12" style="padding: 10px">
						<div id='calendar'></div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>