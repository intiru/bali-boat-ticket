<div class="content-wrap">
    <div class="promo promo-light promo-full bottommargin-sm header-stick notopborder">
        <div class="container clearfix">
            <h3>Call us today at <span>+91.22.57412541</span> or Email us at
                <span>support@baliboatticket.com</span></h3>
            <span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span>
            <a href="#" class="button button-dark button-xlarge button-rounded">Contact Us</a>
        </div>
    </div>

    <div class="container clearfix">
        <h1>D'Camel</h1>
        <div class="col_full bottommargin-sm clearfix">

            <div class="masonry-thumbs grid-6" data-big="1" data-lightbox="gallery">
                <a href="<?php echo base_url() ?>assets/template/images/bbt/1.jpg" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo base_url() ?>assets/template/images/bbt/1.jpg" alt="Gallery Thumb 1"></a>
                <a href="<?php echo base_url() ?>assets/template/images/bbt/2.jpg" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo base_url() ?>assets/template/images/bbt/2.jpg" alt="Gallery Thumb 2"></a>
                <a href="<?php echo base_url() ?>assets/template/images/bbt/3.jpg" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo base_url() ?>assets/template/images/bbt/3.jpg" alt="Gallery Thumb 3"></a>
                <a href="<?php echo base_url() ?>assets/template/images/bbt/4.jpg" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo base_url() ?>assets/template/images/bbt/4.jpg" alt="Gallery Thumb 4"></a>
                <a href="<?php echo base_url() ?>assets/template/images/bbt/5.jpg" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo base_url() ?>assets/template/images/bbt/5.jpg" alt="Gallery Thumb 5"></a>
                <a href="<?php echo base_url() ?>assets/template/images/bbt/6.jpg" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo base_url() ?>assets/template/images/bbt/6.jpg" alt="Gallery Thumb 6"></a>
            </div>

        </div>

        <p style="text-align: justify;">
            Services depart from Sanur beach and include <strong>free hotel transfers</strong> to and from most areas in South Bali, including <strong>Kuta, Seminyak, Sanur, Legian, Nusa Dua and Jimbaran.</strong>&nbsp; Hotel pick up times vary depending on location but can be up to 1 and half hours before departure time. If your hotel is outside of the free transfer area and you would like D'camel to pick you up there will be an additional charge payable on the day. Contact details will be on your e-tickets. If you´re arranging your own transport then check in&nbsp;time is 30 minutes before departure at Pantai Sanur.</p>
        <p style="text-align: justify;">
            D'Camel depart from&nbsp;<strong>Sanur</strong>&nbsp;beach in south Bali (near Denpasar) and offer 3 daily crossings from Bali to Lembongan and back. The total crossing time is just 20-30 minutes.&nbsp;<strong>Free Bali hotel pickup</strong>&nbsp;is included from&nbsp;<strong>Sanur, Kuta, Ubud Central, Jimbaran, Denpasar Airport, Nusa Dua and Legian</strong>. Outside these areas an extra fee will be quoted by email, with the following as guidelines:</p>
        <ul>
            <li style="text-align: justify;">
                Outer Ubud areas (not Ubud central ) Payangan, Tegalalang, Pejeng - IDR 100.000/car</li>
            <li style="text-align: justify;">
                Pick up and drop off service to/from Canggu, Sawangan, Pecatu, Uluwatu, Balangan, Padang-Padang -&nbsp;IDR 150.000/car</li>
            <li style="text-align: justify;">
                <strong>Please note</strong>: Free central Ubud pickup is available from 2 passengers or more. 1 Passenger only will be charged&nbsp;IDR 50.000</li>
        </ul>
        <p style="text-align: justify;">
            <strong>Shuttle service times are set for the following areas:</strong></p>
        <table border="1" cellpadding="1" cellspacing="1" style="width: 960px;">
            <tbody>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <strong>Zone are pick up / drop off</strong></td>
                <td>
                    <strong>Time pick up</strong></td>
                <td>
                    <strong>Departure From</strong></td>
            </tr>
            <tr>
                <td>
                    <strong>1</strong></td>
                <td>
                    Uluwatu, Sawangan, Ubud</td>
                <td>
                    07.00 AM - 07.30 AM</td>
                <td>
                    Sanur at 09:30 AM</td>
            </tr>
            <tr>
                <td>
                    <b>2</b></td>
                <td>
                    Kuta, Seminyak, Nusa Dua, Legian, Jimbaran, Denpasar</td>
                <td>
                    07.30 AM - 08.00 AM</td>
                <td>
                    Sanur at 09:30 AM</td>
            </tr>
            <tr>
                <td>
                    <b>3</b></td>
                <td>
                    Sanur Area</td>
                <td>
                    08.30 AM - 08.45 AM</td>
                <td>
                    Sanur at 09:30 AM</td>
            </tr>
            </tbody>
        </table>
        <p style="text-align: justify;">
            The 80-seater boat is powered by 5 250HP Suzuki Engines, is fully insured and features ample safety equipment and facilities as follows:</p>
        <ul>
            <li>
                3 Units Life Raft</li>
            <li>
                90 Life Jackets</li>
            <li>
                4 Units Life Rings</li>
            <li>
                First Aid Kit Box</li>
            <li>
                Insurance for Passengers by PT. Jasa Raharja Putra Insurance</li>
            <li>
                Free Mineral water</li>
            <li>
                Toilet on board</li>
        </ul>
        <p>
            &nbsp;</p>
        <div class="text-center bottommargin-sm">
            <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">D'camel Fast Ferry Schedule</button>
            <button class="btn btn-success btn-book">Book</button>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-body">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" align="center" id="myModalLabel">D'camel Fast Ferry Schedule</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                        <tr class="tabs heading tabs_51 first"><th colspan="100"><a class="table-tab active" data-image="/uploads/content/maps/dcamel-route-map.jpg" data-schedule="22" data-company="51" href="javascript:;"><span>D'Camel Fast Ferry</span></a></th></tr><tr class="schedule_22 company_51" style="display: table-row;">
                                            <th scope="col">
                                                Departure<br>
                                                point Bali:</th>
                                            <th scope="col">
                                                Distance<br>
                                                to Kuta:</th>
                                            <th scope="col">
                                                Departing at:</th>
                                            <th scope="col">
                                                Stopping&nbsp;<br>
                                                at:</th>
                                            <th scope="col">
                                                Arrival Time:</th>
                                            <th scope="col">
                                                Departing<br>
                                                &nbsp;at:</th>
                                            <th scope="col">
                                                Stopping<br>
                                                at:</th>
                                            <th scope="col">
                                                Arrival Time:</th>
                                            <th scope="col">
                                                Bag limits:&nbsp;&nbsp;</th>
                                            <th scope="col">
                                                Approx<br>
                                                Ticket Prices:</th>
                                        </tr><tr class="schedule_22 company_51" style="display: table-row;">
                                            <td>
                                                Sanur<br>
                                                Beach</td>
                                            <td>
                                                30 mins</td>
                                            <td>
                                                <p>
                                                    09:30<br>
                                                    14:00<br>
                                                    17:00</p>
                                            </td>
                                            <td>
                                                <p>
                                                    Lembongan</p>
                                            </td>
                                            <td>
                                                10:00<br>
                                                14:30<br>
                                                17:30</td>
                                            <td>
                                                08:30<br>
                                                12:30<br>
                                                16:00</td>
                                            <td>
                                                Bali<br>
                                                (Sanur)</td>
                                            <td>
                                                09:00<br>
                                                13:00<br>
                                                16:30</td>
                                            <td>
                                                25kg per<br>
                                                person</td>
                                            <td>
                                                US$25<br>
                                                $42 rtn</td>
                                        </tr></tbody>
                                    </table>
                                </div>
                                <br />
                                <div class="tablemaps double-margin">
                                    <div class="row-fluid">
                                        <div class="center">
                                            <img id="large-map" src="<?php echo base_url() ?>assets/template/images/bbt/map.jpg" width="100%" alt="map">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <img src="<?php echo base_url() ?>assets/template/images/bbt/map.jpg" width="100%">
    </div>