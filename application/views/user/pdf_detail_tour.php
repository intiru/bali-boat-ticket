<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap_4/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paypal.css') ?>">
<style>@page {
        margin: 0px 12px 12px 12px;
    }
</style>
<div class="row">
    <div class="col-xs-8">
        <br /><br /><br />
        <img src="<?php echo base_url('assets/img/template/bck-text.png') ?>"><br />
        Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />
        Phone : +62.81239922222 Whatsapp: +62.81236016914<br/>
        sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />
    </div>
    <div class="col-xs-3 text-center">
        <img src="<?php echo base_url('assets/img/template/BCK.png'); ?>" width="150">
    </div>
</div>
<div class="text-center">
  <img src="<?php echo base_url('assets/img/template/separate.png'); ?>">
</div>
<div class="row">
  <div class="col-xs-12">
    <br />
      <div class="text-center">
          <img src="<?php echo base_url('assets/img/template/tour_program.png') ?>">
      </div>
    <br />
    <div class="separate-2"></div>
    Boat : <?php echo $booking->artikel_nama ?><br />
    Package : <?php echo $booking->itinerary_title ?><Br />
    Periode : <?php echo date('d F Y', strtotime($booking->date_in)) ?><br />
      Guest Name : <?php echo $booking->title.' '.$booking->first_name.' '.$booking->last_name ?><br />
    Passanger : <?php echo 'Adult : ' . $booking->adult . ' Children : ' . $booking->children . ' Infant : ' . $booking->infant ?><br /><br />

    <?php echo $artikel->artikel_remark; ?>
    <br />
    <?php echo $itinerary->artikel_isi; ?>
    <br />
  </div>
</div>