<?php
$activities_area = $this->db->order_by('kategori_nama', 'ASC')->get('tb_kategori')->result();
$activities_list = $this->db
    ->join('tb_kategori_activities', 'tb_kategori_activities.kategori_id = tb_artikel.kategori_activities_id')
    ->where(array(
    'posisi' => 'artikel',
    'link' => 'activities'
))
    ->order_by('artikel_title', 'ASC')
    ->get('tb_artikel')
    ->result();
$type = $this->session->userdata('type');
$type_id = $this->session->userdata('type_id');
?>

<div class="activities-card">
    Enter Destination Name Or Activity
    <form class="row" action="<?php echo base_url('activities/area') ?>" style="margin-top: 10px;">
        <div class="col-md-10">
            <select class="select2 form-control" name="id" style="width: 100%">
                <?php foreach ($activities_area as $r) { ?>
                    <option value="<?php echo 'area,' . $r->kategori_id ?>"
                        <?php echo $type == 'area' && $r->kategori_id == $type_id ? 'selected':'' ?>>
                        (Area) <?php echo $r->kategori_nama . ', ' . $r->kategori_location ?>
                    </option>
                <?php } ?>
                <?php foreach ($activities_list as $r) { ?>
                    <option value="<?php echo 'act,' . $r->artikel_id ?>"
                        <?php echo $type == 'act' && $r->artikel_id == $type_id ? 'selected':'' ?>>
                        (<?php echo $r->kategori_nama ?>) <?php echo $r->artikel_title ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Filter</button>
        </div>
    </form>
</div>