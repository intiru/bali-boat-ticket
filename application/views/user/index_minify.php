<?php echo doctype('html5'); ?>

<head>
    <title><?php echo $post->meta_title; ?></title>
    <?php
    $meta = array(
        array('name' => 'description', 'content' => $post->meta_description),
        array('name' => 'keywords', 'content' => $post->meta_keywords),
        array('name' => 'revisit-after', 'content' => '2 days'),
        array('name' => 'robots', 'content' => 'index, follow'),
        array('name' => 'rating', 'content' => 'General'),
        array('name' => 'author', 'content' => base_url()),
        array('name' => 'charset', 'content' => 'ISO-8859-1', 'type' => 'equiv'),
        array('name' => 'content-language', 'content' => 'English', 'type' => 'equiv'),
        array('name' => 'MSSmartTagsPreventParsing', 'content' => 'true'),
    );

    echo meta($meta);
    ?>
    <meta name="google-site-verification" content="my_UkRIvUjQptsU9aluSN1E0QpBpQ77SZ-IgxKjPK2M"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php header('Content-Type: text/html; charset=utf-8'); ?>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>uploaded/content/<?php echo $favicon ?>"/>
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>uploaded/content/<?php echo $favicon; ?>" sizes="16x16"
          type="image/png"/>

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"
          rel="stylesheet" type="text/css"/>
<?php if($post->link == 'tour') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/detail_tour.min.css') ?>">
<?php } elseif($post->link == 'search_boat') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/search_boat.min.css') ?>">
<?php } elseif($post->link == 'fast_boat') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fast_boat.min.css') ?>">
<?php } elseif($post->link == 'term') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/term.min.css') ?>">
<?php } elseif($post->link == 'travel_news') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/travel_news.min.css') ?>">
<?php } elseif($post->link == 'how_book') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/how_book.min.css') ?>">
<?php } elseif($post->link == 'contact') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/contact.min.css') ?>">
<?php } elseif($post->link == 'detail_news') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/detail_news.min.css') ?>">
<?php } elseif($post->link == 'reservation') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/reservation.min.css') ?>">
<?php } else { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/general.min.css') ?>">
<?php } ?>

    <style type="text/css">
        #footer *{
            color: rgba(255,255,255,0.9);
        }
        #copyrights {
            background-color: #fcb11b;
        }
    </style>
</head>
<body class="stretched">
<div id="wrapper" class="clearfix">
    <header id="header" class="full-header">
        <div id="header-wrap">
            <div class="container clearfix">
                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                <div id="logo" style="padding: 0 !important; border: none; padding: 10px 0 !important;">
                    <a href="<?php echo base_url() ?>" style="padding: 0 !important;">
                        <img src="<?php echo base_url() ?>assets/template/images/logo-bali-boat-ticket.png" alt="Bali Boat Ticket Logo" style="height: 80px !important;">
                    </a>
                </div>
                <nav id="primary-menu">
                    <ul>
                        <li <?php echo $post->link == 'home' ? ' class="current"' : '' ?>>
                            <a href="<?php echo base_url() ?>">
                                <div>Home</div>
                            </a>
                        </li>
                        <li <?php echo in_array($post->link, array('fast_boat', 'tour')) ? ' class="current"' : '' ?>>
                            <a href="<?php echo base_url('fast-boats') ?>">
                                <div>Fast Boats</div>
                            </a>
                            <!--<ul>
                                <?php /*foreach($kategori as $r) { */?>
                                <li>
                                    <a href="<?php /*echo $this->base_value->permalink(array($r->kategori_nama)) */?>">
                                        <div><i class="icon-minus"></i><?php /*echo $r->kategori_nama */?></div>
                                    </a>
                                </li>
                                <?php /*} */?>
                            </ul>-->
                        </li>
                        <li <?php echo $post->link == 'term' ? ' class="current"' : '' ?>><a href="<?php echo base_url('term-and-conditions') ?>">
                                <div>Term & Conditions</div>
                            </a></li>
                        <li <?php echo in_array($post->link, array('travel_news','detail_news')) ? ' class="current"' : '' ?>><a href="<?php echo base_url('travel-news') ?>">
                                <div>Travel News</div>
                            </a></li>
                        <li <?php echo $post->link == 'how_book' ? ' class="current"' : '' ?>><a href="<?php echo base_url('how-to-book') ?>">
                                <div>How to Book</div>
                            </a></li>
                        <li <?php echo $post->link == 'contact' ? ' class="current"' : '' ?>><a href="<?php echo base_url('contact-us') ?>">
                                <div>Contact Us</div>
                            </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <section class="slider-element">
        <div class="slider-parallax-inner">
            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="container">
                            <div class="col-xs-12 col-sm-12 col-md-7">
                                <div class="postcontent form-book">
                                    <h2 align="center">Search your fast boat</h2>
                                    <form class="form-booking" action="<?php echo base_url('search-boat') ?>"
                                          method="get">
                                        <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio"
                                                       name="way"
                                                       id="inlineRadio1"
                                                       value="one"
                                                       <?php echo $way == 'one' || $way == '' ? 'checked':'' ?>> One Way
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <label class="radio-inline">
                                                <input type="radio"
                                                       name="way"
                                                       id="inlineRadio2"
                                                       value="return"
                                                       <?php echo $way == 'return' ? 'checked':'' ?>> Return
                                            </label>
                                        </div>
                                        <div class="wrapper-step-1">
                                            <div class="form-group">
                                                <label>From</label>
                                                <select name="from" class="form-control">
                                            <?php foreach($destination as $r) { ?>
                                                    <option value="<?php echo $r->id ?>"
                                                        <?php echo $from == $r->id ? 'selected':'' ?>>
                                                        <?php echo $r->destination ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>To</label>
                                                <select name="to" class="form-control">
                                                    <?php foreach($destination as $r) { ?>
                                                        <option value="<?php echo $r->id ?>"
                                                            <?php echo $to == $r->id ? 'selected':'' ?>>
                                                            <?php echo $r->destination ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="text-center">
                                                <button type="button" class="btn btn-primary mt-3 btn-step-1">Next
                                                </button>
                                            </div>
                                            <br/>
                                            <small>
                                                <strong>Note:</strong> This trip is not recommended if you are:
                                                * under 2 years old * Pregnant * have heart or back problems or other
                                                physical impediments * All prices are in US $
                                            </small>
                                        </div>
                                        <div class="wrapper-step-2 hidden">
                                            <div class="form-row">
                                                <div class="form-group">
                                                    <div class="input-daterange travel-date-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label>Departure</label>
                                                            </div>
                                                            <div class="col-md-6 label-return">
                                                                <label>Return</label>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-daterange input-group">
                                                                    <input type="text"
                                                                           name="departure"
                                                                           class="form-control tleft"
                                                                           placeholder="MM/DD/YYYY"
                                                                           required
                                                                           value="<?php echo $departure ?>">
                                                                    <input type="text"
                                                                           name="return"
                                                                           class="form-control tleft"
                                                                           placeholder="MM/DD/YYYY"
                                                                           value="<?php echo $return ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">Adults</label>
                                                    <input type="number"
                                                           name="adults"
                                                           class="form-control"
                                                           value="<?php echo $adults ? $adults : 1 ?>"
                                                           min="1">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputPassword4">Children</label>
                                                    <input type="number"
                                                           name="children"
                                                           class="form-control"
                                                           value="<?php echo $children ? $children : 0 ?>"
                                                           min="0">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputPassword4">Infants</label>
                                                    <input type="number"
                                                           name="infant"
                                                           class="form-control"
                                                           value="<?php echo $children ? $children : 0 ?>""
                                                           min="0">
                                                </div>

                                            </div>

                                            <div class="text-center">
                                                <button type="button" class="btn btn-warning mt-3 btn-step-1-prev">
                                                    Previous
                                                </button>
                                                <button type="submit" class="btn btn-primary mt-3 btn-step-1">Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <section id="content">
        <div class="content-wrap">
            <?php echo $content ?>
            <div class="section parallax kategori-list hidden"
                 style="background-image: url('<?php echo base_url() ?>assets/template/images/parallax/3.jpg'); padding: 50px 0;"
                 data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -150px;">
                <div class="container">
                    <div class="row">
                        <?php for ($i = 1; $i <= 6; $i++) { ?>
                            <a href="" class="col-xs-6 col-sm-3 col-md-2 text-center">
                                <img src="<?php echo base_url() ?>assets/template/images/bbt/Icon-komodo-liveaboard.jpg"
                                     alt="PHINISI LIVEABOARD"
                                     class="img-circle">
                                <br/>
                                Phinisi Liveaboard
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12 text-center">
                        <hr />
                        <h3 align="center" class=" topmargin-sm">
                            Why Choose Us ?
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4 text-center">
                        <img src="<?php echo base_url() ?>assets/template/images/bbt/price-icon.png" height="84"><br>
                        <strong><?php echo $reason->artikel_title ?></strong><br>
                        <?php echo $reason->artikel_isi ?>
                    </div>
                    <div class="col-xs-12 col-md-4 text-center">
                        <img src="<?php echo base_url() ?>assets/template/images/bbt/experience-icon.png"
                             height="84"><br>
                        <strong><?php echo $experience->artikel_title ?></strong><br>
                        <?php echo $experience->artikel_isi ?>
                    </div>
                    <div class="col-xs-12 col-md-4 text-center">
                        <img src="<?php echo base_url() ?>assets/template/images/bbt/support-icon.jpg" height="84"><br>
                        <strong><?php echo $support->artikel_title ?></strong><br>
                        <?php echo $support->artikel_isi ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer id="footer" style="background-color: #FFC548">
        <div class="container">
            <div class="footer-widgets-wrap row">

                <div class="col-xs-12 col-sm-4 col-md-4 widget widget_links">
                    <h4>Travel News</h4>
                    <ul>
                        <?php foreach($footer_news as $r) { ?>
                            <li><a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>"><?php echo $r->artikel_title ?></a></li>
                        <?php } ?>
                    </ul>
                    <div style="margin-top: 10px;">
                        <a href="<?php echo base_url('travel-news') ?>"
                           class="button button-3d button-mini button-rounded button-white button-light"
                           style="color: black !important">
                            <i class="icon-arrow-right"></i> All Travel News
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 widget" style="margin-top: 0px;">
                    <h4>Quick Info </h4>
                    <p style="text-align: left;"><strong>Bali Sales Office<br></strong>Jalan Benesari Pantai Kuta Bali Indonesia, 80361<br><strong>P : +62 361 4726382</strong>, + 62 82145997124 <br><strong>Email</strong>&nbsp;: sales@komodoflorestrip.com<br>kcwtours@gmail.com</p>
                    <p style="text-align: left;"><strong>Labuan Bajo Office&nbsp;</strong><br>Jalan Soekarno Hatta Labuan Bajo, Flores East Nusa Tenggara - Indonesia<br>Call / WA : + 62 81 338 126 451</p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 widget" style="margin-top: 0px;">
                    <h4>Payments</h4>
                    <a href="" target="_blank">
                        <img src="<?php echo base_url('assets/template/images/visa-mastercard-paypal.png') ?>" alt="" width="300">
                    </a>
                </div>
            </div><!-- .footer-widgets-wrap end -->

        </div>

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="col_half">
                    Copyrights © <?php echo date('Y') ?> PT. Komodo Cipta Wisata<br>
                    <div class="copyright-links">
                        <a href="<?php echo base_url('term-and-conditions') ?>">Term & Conditions</a> /
                        <a href="<?php echo base_url('how-to-book') ?>">How to Book</a></div>
                </div>

                <div class="col_half col_last tright">
                    <div class="fright clearfix">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>

                    <div class="clear"></div>

                    <i class="icon-envelope2"></i> <a href="mail:to<?php echo $email ?>"><?php echo $email ?></a> <span class="middot">·</span> <i
                            class="icon-headphones"></i> <a href="tel:<?php echo str_replace(' ','', $no_hp) ?>"><?php echo $no_hp ?></a>
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer>

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<?php if($post->link == 'tour') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/detail_tour.min.js"></script>
<?php } elseif($post->link == 'search_boat') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/search_boat.min.js"></script>
<?php } elseif($post->link == 'fast_boat') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/fast_boat.min.js"></script>
<?php } elseif($post->link == 'term') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/term.min.js"></script>
<?php } elseif($post->link == 'travel_news') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/travel_news.min.js"></script>
<?php } elseif($post->link == 'how_book') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/how_book.min.js"></script>
<?php } elseif($post->link == 'contact') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/contact.min.js"></script>
<?php } elseif($post->link == 'detail_news') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/detail_news.min.js"></script>
<?php } elseif($post->link == 'reservation') { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/reservation.min.js"></script>
<?php } else { ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/general.min.js"></script>
<?php } ?>

<span id="base-value" data-base-url="<?php echo base_url(); ?>"></span>

</body>
</html>