<?php echo $bar_contact_us ?>
<div class="container clearfix">
    <h1><?php echo $post->artikel_title ?></h1>
    <?php echo $post->general_description; ?>
    <div class="col_full bottommargin-sm clearfix">

        <div id="jssor_1"
             style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:480px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin"
                 style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                     src="../svg/loading/static-svg/spin.svg"/>
            </div>
            <div data-u="slides"
                 style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                <div>
                    <img data-u="image" src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"/>
                    <img data-u="thumb" src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"/>
                </div>
                <?php foreach ($galeri as $r) { ?>
                    <div>
                        <img data-u="image" src="<?php echo base_url('uploaded/galeri/' . $r->galeri_nama) ?>"/>
                        <img data-u="thumb" src="<?php echo base_url('uploaded/galeri/' . $r->galeri_nama) ?>"/>
                    </div>
                <?php } ?>
            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort101"
                 style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;"
                 data-autocenter="1" data-scale-bottom="0.75">
                <div data-u="slides">
                    <div data-u="prototype" class="p" style="width:190px;height:84px;">
                        <div data-u="thumbnailtemplate" class="t"></div>
                        <svg viewBox="0 0 16000 16000" class="cv">
                            <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                            <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                            <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                        </svg>
                    </div>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;"
                 data-scale="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                    <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;"
                 data-scale="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                    <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                </svg>
            </div>
        </div>

    </div>

    <div class="text-center">
        <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>Book
            Now
        </button>
    </div>
    <br/>
    <?php if ($post->boat_spesification) { ?>
        <h2>Boat Spesification</h2>
        <?php echo $post->boat_spesification ?>
        <div class="divider"><i class="icon-circle"></i></div>
    <?php } ?>
    <?php if ($post->pickup_info) { ?>
        <h2>Pick Up Information</h2>
        <?php echo $post->pickup_info ?>
        <div class="divider"><i class="icon-circle"></i></div>
    <?php } ?>

    <div class="text-center">
        <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>Book
            Now
        </button>
    </div>
    <?php if ($post->schedule_route) { ?>
        <h2>Schedule & Route</h2>
        <?php echo $post->schedule_route ?>
    <?php } ?>

    <div class="text-center">
        <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>Book
            Now
        </button>
    </div>


</div>
