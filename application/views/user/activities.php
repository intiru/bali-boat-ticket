<div class="container clearfix" style="margin-top: -20px">
    <h1 align="center"><?php echo $post->artikel_title ?></h1>
    <?php echo $post->artikel_isi ?>

    <?php echo $activities_filter ?>

    <?php foreach ($activities_list as $r) { ?>
        <a href="<?php echo $this->base_value->permalink(array('activities',$r->artikel_title)) ?>" class="activities-item">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <img src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>">
                </div>
                <div class="col-xs-12 col-md-8 activities-info">
                    <h3><?php echo $r->artikel_title ?></h3>
                    <i class="icon-location"></i> <?php echo $r->kategori_nama ?> &nbsp;&nbsp;
                    <i class="icon-eye"></i> <?php echo number_format($r->artikel_view) ?> views
                    <p align="justify"><?php echo substr(strip_tags($r->general_description), 0, 200) ?>...</p>

                    <div class="pull-right">
                        <button class="btn btn-default"> USD <?php echo number_format($r->artikel_harga) ?></button>
                        <button class="btn btn-primary"><i class="icon-ok"></i> Detail</button>
                    </div>
                </div>
            </div>
        </a>
    <?php } ?>

</div>