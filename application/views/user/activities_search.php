<div class="container clearfix" style="margin-top: -20px">

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="hidden activities-filter">
                <?php echo $activities_filter ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <button type="button" class="btn btn-primary btn-activity-filter-show btn-block"><i class="icon-search"></i>
                Change Search
            </button>
            <br/>
            <form action="">
                <div class="activities-card">
                    <h4>Sort Results</h4>
                    <small>Sort results based on your preferences</small>
                    <hr/>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sort" id="gridRadios1" value="popular"
                               <?php echo $sort == 'popular' ? 'checked':'' ?>>
                        <label class="form-check-label" for="gridRadios1">
                            Popular
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="price_high"
                            <?php echo $sort == 'price_high' ? 'checked':'' ?>>
                        <label class="form-check-label" for="gridRadios2">
                            Highest Prices
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sort" id="gridRadios3" value="price_low"
                            <?php echo $sort == 'price_low' ? 'checked':'' ?>>
                        <label class="form-check-label" for="gridRadios3">
                            Lowest Prices
                        </label>
                    </div>
                </div>

                <div class="activities-card">
                    <h4>Filter Results</h4>
                    <small>Showing results based on your preferences</small>
                    <hr/>
                    <div class="form-row">
                        <div class="form-group" style="width: 100%">
                            <label>Price From (USD)</label>
                            <input type="number" name="price_low" class="form-control" min="<?php echo $price_low_stick ?>"
                                   max="<?php echo $price_high_stick ?>" value="<?php echo $price_low ?>">
                        </div>
                        <div class="form-group" style="width: 100%">
                            <label>Price To (USD)</label>
                            <input type="number" name="price_high" class="form-control" min="<?php echo $price_low_stick ?>"
                                   value="<?php echo $price_high ?>">
                        </div>
                        <div class="form-group">
                            <label>Categories</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="kategori_id" id="kategori_id_all"
                                       value="all" <?php echo $kategori_id == 'all' ? 'checked':'' ?>>
                                <label class="form-check-label" for="kategori_id_all">
                                    All
                                </label>
                            </div>
                            <?php foreach ($activities_category as $r) { ?>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="kategori_id"
                                           id="kategori_id_<?php echo $r->kategori_id ?>"
                                           value="<?php echo $r->kategori_id ?>"
                                           <?php echo $r->kategori_id == $kategori_id ? 'checked':'' ?>>
                                    <label class="form-check-label" for="kategori_id_<?php echo $r->kategori_id ?>">
                                        <?php echo $r->kategori_nama ?>
                                    </label>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-activity-filter-show btn-block"><i
                            class="icon-check"></i>
                    Sort & Filter Submit
                </button>
            </form>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="row">
                <?php foreach ($activities_list as $r) { ?>
                    <div class="col-xs-12 col-md-6" style="">
                        <a href="<?php echo $this->base_value->permalink(array('activities', $r->artikel_title)) ?>"
                           class="activities-card" style="padding: 0;">
                            <img src="<?php echo base_url('uploaded/content/' . $r->artikel_gambar) ?>"
                                 alt="<?php echo $r->meta_title ?>" title="<?php echo $r->artikel_title ?>">
                            <div class="activities-desc">
                                <h3><?php echo $r->artikel_title ?></h3>
                                <i class="icon-location"></i> <?php echo $post->kategori_nama ?> &nbsp;&nbsp;
                                <i class="icon-eye"></i> <?php echo number_format($r->artikel_view) ?> views
                                <p align="justify"><?php echo substr(strip_tags($r->general_description), 0, 200) ?>
                                    ...</p>

                                <div class="text-center">
                                    <button class="btn btn-default">
                                        USD <?php echo number_format($r->artikel_harga) ?></button>
                                    <button class="btn btn-primary"><i class="icon-ok"></i> Detail</button>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

</div>