<div class="input-group">
  <input type="text" class="form-control input-search" name="search" value="<?php echo $this->session->userdata('keyword') ?>" placeholder="Keyword Search ..." style="height: 34px">
  <span class="input-group-btn">
    <button class="btn btn-default btn-search" type="button">
      <span class="glyphicon glyphicon-search"></span>
    </button>
  </span>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('.btn-search').click(function() {
    search();
  });
  $('.input-search').keyup(function(e) {
    if(e.which === 13) {
      search();
    }
  });
});

function search() {
  var keywords = $('input[name="search"]').val();
  window.location.href = '<?php echo base_url('search/'); ?>'+keywords;
}
</script>