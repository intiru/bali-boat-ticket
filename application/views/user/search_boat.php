<div class="content-wrap notopmargin notoppadding">
    <div class="container clearfix">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1 align="center">Results</h1>
                <hr/>
                <br/>
            </div>
            <div class="col-xs-12 col-sm-3">
                <h2>Data Book Form:</h2>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Way Type</td>
                        <td><?php echo $way_label ?></td>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td><?php echo $from_label ?></td>
                    </tr>
                    <tr>
                        <td>To</td>
                        <td><?php echo $to_label ?></td>
                    </tr>
                    <tr>
                        <td>Departure Date</td>
                        <td><?php echo $departure ?></td>
                    </tr>
                    <?php if ($way == 'return') { ?>
                        <tr>
                            <td>Return Date</td>
                            <td><?php echo $return ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>Adults</td>
                        <td><?php echo $adults ?></td>
                    </tr>
                    <tr>
                        <td>Children</td>
                        <td><?php echo $children ?></td>
                    </tr>
                    <tr>
                        <td>Infant</td>
                        <td><?php echo $infant ?></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <button class="button button-3d button-rounded button-green btn-book"><i class="icon-repeat"></i>Update
                        Book Form
                    </button>
                </div>
            </div>
            <div class="<?php echo $col_departure ?>">
                <h2>Departure:</h2>
                <label>Filter:</label>
                <select class="form-control" name="filter-departure" data-params="departure">
                    <option value="time_early" <?php echo $filter_departure == 'time_early' ? 'selected':'' ?>>Departure Time Early</option>
                    <option value="time_last" <?php echo $filter_departure == 'time_last' ? 'selected':'' ?>>Departure Time Last</option>
                    <option value="lowest" <?php echo $filter_departure == 'lowest' ? 'selected':'' ?>>Lowest Price</option>
                    <option value="highest" <?php echo $filter_departure == 'highest' ? 'selected':'' ?>>Highest Price</option>
                </select>
                </select>
                <br/><br/>


                <?php if(count($departure_list) == 0) { ?>
                    <div class="style-msg alertmsg">
                        <div class="sb-msg">
                            <i class="icon-warning-sign"></i><strong>Sorry!</strong> Empty Departure Fasboat, please select another departure place
                        </div>
                    </div>

                    <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i> Select Departure</button>
                <?php } ?>
                <ul id="list-departure">
                    <?php foreach ($departure_list as $r) {
                        $permalink = $this->base_value->permalink(array($r->artikel_title)); ?>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <h3>
                                        <a href="<?php echo $permalink ?>" target="_blank">
                                            <?php echo $r->artikel_title ?>
                                        </a>
                                    </h3>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <img src="<?php echo base_url('uploaded/content/' . $r->artikel_gambar) ?>">
                                    <div class="boat-price">
                                        USD <?php echo number_format($r->price) ?>
                                    </div>
                                    <div class="text-center">
                                        <button data-way="departure"
                                                data-artikel-id="<?php echo $r->artikel_id ?>"
                                                data-harga-id="<?php echo $r->harga_id ?>"
                                                data-harga="<?php echo $r->price ?>"
                                                type="button"
                                                class="btn-book-departure button btn-block button-3d button-mini button-rounded button-amber button-light">
                                            SELECT
                                        </button>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8 boat-short-desc">
                                    <ul>
                                        <li>Departure time : <?php echo $r->time ?></li>
                                        <li>Departure place : <?php echo $from_label ?></li>
                                        <li>Destination place : <?php echo $to_label ?></li>
                                    </ul>
                                </div>
                            </div>
                            <hr/>
                        </li>
                    <?php } ?>
                </ul>
                <div class="text-center">
                    <div class="holder-departure"></div>
                </div>
            </div>
            <div class="<?php echo $col_destination ?>">
                <h2>Return:</h2>
                <label>Filter:</label>
                <select class="form-control" name="filter-destination" data-params="destination">
                    <option value="time_early" <?php echo $filter_destination == 'time_early' ? 'selected':'' ?>>Departure Time Early</option>
                    <option value="time_last" <?php echo $filter_destination == 'time_last' ? 'selected':'' ?>>Departure Time Last</option>
                    <option value="lowest" <?php echo $filter_destination == 'lowest' ? 'selected':'' ?>>Lowest Price</option>
                    <option value="highest" <?php echo $filter_destination == 'highest' ? 'selected':'' ?>>Highest Price</option>
                </select>
                <br/><br/>

                <?php if(count($destination_list) == 0) { ?>
                    <div class="style-msg alertmsg">
                        <div class="sb-msg">
                            <i class="icon-warning-sign"></i><strong>Sorry!</strong> Empty Destination Fasboat, please select another destination place</div>
                    </div>

                    <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i> Select Destination</button>
                <?php } ?>

                <ul id="list-destination">
                    <?php foreach ($destination_list as $r) {
                        $permalink = $this->base_value->permalink(array($r->artikel_title)); ?>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <h3>
                                        <a href="<?php echo $permalink ?>" target="_blank">
                                            <?php echo $r->artikel_title ?>
                                        </a>
                                    </h3>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <img src="<?php echo base_url('uploaded/content/' . $r->artikel_gambar) ?>">
                                    <div class="boat-price">
                                        USD <?php echo number_format($r->price) ?>
                                    </div>
                                    <div class="text-center">
                                        <div class="btn-group">
                                            <button data-way="destination"
                                                    data-artikel-id="<?php echo $r->artikel_id ?>"
                                                    data-harga-id="<?php echo $r->harga_id ?>"
                                                    data-harga="<?php echo $r->price ?>"
                                                    type="button"
                                                    class="btn-book-destination button btn-block button-3d button-mini button-rounded button-amber button-light">
                                                SELECT
                                            </button>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <div class="wrapper-button-book clearfix hidden"
                                             data-artikel-id="<?php echo $r->artikel_id ?>"
                                             data-harga-id="<?php echo $r->harga_id ?>">
                                            <button class="button button-3d button-large btn-block button-rounded button-green btn-book-now">
                                                BOOK NOW!!
                                            </button>
                                            <div class="boat-price">
                                            Total:  $<span class="total-harga">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8 boat-short-desc">
                                    <ul>
                                        <li>Departure time : <?php echo $r->time ?></li>
                                        <li>Departure place : <?php echo $to_label ?></li>
                                        <li>Destination place : <?php echo $from_label ?></li>
                                    </ul>
                                </div>
                                <div class="wrapper-disable-destination" data-artikel-id="<?php echo $r->artikel_id ?>"></div>
                            </div>
                            <hr/>
                        </li>
                    <?php } ?>
                </ul>
                <div class="text-center">
                    <div class="holder-destination"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<span id="data-value" data-way="<?php echo $way ?>"></span>

