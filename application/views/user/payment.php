

<script type="text/javascript">
  $(document).ready(function () {
    var payment_method = '<?php echo $booking->payment_method ?>';
    if(payment_method == 'paypal') {
      $('input[name="submit"]').click();
    } else {
      window.location.href = '<?php echo base_url() ?>nias/bank_transfer';
    }
  });
</script>

  <?php echo form_open('nias/payment_send', array('id' => 'payment_send')); ?>
  <input type="hidden" name="artikel_title" class="form-control" required value="<?php echo $tour->artikel_title; ?>">
  <input type="hidden" name="max_adult" class="form-control" required value="<?php echo $max_adult; ?>">
  <input type="hidden" name="max_children" class="form-control" required value="<?php echo $max_children; ?>">
  <input type="hidden" name="max_infant" class="form-control" required value="<?php echo $max_infant; ?>">
  <input type="hidden" name="price_adult" class="form-control" required value="<?php echo $price_adult; ?>">
  <input type="hidden" name="price_children" class="form-control" required value="<?php echo $price_child; ?>">
  <input type="hidden" name="price_infant" class="form-control" required value="<?php echo $price_infant; ?>">
  <input type="submit" name="submit" class="hide">
  <?php echo form_close(); ?>

<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 content-2 payment hide" style="padding-right: 30px; padding-left: 25px">
  <h1 style="color: white;" class="hide"><?php echo $post->artikel_title; ?></h1>
  <br /><br />
  <?php //echo $post->artikel_isi; ?>


  <table class="table table-bordered table-stripped hide">
    <thead>
      <tr>
        <th width="5%">No.</th>
        <th>Type</th>
        <th>Count</th>
        <th>Price</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1.</td>
        <td>Adult</td>
        <td><?php echo $max_adult; ?></td>
        <td>Rp. <?php echo number_format($tour->price_adult); ?></td>
        <td>Rp. <?php echo number_format($max_adult * $tour->price_adult); ?></td>
      </tr>
      <tr>
        <td>2.</td>
        <td>Children</td>
        <td><?php echo $max_children; ?></td>
        <td>Rp. <?php echo number_format($tour->price_children); ?></td>
        <td>Rp. <?php echo number_format($max_children * $tour->price_children); ?></td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="2"><strong>Total</strong></td>
        <td><?php echo $max_children + $max_adult; ?></td>
        <td></td>
        <td>Rp. <?php echo number_format(($max_adult * $tour->price_adult) + ($max_children * $tour->price_children)); ?></td>
      </tr>
    </tfoot>
  </table>
  <div style="font-size: 12px;">
    Dear <?php echo $title_name; ?> <?php echo $first_name . ' ' . $last_name; ?><br /><Br />

    Warmest greeting from Boat Charter Komodo.<br /><br />

    Regarding your request, herewith we are pleased to send you our proposed offer ( as requested ) with details as follows:<Br /><Br />

    Boat Name : <?php echo $tour->artikel_title; ?><br />
    Package : <?php echo $package->artikel_title ?><br />
    Periode : <?php echo date('d F Y', strtotime($date_in)); ?><Br />
    Adult : <?php echo $max_adult ?> Pax - Child : <?php echo $max_children; ?> Pax  - Infant : <?php echo $max_infant; ?> Pax <Br />
    Price : Adult : <?php echo $price_adult ?> USD - Child : <?php echo $price_child; ?> USD - Infant : <?php echo $price_infant; ?> USD<Br />
    <br />
    <b>We Accepted Payment using Paypal</b>
    <Br />
    Our paypal account: pay@indonesiarooms.biz<br />
    Beneficiary : Edelbertus Harianto<br />
    Paypal Fee : <?php echo $paypal_fee ?> % from total payment<br />
    Pre-payment : <?php echo $pre_payment ?> % from <?php echo $total_payment; ?> USD<br />
    Total your Pre-payment : <span style="color:red; font-weight: bold"><?php echo $total_pre_payment; ?> USD</span><br />
    Please click paypal icon bellow to make instant booking :<Br />
    <a href=""><img src="<?php echo base_url(); ?>assets/img/user/paypal.png" width="210" id="paypal"></a><br /><Br />

    <!-- Price Include dan exclude  : <Br />-->
    <?php echo $tour->artikel_remark ?><br /><BR />
  </div>
  Tour Program:<br />
  <div style="font-size: 12px;">
    <?php echo $package->artikel_isi; ?>
  </div>
  <Br />
  <div style="font-size: 12px;">
    Thank you and looking forward to hearing from you.<Br /><Br />

    <div style="border-bottom:2px solid #B5C4DF; padding-bottom:10px; width:250px;">
      Marcelino Sunjaya<Br />
      Operation Manager<Br />
    </div><Br />
    <a href="http://www.boatcharterkomodo.com" target="_blank"><img src="<?php echo base_url(); ?>assets/img/user/boatcharter.png"></a><Br />
    <strong>HEAD OFFICE:</strong><br />
    Jalan Soekarno Hatta Labuan Bajo Flores<br />
    <strong>W</strong>: <a href="http://www.boatcharterkomodo.com" target="_blank">www.boatcharterkomodo.com</a> | <strong>E</strong> : <a href="mailto:info@komodotours.co.id">info@komodotours.co.id</a><br />
    <strong>F</strong> : <a href="https://www.facebook.com/komodotoursadventure" target="_blank">https://www.facebook.com/komodotoursadventure</a><br />
    <strong>T</strong> : <a href="http://visitkomodotours.tumblr.com" target="_blank">http://visitkomodotours.tumblr.com</a> <strong>Twitter</strong>: <a href="https://twitter.com/komodoboattours">@komodoboattours</a><br />
    <strong>M</strong> : +62.812399.22222 <strong>WA</strong>: +62.81236016914  <strong>PIN BB</strong>: 5AF83360 <strong>Skype</strong>: indonesiarooms<br />

  </div>




</div>
<br />
<div class="hide">
<?php
  if ($reserv_type == 'tour') {
    echo $sidebar;
  } elseif ($reserv_type == 'hotel') {
    echo $sidebar_tour;
  } else {
    echo $sidebar_open_trip;
  }
  ?>
</div>
<div class="bersih"></div>
<div class="clearfix"></div>
