<?php echo $bar_contact_us ?>
<div class="container clearfix bottommargin-sm">
    <h1 align="center"><?php echo $post->artikel_title ?></h1>
    <?php echo $post->artikel_isi ?>
    <br />
    <div class="row">
        <?php foreach($tours as $r) { ?>
            <div class="col-xs-12  col-sm-6 col-md-4 col-lg-4  boat-list" style="margin-bottom:30px;padding:8px;">
                <a class=""
                   href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>">
                    <img class="gambar"
                         src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>"
                         height="200">
                    <h4 align="center"><?php echo $r->artikel_title ?></h4>
                    <br />
                    <button type="button" class="button button-3d button-rounded button-aqua"><i class="icon-info-sign"></i>View Info</button>
                    <button type="button" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>Book Now</button>
                    <br /><br />
                </a>
            </div>
        <?php } ?>
    </div>
</div>
