<?php //echo json_encode($blog); exit; ?>

<div class="container clearfix">
    <div id="posts" class="post-grid grid-container post-masonry post-timeline grid-2 clearfix">
        <div class="timeline-border"></div>
    <?php foreach($blog as $k=>$r) {  ?>
        <div class="entry entry-date-section notopmargin"><span><?php echo $r['date'] ?></span></div>
        <?php foreach($r['blog'] as $r_blog) { ?>
        <div class="entry clearfix">
            <div class="entry-timeline">
                <div class="timeline-divider"></div>
            </div>
            <div class="entry-image">
                <a href="<?php echo base_url('uploaded/content/'.$r_blog->artikel_gambar) ?>" data-lightbox="image"><img
                            class="image_fade"
                            src="<?php echo base_url('uploaded/content/'.$r_blog->artikel_gambar) ?>"
                            alt="Standard Post with Image"></a>
            </div>
            <div class="entry-title">
                <h2><a href="<?php echo $this->base_value->permalink(array($r_blog->artikel_title)) ?>"><?php echo $r_blog->artikel_title ?></a></h2>
            </div>
            <ul class="entry-meta clearfix">
                <li><i class="icon-calendar3"></i> <?php echo $r_blog->artikel_waktu ?></li>
            </ul>
            <div class="entry-content">
                <p align="justify"><?php echo substr(strip_tags($r_blog->artikel_isi), 0, 200) ?> ...</p>
                <a href="<?php echo $this->base_value->permalink(array($r_blog->artikel_title)) ?>" class="more-link button button-3d button-rounded button-green"> <i class="icon-repeat"></i>Read More</a>
            </div>
        </div>
        <?php }
        } ?>
    </div>
</div>
