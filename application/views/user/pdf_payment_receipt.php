<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap_4/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paypal.css') ?>">
<style>@page {
        margin: 0px 12px 12px 12px;
    }</style>
<div class="row">
    <div class="col-xs-8">
        <br /><br /><br />
        <img src="<?php echo base_url('assets/img/template/bck-text.png') ?>"><br />
        Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />
        Phone : +62.81239922222 Whatsapp: +62.81236016914<br/>
        sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />
    </div>
    <div class="col-xs-3 text-center">
        <img src="<?php echo base_url('assets/img/template/BCK.png'); ?>" width="150">
    </div>
</div>
<div class="text-center">
  <img src="<?php echo base_url('assets/img/template/separate.png'); ?>">
</div>
<div class="row">
  <div class="col-xs-12">
    <br />
      <div class="text-center">
          <img src="<?php echo base_url('assets/img/template/payment_receipt.png') ?>">
      </div>
    </center>
    <br />
    <hr class="separate" />
    <br />
    <div class="row">
      <div class="col-xs-6">
        <strong>Invoice ID</strong> : BCK-<?php echo $con->no_invoice($booking->payment_id) ?><br />
        <strong>Date of Issued</strong> : <?php echo date('d F Y h:i:s A') ?><br />
        <strong>Athorization by</strong> : Admin
      </div>
      <div class="col-xs-6">
        <strong>Customer Name</strong> : <?php echo $booking->title . ' ' . $booking->first_name . ' ' . $booking->last_name ?><br />
        <strong>Phone</strong> : <?php echo $booking->telephone ?><br />
        <strong>Email</strong> : <?php echo $booking->email ?>
      </div>
    </div>
    <br /><br /><br />
  </div>
  <div class="col-xs-12">
    <img src="<?php echo base_url('assets/img/template/payment_info.png') ?>">
    <hr class="separate" />
    <strong>Payment Status</strong> : Pre-Payment (<?php echo $booking->persen_dp ?>%)<br />
    <strong>Payment Method</strong> : Paypal
    <br /><br /><br /><br />
    <img src="<?php echo base_url('assets/img/template/payment_details.png') ?>">
    <hr class="separate" />
    <strong>Booking </strong> : <?php echo $booking->artikel_nama ?><br />
    <table width="100%" border="1" class="reserv-table table-blue">
      <tr style="background-color: #ADE6F9">
        <td>Charter Date</td>
        <td>Package</td>
        <td>Adult</td>
        <td>Children</td>
        <td>Infant</td>
        <td>Amount</td>
      </tr>
      <tr>
        <td><?php echo date('d F Y', strtotime($booking->date_in)) ?></td>
        <td><?php echo $booking->itinerary_title ?></td>
        <td><?php echo $booking->adult ?></td>
        <td><?php echo $booking->children ?></td>
        <td><?php echo $booking->infant ?></td>
        <td><?php echo strtoupper($booking->price_tipe) . ' ' . $booking->total ?></td>
      </tr>
    </table>
    <br />
    <div style="text-align: right">
      <strong>Total Receipt</strong> : <?php echo strtoupper($booking->price_tipe).' '.number_format($booking->pay_now) ?>
    </div>
  </div>
</div>
<br /><br />