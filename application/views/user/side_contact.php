<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  <nav id="beyond-contact">
    <center>
      <img src="<?php echo base_url(); ?>assets/img/template/kontak.png" class="img-responsive pull-right" alt="indonesia adventure">
    </center>
    <div style="padding-left:16px;">
<?php if($post->link == 'news') {
  $find    = array(' ', '/', '&', '\\');
	$replace = array('-', '-', '-', '-');
			echo '<header><h3 style="color:black;">TOURISM INFORMATION</h3></header>';
			echo '<main>';
			echo '<nav>';
			echo '<ul style="text-align:justify;margin:-10px 0 0 -20px;">';
	 		foreach($news as $news1) {
				echo '<li style="padding:6px 0">'.anchor(str_replace($find, $replace, strtolower($news1->artikel_title)), $news1->artikel_title).'</li>';
			}
			echo '</ul>';
			echo '</nav>';
			echo '</main>'; 
			echo '<div class="bersih"></div>';
		} elseif($post->link == 'tour') { ?>
			<h3 style="text-align:center">Photo Gallery</h3>
      <div class="galeri">
        <div class="image-row">
          <div class="image-set">
            <?php	foreach($galeri as $row) { ?>
            <figure>
              <a class="example-image-link kiri" href="<?php echo base_url().'uploaded/galeri/'.$row->galeri_nama; ?>" data-lightbox="example-set" title="<?php echo $row->galeri_judul; ?>">
              <img src="<?php echo base_url(); ?>uploaded/galeri/<?php echo $row->galeri_nama; ?>" alt="<?php echo $row->galeri_nama; ?>" class="kiri tour-galeri">
              </a>
            </figure>
            <?php } ?>
            <div class="bersih"></div>
          </div>
        </div>
      </div>	
<?php	} ?>
	</div>
</div>