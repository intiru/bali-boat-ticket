<div class="container clearfix">
    <h1 align="center"><?php echo $post->artikel_title ?></h1>
    <hr/>
    <br/>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?php echo $post->artikel_isi ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p>You can fill form below for contact us.</p>
            <form class="form-send" action="<?php echo base_url('nias/contact_send') ?>" data-redirect="<?php echo base_url('contact-us') ?>" method="post" style="max-width: 35rem;">
                <div class="form-group">
                    <label>Your Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Ex: Marco River">
                </div>
                <div class="form-group">
                    <label>Email Address</label>
                    <input type="email" class="form-control" name="email" placeholder="name@example.com">
                </div>
                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" class="form-control" name="subject" placeholder="Ex: Boat Schedule">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Message</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="message" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label>Security Code</label>
                    <br />
                    <?php echo $captcha; ?>
                    <input type="text" class="form-control" name="captcha">
                </div>
                <div class="form-group">
                    <button type="submit" class="button button-3d button-rounded button-green"><i class="icon-ok"></i>
                        Send Message
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>