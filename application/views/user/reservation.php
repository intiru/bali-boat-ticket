<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paypal.css') ?>">
<script>

    $(document).ready(function () {
      
      
  
  $('[name="price_tipe"]').change(function() {
      price();
    var val = $(this).val();
    if(val == 'usd') {
      $('td.harga-idr').addClass('hide');
      $('td.harga-usd').removeClass('hide');
    } else {
      $('td.harga-usd').addClass('hide');
      $('td.harga-idr').removeClass('hide');
    }
  });

  $('#calendar').fullCalendar({
  header: {
        right: 'prev,next today',
        center: 'title',
        left: ''
        //right: 'month,listMonth'
  },
    defaultDate: '<?php echo date('Y-m-d'); ?>',
    navLinks: false, // can click day/week names to navigate views
    businessHours: true, // display business hours
    editable: false,
    events: [
<?php
foreach ($date as $k => $v) {
  if ($v['status'] == 'available') {
    ?>
        {
        title: 'AVAILABLE',
          start: '<?php echo $k ?>',
          color: '#2196f3',
          url: '<?php echo $k ?>'
        },
  <?php } else if ($v['status'] == 'open_trip') { ?>
        {
        title: 'OPENTRIP',
          start: '<?php echo $k ?>',
          color: '#f4b042',
          url: '<?php echo $k ?>'
        },
  <?php } else { ?>
        {
        title: 'SOLD',
          start: '<?php echo $k ?>',
          color: '#ffcdd2'
        },
  <?php } ?>
<?php } ?>
    ],
    viewRender: function(currentView){
        var minDate = moment();
        // Past
        if (minDate >= currentView.start && minDate <= currentView.end) {
            $(".fc-prev-button").addClass('hide'); 
        }
        else {
            $(".fc-prev-button").removeClass('hide'); 
        }

    },
    eventClick: function (event) {
    if (event.url) {
    $('.input-date-reserv').val(event.url);
      $('#modal-boat-available').modal('hide');
      return false;
    }
    },
    eventRender: function (event, element, view) {
    element.find('span.fc-title').html(element.find('span.fc-title').text());
      element.find('.fc-list-item-title.fc-widget-content a').html(element.find('.fc-list-item-title.fc-widget-content a').text());
    }
  });
  });
  
  $(document).ready(function() {
      var windowsize = $(window).width();
      if(windowsize > 768) {
          //$('.reserv-table-mobile').remove();
          $('.site-comodo-mobile').remove();
      } else {
          //$('.reserv-table').remove();
          $('.site-comodo').remove();
      }
    price();
  
    $('[name="adult"], [name="children"], [name="infant"], [name="price"]').change(function() {
      price();
    });
  
  });
  
  function price() {
    var adult = $('[name="adult"]').val();
    var children = $('[name="children"]').val();
    var infant = $('[name="infant"]').val();
    var artikel_id = $('#artikel-id').data('artikel-id');
    var price_tipe = $('[name="price_tipe"]').val();
    var base_url = $('#base-value').data('base-url');
    var data_send = {
      adult: adult,
      children: children,
      infant: infant,
      artikel_id: artikel_id,
      price_tipe: price_tipe
    };
    $.ajax({
      url: base_url+'nias/reserv_harga',
      type: 'post',
      data: data_send,
      success: function(data) {
        var data = JSON.parse(data);
        $('.harga_total').html(data.harga_total);
        $('.pre_payment').html(data.pre_payment);
        $('.pay_now').html(data.pay_now);
        $('.next_payment').html(data.next_payment);
        
        $('[name="total"]').val(data.harga_total);
        $('[name="persen_dp"]').val(data.pre_payment);
        $('[name="persen_nominal"]').val(data.pay_now);
        $('[name="pay_now"]').val(data.pay_now);
        $('[name="next_payment"]').val(data.next_payment);
        $('[name="harga_adult"]').val(data.harga_adult);
          $('[name="harga_children"]').val(data.harga_children);
          $('[name="harga_infant"]').val(data.harga_infant);
      }
    });
  }
</script>

<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
  
  .fc-today-button {
    display: none;
  }
  
  .fc-day-number {
    font-weight: bold;
  }

  .fc-prev-button, .fc-next-button {
    font-weight: bold !important;
    text-transform: capitalize !important;
  }
  
  h1#title {
    margin-top: 0;
  }
</style>

<span id="artikel-id" data-artikel-id="<?php echo $itinerary->artikel_id ?>"></span>
      <?php echo form_open('reservation-send', array('id' => 'reservation'));
      ?>
<input type="hidden" name="total">
<input type="hidden" name="persen_dp">
<input type="hidden" name="persen_nominal">
<input type="hidden" name="pay_now">
<input type="hidden" name="next_payment">
<input type="hidden" name="itinerary_artikel" value="<?php echo $itinerary->artikel_title ?>">
<input type="hidden" name="harga_adult">
<input type="hidden" name="harga_children">
<input type="hidden" name="harga_infant">

<div class="row">
  <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
    
    <main id="content">

      <div class="row">
          <div class="col-xs-12">
              <div class="separate-3" style="width: 98%"></div>
          </div>
        <div class="col-xs-6 col-md-6">
          <div class="reserv-title">
            <img src="<?php echo base_url('assets/img/template/Personal.png'); ?>">ONLINE RESERVATION
          </div>
        </div>
        <div class="col-xs-6 col-md-6 text-right" style="padding-right: 15px">
          <div class="reserv-phone">
             <a href="https://api.whatsapp.com/send?phone=6281236016914&amp;text=Hi%20*Boat%20Charter%20Komodo*%0A
My name%20%3A%0AEmail%20%3A%0ARequest%20%3A%20<?php echo $title; ?>%0APeriode%20%3A%0AMessage%20%3A"><img src="<?php echo base_url('assets/img/template/Chatting.png'); ?>"> +62.81236016914</a>
          </div>
        </div>
      </div>
      <br />
      <div class="reserv-judul">Booking : <?php echo $title; ?></div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="reserv-step">Step</div>
        </div>
        <div class="pull-left">
          <div class="reserv-circle">1</div>
        </div>
        <div class="pull-left">
          <div class="reserv-step-label">BOOKING DETAILS</div>
        </div>
      </div>
      <div class="reserv-red">Select total of person to book your preferable service</div>
        <div class="table-responsive">
      <!--<table width="98%" class="reserv-table table">
        <tr style="background-color: #ADE6F9;">
            <td width="30%" style="padding-top: 0; padding-bottom: 0"><span class="judul-table">Package Name</span></td>
            <td align="center"  style="padding-top: 0; padding-bottom: 0"><span class="judul-table">Adult</span></td>
            <td  style="padding-top: 0; padding-bottom: 0"><span class="judul-table">Children</span><br /><span class="desc-table">Age 5 - 9</span></td>
            <td  style="padding-top: 0; padding-bottom: 0"><span class="judul-table">Infant</span><br /><span class="desc-table">Age 0 - 5</span></td>
          <td width="24%">
              <div class="pull-left">
                  <span class="judul-table">Price</span>
              </div>
              <div class="pull-left">
                  <select name="price_tipe" class="form-control price-tipe">
                      <option value="usd">USD</option>
                      <option value="idr">IDR</option>
                  </select>
              </div>
              <div class="clearfix"></div>
          </td>
        </tr>
        <tr>
          <td><?php /*echo $itinerary->artikel_title */?></td>
          <td>
            <select name="adult" class="form-control" style="width:60px;" >
              <?php /*foreach ($adult as $r) { */?>
                  <option value="<?php /*echo $r; */?>"><?php /*echo $r; */?></option>
                <?php /*} */?>
            </select>
          </td>
          <td>
            <select name="children" class="form-control" style="width:60px;" >
              <?php /*for ($i = 0; $i <= 15; $i++) { */?>
                <option value="<?php /*echo $i; */?>"><?php /*echo $i; */?></option>
              <?php /*} */?>
            </select>
          </td>
          <td>
            <select name="infant" class="form-control" style="width:60px;" >
              <?php /*for ($i = 0; $i <= 15; $i++) { */?>
                <option value="<?php /*echo $i; */?>"><?php /*echo $i; */?></option>
              <?php /*} */?>
            </select>
          </td>
          <td class="harga-idr hide">
            IDR <span class="harga_total">
          </td>
          <td class="harga-usd">
            USD <span class="harga_total">
          </td>
        </tr>
      </table>-->
            <table width="98%" class="reserv-table-mobile table">
                <tr>
                    <td><strong>Package name</strong></td>
                    <td><strong><?php echo $itinerary->artikel_title ?></strong></td>
                </tr>
                <tr>
                    <td><strong>Adult</strong></td>
                    <td>
                        <select name="adult" class="form-control" style="width:60px;" >
                            <?php foreach ($adult as $r) { ?>
                                <option value="<?php echo $r; ?>"><?php echo $r; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="judul-table">Children</span><br /><span class="desc-table">Age 5 - 9</span>
                    </td>
                    <td>
                        <select name="children" class="form-control" style="width:60px;" >
                            <?php for ($i = 0; $i <= 15; $i++) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="judul-table">Infant</span><br /><span class="desc-table">Age 0 - 5</span>
                    </td>
                    <td>
                        <select name="infant" class="form-control" style="width:60px;" >
                            <?php for ($i = 0; $i <= 15; $i++) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>Price in</strong></td>
                    <td>
                        <select name="price_tipe" class="form-control" style="width:90px;" >
                            <option value="usd">USD</option>
                            <option value="idr">IDR</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
      <table width="98%">
        <tr>
          <td width="30.5%"><strong>Required Deposit</strong></td>
          <td width="50%">: <strong><span class="pre_payment"></span>% * <span class="harga_total"></span></strong></td>
          <td width="8.5%" class="harga-usd"><strong>USD</strong></td>
            <td width="8.5%" class="harga-idr hide"><strong>IDR</strong></td>
          <td width="8.5%"><strong><span class="pay_now"></span></strong></td>
        </tr>
        <tr style="color:red">
          <td><strong>Total to Pay NOW</strong></td>
            <td></td>
          <td class="harga-usd"><strong>USD</strong></td>
            <td class="harga-idr hide"><strong>IDR</strong></td>
          <td><strong><span class="pay_now"></span></strong></td>
        </tr>
        <tr style="color:red">
          <td><strong>Next Payment</strong></td>
            <td>: <strong><span class="harga_total"></span> - <span class="pay_now"></span></strong></td>
            <td class="harga-usd"><strong>USD</strong></td>
            <td class="harga-idr hide"><strong>IDR</strong></td>
          <td><strong><span class="next_payment"></span></strong></td>
        </tr>
      </table>
      <br />
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="reserv-step">Step</div>
        </div>
        <div class="pull-left">
          <div class="reserv-circle">2</div>
        </div>
        <div class="pull-left">
          <div class="reserv-step-label">PERSONAL INFORMATION</div>
        </div>
      </div>
      <br />
      <div class="reserv-judul">Complete your personal Information</div>
      <?php //echo json_encode($itinerary); ?>
      <?php echo $post->artikel_isi; ?>
      <input type="hidden" name="artikel_nama" value="<?php echo $title; ?>" />
      <input type="hidden" name="reserv_type" value="<?php echo $reserv_type; ?>" />
      <input type="hidden" name="link" value="<?php echo current_url(); ?>">
      <input type="hidden" name="artikel_id" value="<?php echo $artikel_id; ?>">
      <input type="hidden" name="artikel_id_itinerary" value="<?php echo $artikel_id_itinerary; ?>">
      <input type="hidden" name="uri_2" value="<?php echo $this->uri->segment(2) ?>">
      <input type="hidden" name="uri_3" value="<?php echo $this->uri->segment(3) ?>">
      <span style="color:red">*</span> are mandatory
      <table id="table-reservation" >
        
        <tr>
          <td width="140">Title <span style="color:red">*</span></td>
          <td width="173">
            <select name="title" style="width:80px;" class="form-control">
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </select>
            <span class="form-error"></span>
          </td>
        </tr>
        
        <tr>
          <td width="140">First Name <span style="color:red">*</span></td>
          <td>
            <input type="text" class="form-control" name="f_name"/>
            <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td width="140">Last Name <span style="color:red">*</span></td>
          <td>
            <input type="text" class="form-control" name="l_name" />
            <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td width="140">Email <span style="color:red">*</span></td>
          <td>
            <input type="text" class="form-control" name="email" />
            <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td width="140">Telephone <span style="color:red">*</span></td>
          <td>
            <input type="text" class="form-control" name="phone" style="width:200px;" />
            <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td width="140">Address <span style="color:red">*</span></td>
          <td>
            <input type="text" class="form-control" name="address" />
            <span class="form-error"></span>
          </td>
        </tr>
        <td width="140">City <span style="color:red">*</span></td>
        <td>
          <input type="text" class="form-control" name="city" style="width:220px;" />
          <span class="form-error"></span>
        </td>
        </tr>
        <tr>
          <td width="140">Country <span style="color:red">*</span></td>
          <td>
            <select name="country" class="form-control">
              <?php require_once ("negara_list.php"); ?>
            </select>
            <span class="form-error"></span>
          </td>
        </tr>
        <tr class="hide">
          <td width="140">Postal Code <span style="color:red">*</span></td>
          <td>
            <input type="text" class="form-control" name="code" style="width:120px;" />
            <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td valign="middle"><?php
            if ($reserv_type == 'hotel') {
              echo 'Tour Date';
            } elseif ($reserv_type == 'open_trip') {
              echo 'Tour Date';
            } else {
              echo 'Charter Date';
            }
            ?> <span style="color:red">*</span></td>
          <td>
            <input type="text" name="date_in"
            <?php
            if ($reserv_type == 'open_trip') {
              echo 'value="" readonly class="form-control" style="width:120px; background-color: white !important"';
            } else {
              echo ' class="form-control input-date-reserv"';
            }
            ?>
                   />
          </td>
        </tr>
        <tr>
          <td></td>
          <td><span class="form-error"></span></td>
      </tr>
<!--          <tr>
          <td>Number of adult <span style="color:red">*</span></td>
          <td>
            <input type="number" class="form-control" name="adult" value="1" min="1" max="<?php echo $tour->max_adult; ?>" />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">
                <span class="glyphicon glyphicon-user" style="color: #449D44"></span>
              </span>
              <select name="adult" class="form-control" style="width:70px;">
                <?php foreach ($adult as $r) { ?>
                  <option value="<?php echo $r; ?>"><?php echo $r; ?></option>
                <?php } ?>
              </select>
            </div>
            <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td>Children <span style="color:red">*</span></td>
          <td>
            <div class="pull-left">
              <input type="number" class="form-control" name="children" style="width:70px;" value="0" min="0" max="<?php echo $tour->max_children; ?>" />
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                  <span class="glyphicon glyphicon-user" style="color: #449D44"></span>  
                </span>
                <select name="children" class="form-control" style="width:70px;">
                  <?php for ($i = 0; $i <= 15; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="pull-left" style="padding: 8px;">
              (age 5-9)
            </div>
            <div class="clearfix"
                 <span class="form-error"></span>
          </td>
        </tr>
        <tr>
          <td>Infant <span style="color:red">*</span></td>
          <td>
            <div class="pull-left">
              <input type="number" class="form-control" name="infant" style="width:70px;" value="0" min="0" max="<?php echo $tour->max_children; ?>" />

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                  <span class="glyphicon glyphicon-user" style="color: #449D44"></span>
                </span>
                <select name="infant" class="form-control" style="width:70px;">
                  <?php for ($i = 0; $i <= 15; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="pull-left" style="padding: 8px;">
              (age 0-5)
            </div>
            <div class="clearfix"></div>
            <span class="form-error"></span>
          </td>
        </tr>-->
        <tr>
          <td colspan="2">Please describe your inquiries in details</td>
        </tr>
      </table>
      <table>
        <tr>
          <td></td>
          <td height="73" width="573">
            <textarea class="form-control" name="comment" cols="20" rows="7"></textarea>
            <span class="form-error"></span>
          </td>
        </tr>
      </table>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="reserv-step">Step</div>
        </div>
        <div class="pull-left">
          <div class="reserv-circle">3</div>
        </div>
        <div class="pull-left">
          <div class="reserv-step-label">PAYMENT METHOD</div>
        </div>
      </div>
      <br />
      <div class="reserv-judul">
        Payment by * : &nbsp;&nbsp; Paypal : 
        <input type="radio" name="payment_method" value="paypal">&nbsp;&nbsp;&nbsp;&nbsp; Bank Transfer : <input type="radio" name="payment_method" value="bank" checked>
      </div>
      <div class="reserv-agree">
        <ul>
          <li>- After proceeding the paypal payment, you will be redirected back to boatcharterkomodo.com</li>
          <li>- Payment receipt and booking confirmation will be automatically sent to your email when your payment succesfully</li>
          <li>- For last minutes booking, it is advisable to contact us BY WhatsApp:  <a href="https://api.whatsapp.com/send?phone=6281236016914&amp;text=Hi%20*Boat%20Charter%20Komodo*%0A
My name%20%3A%0AEmail%20%3A%0ARequest%20%3A%20<?php echo $title; ?>%0APeriode%20%3A%0AMessage%20%3A"><b>+62.81236016914</b></a> before processing the payment</li>
          <li>- By clicking "Send Reservation" you agree with our <a href="<?php echo base_url('terms-and-conditions') ?>"><strong>Payment Term</strong> </a>of booking.</li>
        </ul>
      </div>
      <table width="100%">
        <tr>
          <td colspan="2"> 
            To prevent spamming, Please enter the code below:</td>
            <td></td>
        </tr>
        <tr>
          <td></td>
          <td colspan="3"><?php echo $captcha; ?></td>
        </tr>
        <tr>
          <td>Security <span style="color:red">*</span></td>
          <td colspan="3">
            <input type="text" class="form-control" name="captcha" style="width:140px;" />
            <span class="form-error captcha"></span>
          </td>
        </tr>
        <tr>
            <td></td>
          <td>
              <input type="submit" class="btn btn-large btn-success" value="SEND RESERVATION" />
            </td>
            <td align="right">
                <img src="<?php echo base_url('assets/img/template/ssl-site.png') ?>" class="site-comodo">
            </td>
        </tr>
      </table>
        <img src="<?php echo base_url('assets/img/template/ssl-site.png') ?>" class="site-comodo-mobile">
<div class="separate-3"></div>

    </main>
  </article>
  <?php
  if ($reserv_type == 'tour') {
    echo $sidebar;
  } elseif ($reserv_type == 'hotel') {
    echo $sidebar_tour;
  } else {
    echo $sidebar_open_trip;
  }
  ?>
</div>
      <?php echo form_close(); ?>

<div class="modal" id="modal-boat-available" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Price</h4>
      </div>
      <div class="modal-body">
        <div id="calendar"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
  $('.input-date-reserv').click(function() {
  $('#modal-boat-available').modal('show');
    $('.modal-backdrop').hide();
    $('.fc-today-button').click();
      $('.fc-next-button').text('NEXT MONTH');
      $('.fc-prev-button').text('PREVIOUS MONTH');
  });
  });
</script>