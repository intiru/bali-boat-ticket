<?php

if (!defined('BASEPATH'))
  exit('No script access allowed');

class Partners extends CI_Controller {

  function __construct() {
    parent:: __construct();
    $this->load->helper(array('form', 'url', 'html'));
    $this->load->library(array('template', 'session'));
    $this->load->model('mod_root');
  }

  function data_primary() {
    $config = $this->db->where('config_id', '1')->get('tb_config')->row();
    $data['date_from'] = $config->config_date_from;
    $data['date_to'] = $config->config_date_to;
    
    $partner_id = $this->session->userdata('partner_id');
    $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    $artikel_id = json_decode($partner->artikel_id, TRUE);
    $data['boat_daftar'] = $this->db->where_in('artikel_id', $artikel_id)->get('tb_artikel')->result();
    
    return $data;
  }

  function send_mail($title = NULL, $from = NULL, $message = NULL, $file = NULL) {
    $email = $this->mod_root->select_email_send()->result();
    $this->load->library('base_value');
    $this->base_value->email($title, $from, $message);
    $this->load->library('email');
    foreach ($email as $row) {
      $this->email->clear(TRUE);

      $config['protocol'] = 'mail';
      $config['mailtype'] = 'html';
      $config['charset'] = 'utf-8';
      $config['wordwrap'] = TRUE;
      $this->email->initialize($config);

      $this->email->from($from);
      $this->email->to($row->config_akun);

      $this->email->subject($title);
      $this->email->message($message);

      if ($file != NULL) {
        $this->email->attach($file);
      }

      if (!$this->email->send()) {
        echo $this->email->print_debugger();
      }
    }
  }

  function send_mail_from_admin($title = NULL, $to = NULL, $message = NULL, $file = NULL) {
    $email = $this->mod_root->select_email_send()->result();
    $this->load->library('base_value');
    $this->base_value->email($title, $to, $message);
    $this->load->library('email');
    foreach ($email as $row) {
      $this->email->clear(TRUE);

      $config['protocol'] = 'mail';
      $config['mailtype'] = 'html';
      $config['charset'] = 'utf-8';
      $config['wordwrap'] = TRUE;
      $this->email->initialize($config);

      $this->email->from($row->config_akun);
      $this->email->to($to);

      $this->email->subject($title);
      $this->email->message($message);

      if ($file != NULL) {
        $this->email->attach($file);
      }

      if (!$this->email->send()) {
        echo $this->email->print_debugger();
      }
    }
  }

  function login_check() {
    $level_login = $this->session->userdata('level_login');
    if ($level_login != 'partners') {
      redirect('partners');
    }
  }

  function index() {
    $level_login = $this->session->userdata('level_login');
    if ($level_login != 'partners') {
      $this->load->view('partners/login');
    } else {
      $data = $this->session->all_userdata();
      $data['news'] = $this->db->where(array(
          'posisi' => 'artikel',
          'link' => 'partner_news'
        ))
        ->order_by('artikel_id', 'DESC')
        ->get('tb_artikel', 0, 1)
        ->row();
      $data = array_merge($data, $this->data_primary());
      $this->template->partners('home', $data);
    }
  }

  function check_login($str) {
    $login_number = $this->input->post('login_number');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $where = array(
      //'prt_login_number' => $login_number,
      'prt_username' => $username,
      'prt_password' => $password
    );
    $check = $this->db->where($where)->get('tb_partner')->num_rows();
    if ($check == 0) {
      $this->form_validation->set_message('check_login', 'Username or Password was wrong');
      return FALSE;
    } else {
      return TRUE;
    }
  }

  function login() {
    $this->load->library('form_validation');
    //$this->form_validation->set_rules('login_number', 'Login Number', 'trim|required');
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_login');
    $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

    if ($this->form_validation->run() === FALSE) {
      echo json_encode(array(
        'status' => FALSE,
        'form' => array(
          //'login_number' => form_error('login_number'),
          'username' => form_error('username'),
          'password' => form_error('password'),
        )
      ));
    } else {
      $partner = $this->db->where(array(
          //'prt_login_number' => $this->input->post('login_number'),
          'prt_username' => $this->input->post('username'),
          'prt_password' => $this->input->post('password'),
        ))
        ->get('tb_partner')
        ->row();

      $return = array(
        'status' => TRUE,
        'message' => 'Login Successfull'
      );
      $session = array(
        'level_login' => 'partners',
        'level' => 'partners',
        'username' => $partner->prt_nama,
        'partner_id' => $partner->partner_id,
        'artikel_id'=>$partner->artikel_id
      );
      $this->session->set_userdata($session);
      echo json_encode($return);
    }
  }

  function logout() {
    $this->session->sess_destroy();
    redirect('partners');
  }

  function register() {
    $level_login = $this->session->userdata('level_login');
    if ($level_login != 'partners') {
      $data['captcha'] = $this->captcha();
      $this->load->view('partners/register', $data);
    } else {
      $this->load->view('partners/dashboard');
    }
  }

  function check_file($str) {
    if (empty($_FILES['userfile']['name'])) {
      $this->form_validation->set_message('check_file', 'File Must Entered');
      return FALSE;
    } else {
      return TRUE;
    }
  }

  function check_email($str) {
    $check = $this->db->where(array(
        'prt_email' => $str
      ))
      ->get('tb_partner')
      ->num_rows();
    if ($check == 0) {
      return TRUE;
    } else {
      $this->form_validation->set_message('check_email', 'Email Not Available');
      return FALSE;
    }
  }

  function captcha() {

    $this->load->helper(array('captcha', 'string'));

    $vals = array(
      'img_path' => './uploaded/captcha_mwz/',
      'img_url' => base_url() . 'uploaded/captcha_mwz/',
      'font_path' => './assets/font/segoepr.ttf',
      'img_width' => '200',
      'img_height' => 50,
      'border' => 0,
      'expiration' => 3600,
      'word' => random_string('numeric', 8)
    );

    // create captcha image
    $cap = create_captcha($vals);

    // store image html code in a variable
    $captcha = $cap['image'];

    // store the captcha word in a session
    $this->session->set_userdata('captcha_mwz', $cap['word']);

    return $captcha;
  }

  function captcha_check($str) {
    if ($str != $this->session->userdata('captcha_mwz')) {
      $this->form_validation->set_message('captcha_check', 'Security Code was Wrong, Please Try Again');
      return FALSE;
    } else {
      return TRUE;
    }
  }

  function rules_upload($path = 'content') {
    $config['upload_path'] = './uploaded/' . $path . '/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['overwrite'] = TRUE;
    $config['max_size'] = '20000000';
    $config['max_width'] = '2560';
    $config['max_height'] = '1900';
    $this->load->library('upload', $config);
  }

  function signup() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('prt_nama', 'Nama (Contact Person)', 'trim|required');
    $this->form_validation->set_rules('userfile', 'Personal Photo', 'trim|callback_check_file');
    $this->form_validation->set_rules('prt_company_name', 'Company Name', 'trim|required');
    $this->form_validation->set_rules('prt_address', 'Address', 'trim|required');
    $this->form_validation->set_rules('prt_phone', 'Telephone', 'trim|required');
    $this->form_validation->set_rules('prt_wa', 'WhatsApp Number (WA)', 'trim|required');
    $this->form_validation->set_rules('prt_fax', 'Fax Number', 'trim|required');
    $this->form_validation->set_rules('prt_license', 'License Number', 'trim|required');
    $this->form_validation->set_rules('prt_website', 'Website', 'trim|required');
    $this->form_validation->set_rules('prt_email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('prt_bank_name', 'Bank Name', 'trim|required');
    $this->form_validation->set_rules('prt_bank_account', 'Bank Account', 'trim|required');
    $this->form_validation->set_rules('prt_bank_account_beneficiary', 'Bank Account Beneficiary', 'trim|required');
    $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
    $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

    if ($this->form_validation->run() === FALSE) {
      $fields = array('prt_nama', 'userfile', 'prt_company_name', 'prt_address', 'prt_phone', 'prt_wa', 'prt_wa', 'prt_fax', 'prt_license', 'prt_website', 'prt_email', 'prt_bank_name', 'prt_bank_account', 'prt_bank_account_beneficiary', 'captcha');
      $form = array();
      foreach ($fields as $r) {
        $form[$r] = form_error($r);
      }
      echo json_encode(array(
        'status' => FALSE,
        'message' => 'Something was wrong',
        'form' => $form
      ));
    } else {
      $this->rules_upload();
      if (!$this->upload->do_upload()) {
        $error = $this->upload->display_errors('<span class="help-block">', '</span>');
        echo json_encode(array('status' => FALSE, 'form' => array('userfile' => $error)));
      } else {
        $data = $this->input->post(NULL);
        $data['prt_photo'] = $this->upload->file_name;
        $data['prt_date_register'] = date('Y-m-d H:i:s');
        unset($data['userfile'], $data['captcha']);
        $this->db->insert('tb_partner', $data);

        $title = 'Register KTW TOURS';
        $from = $data['prt_email'];
        $message = 'Dear Administrator<br />'
          . 'Login to admin for Confirmation new Regiter<br /><Br />'
          . anchor('www', 'Admin Link') . '<br /><br />'
          . 'Thank you';

        $this->send_mail($title, $from, $message);

        echo json_encode(array(
          'status' => TRUE,
          'message' => 'Register Successfull, please wait for Administrator confirmed'
        ));
      }
    }
  }

  function forgot() {
    $data['captcha'] = $this->captcha();
    $this->load->view('partners/forgot', $data);
  }

  function check_email_forgot($str) {
    $check = $this->db->where('prt_email', $str)
      ->get('tb_partner')
      ->num_rows();
    if ($check == 0) {
      $this->form_validation->set_message('check_email_forgot', 'Email not registered');
      return FALSE;
    } else {
      return TRUE;
    }
  }

  function forgot_send() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_email_forgot');
    $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
    $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
    if ($this->form_validation->run() === FALSE) {
      echo json_encode(array(
        'status' => FALSE,
        'form' => array(
          'email' => form_error('email'),
          'captcha' => form_error('captcha')
        )
      ));
    } else {
      $email = $this->input->post('email');
      $partner = $this->db->where(array(
          'prt_email' => $email
        ))
        ->get('tb_partner')
        ->row();
      $title = 'Forgot Password - Boat Charter Komodo';
      $to = $this->input->post('email');
      $message = 'Dear ' . $partner->prt_nama . '<br /><b />'
        . 'Your Account Detail Login :<br />'
        //. 'Login Number: ' . $partner->prt_login_number . '<br />'
        . 'Username: ' . $partner->prt_username . '<br />'
        . 'Password: ' . $partner->prt_password . '<br /><br />';

      $this->send_mail_from_admin($title, $to, $message);
      echo json_encode(array(
        'status' => TRUE
      ));
    }
  }

  function boat_list() {
    $this->login_check();
    
    $partner_id = $this->session->userdata('partner_id');
    $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    $artikel_id = json_decode($partner->artikel_id, TRUE);
    $data['boat'] = $this->db->where_in('artikel_id', $artikel_id)->get('tb_artikel')->result();
    $data['title'] = 'Boat List';
    $this->template->partners('boat_list', $data);
  }
  
  function calendar($artikel_id) {
    $this->login_check();
    $data = $this->data_primary();
    $data['artikel_id'] = $artikel_id;
    $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
      ->get('tb_calendar')
      ->result();

    $this->db->select('*');
    $this->db->from('bookings');
    $this->db->order_by('charter_from', 'DESC');
    $this->db->join('tb_artikel', 'bookings.itinerary_id = tb_artikel.artikel_id');
    $this->db->where(array(
        'bookings.artikel_id'=>$artikel_id,
        'bookings.charter_from >='=>date('Y-m-d'),
      ));
    $query = $this->db->get();
    $data['listBookings'] = $query->result();

    $tanggal = array();
    foreach ($data['data_list'] as $r) {
      $tanggal[] = $r->date;
    }

    $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
    $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval, $end);

    $data['date'] = array();
    $no = 0;
    $date_now = date('Y-m-d');
    foreach ($daterange as $date) {
      $date_cal = $date->format('Y-m-d');

      /*if ($date_cal < $date_now) {
        $status = 'not_available';
      } elseif (in_array($date_cal, $tanggal)) {

        $status = 'not_available';
      } else {
        $status = 'available';
      }*/

      $status = 'available';
      if ($date_cal < $date_now) {
        $status = 'not_available';
      }else{
        $query = $this->db->where('artikel_id', $artikel_id)
          ->where('date', $date_cal)
          ->get('tb_calendar');
          if ($query->num_rows() > 0){
            $row = $query->row(); 
            if($row->opentrip==0)
              $status = 'not_available';
            else
              $status = 'open_trip';
          }
          
      }

      $data['date'][$date_cal]['status'] = $status;
      //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
      //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

      $no++;
    }

    $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
    $data['artikel_title'] = $boat->artikel_title;
    $data['title'] = $boat->artikel_title;
    $data['list'] = $this->db->where('artikel_id', $artikel_id)
      ->get('tb_calendar')
      ->result();
    $this->template->partners('calendar', $data);
  }

  function price_sell($kategori_id = 0) {
    $this->login_check();
    $data = $this->data_primary();
    $data['title'] = 'Travel Packages';
    $data['persen_partner'] = $this->db->select('config_partner')->where('config_id', '1')->get('tb_config')->row()->config_partner;
    $data['count_adult'] = $this->db->distinct()
      ->select('count_adult')
      ->order_by('count_adult')
      ->get('tb_payment')
      ->result();
    $data['no'] = 1;
    if ($kategori_id == 0) {
      $where = array(
        'a.posisi' => 'artikel',
        'a.link' => 'tour'
      );
    } else {
      $where = array(
        'a.posisi' => 'artikel',
        'a.link' => 'tour',
        'a.kategori_id' => $kategori_id
      );
    }

    $data['persen_domestic'] = $this->db->select('config_domestic')
        ->where('config_id', 1)
        ->get('tb_config')
        ->row()
      ->config_domestic;

    $data['tours'] = $this->db->join('tb_kategori k', 'k.kategori_id = a.kategori_id')
      ->where($where)
      ->order_by('a.artikel_id', 'ASC')
      ->get('tb_artikel a')
      ->result();
    $data['kategori_id'] = $kategori_id;
    $data['kategori'] = $this->db->order_by('kategori_nama')->get('tb_kategori')->result();
    $this->template->partners('price_sell', $data);
  }

  function change_password() {
    $this->login_check();
    $partner_id = $this->session->userdata('partner_id');
    $data = $this->data_primary();
    $data['title'] = 'Change Password';
    $data['partner'] = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    $this->template->partners('change_password', $data);
  }

  function change_password_update() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('password', 'Password', 'trim|required');
    $this->form_validation->set_rules('password_conf', 'Password', 'trim|required|matches[password]');
    $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
    if ($this->form_validation->run() === FALSE) {
      echo json_encode(array(
        'status' => FALSE,
        'form' => array(
          'password' => form_error('password'),
          'password_conf' => form_error('password_conf')
        )
      ));
    } else {
      $partner_id = $this->session->userdata('partner_id');
      $data = array(
        'prt_password' => $this->input->post('password')
      );
      $this->db->where('partner_id', $partner_id)->update('tb_partner', $data);
      echo json_encode(array(
        'status' => TRUE
      ));
    }
  }

  function profil_data() {
    $this->login_check();
    $partner_id = $this->session->userdata('partner_id');
    $data = $this->data_primary();
    $data['title'] = 'Profil Data';
    $data['edit'] = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    $this->template->partners('profil_data', $data);
  }

  function profil_data_update() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('prt_nama', 'Nama (Contact Person)', 'trim|required');
    $this->form_validation->set_rules('userfile', 'Personal Photo', 'trim');
    $this->form_validation->set_rules('prt_company_name', 'Company Name', 'trim|required');
    $this->form_validation->set_rules('prt_address', 'Address', 'trim|required');
    $this->form_validation->set_rules('prt_phone', 'Telephone', 'trim|required');
    $this->form_validation->set_rules('prt_wa', 'WhatsApp Number (WA)', 'trim|required');
    $this->form_validation->set_rules('prt_fax', 'Fax Number', 'trim|required');
    $this->form_validation->set_rules('prt_license', 'License Number', 'trim|required');
    $this->form_validation->set_rules('prt_website', 'Website', 'trim|required');
    $this->form_validation->set_rules('prt_email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('prt_bank_account', 'Bank Account', 'trim|required');
    $this->form_validation->set_rules('prt_bank_name', 'Bank Name', 'trim|required');
    $this->form_validation->set_rules('prt_bank_account_beneficiary', 'Bank Account Beneficiary', 'trim|required');
    $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

    if ($this->form_validation->run() === FALSE) {
      $fields = array('prt_nama', 'userfile', 'prt_company_name', 'prt_address', 'prt_phone', 'prt_wa', 'prt_wa', 'prt_fax', 'prt_license', 'prt_website', 'prt_email', 'prt_bank_name', 'prt_bank_account', 'prt_bank_account_beneficiary');
      $form = array();
      foreach ($fields as $r) {
        $form[$r] = form_error($r);
      }
      echo json_encode(array(
        'status' => FALSE,
        'message' => 'Something was wrong',
        'form' => $form
      ));
    } else {

      $data = $this->input->post(NULL);
      unset($data['userfile']);

      if (!empty($_FILES['userfile']['name'])) {
        $this->rules_upload();
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors('<span class="help-block">', '</span>');
          echo json_encode(array('status' => FALSE, 'form' => array('userfile' => $error)));
          exit;
        } else {
          $data['prt_photo'] = $this->upload->file_name;
        }
      }
      
      $artikel_id = $this->session->userdata('artikel_id');
      $data_update = array(
        'email_pemilik_kapal'=> $data['prt_email']
      );
      $this->db->where('artikel_id', $artikel_id)->update('tb_artikel', $data_update);

      $partner_id = $this->session->userdata('partner_id');
      $this->db->where('partner_id', $partner_id)->update('tb_partner', $data);
      echo json_encode(array(
        'status' => TRUE,
        'message' => 'Update Successfull'
      ));
    }
  }

  function agents() {
    $this->login_check();
    $data = $this->data_primary();
    $data['title'] = 'Agents List';
    $data['agents'] = $this->db->order_by('partner_id', 'DESC')->get('tb_partner')->result();
    $this->template->partners('agents', $data);
  }
  
  function policy() {
    $this->login_check();
    //$artikel_id = $this->session->userdata('artikel_id');
    $partner_id = $this->session->userdata('partner_id');
    $data = $this->data_primary();
    $data['title'] = 'Policy';
    //$data['artikel'] = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
    $data['config'] = $this->db->where('config_id', '1')->get('tb_config')->row();
    $data['partner'] = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    //echo json_encode($data['partner']);
    $this->template->partners('policy', $data);
  }
  
  function policy_download() {
    $this->login_check();
    $this->load->helper('download');
    $partner_id = $this->session->userdata('partner_id');
    $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    force_download('uploaded/content/'.$partner->policy_file, NULL);
  }

  function itinerary($method = 'list', $artikel_id = '', $artikel_id_it = '') {
    $this->login_check();
    $data = $this->data_primary();
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;
      $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      if ($method == 'list') {
        $data['title'] = $tour->artikel_title;
        $data['list'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();
        $this->template->partners('itinerary', $data);
      } else {
        redirect();
      }
  }

  function icalendar($method = 'list', $artikel_id = '', $calendar_id = '') {
    //if ($this->login_check()) {
    $this->login_check();
      $data = $this->data_primary();
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;
      
      $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      $boat_ava = $this->db->where('artikel_id', $boat->itinerary_artikel_id)
        ->get('tb_calendar')
        ->result();
      $boat_tgl = array();
      foreach ($boat_ava as $r) {
        $boat_tgl[] = $r->date;
      }

      $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
        ->get('tb_calendar')
        ->result();
      $tanggal = array();
      foreach ($data['data_list'] as $r) {
        $tanggal[] = $r->date;
      }

      $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
      $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval, $end);

      $data['date'] = array();
      $no = 0;
      $date_now = date('Y-m-d');
      foreach ($daterange as $date) {
        $date_cal = $date->format('Y-m-d');

        if ($date_cal < $date_now) {
          $status = 'not_available';
        } elseif (in_array($date_cal, $boat_tgl)) {
          $status = 'not_available';
        } elseif (in_array($date_cal, $tanggal)) {
          $query = $this->db->where('artikel_id', $artikel_id)
          ->where('date', $date_cal)
          ->get('tb_calendar');
          if ($query->num_rows() > 0){
            $row = $query->row(); 
            if($row->opentrip==0)
              $status = 'not_available';
            else
              $status = 'open_trip';
          }
        } else {
          $status = 'available';
        }

        $data['date'][$date_cal]['status'] = $status;
        //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
        //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

        $no++;
      }

      
      $data['title'] = 'Available List - ' . $boat->artikel_title;
      $data['list'] = $this->db->where('artikel_id', $artikel_id)
        ->get('tb_calendar')
        ->result();
      $data['level_tipe'] = 'admin';
      $data['boat'] = $boat;
      if ($method == 'list') {
        $this->template->partners('icalendar', $data);
      } elseif ($method == 'update') {
        
      } else {
        redirect();
      }
    //} else {
    //  redirect();
    //}
  }

  function bookingcalendar($artikel_id)
  {
    $this->login_check();
    $data = $this->data_primary();
    $data['method'] = $method;
    $data['artikel_id'] = $artikel_id;
    $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
    $this->db->select('*');
    $this->db->from('bookings');
    $this->db->order_by('charter_from', 'DESC');
    $this->db->join('tb_artikel', 'bookings.itinerary_id = tb_artikel.artikel_id');
    $this->db->where(array(
        'bookings.artikel_id'=>$artikel_id,
        'bookings.charter_from >='=>date('Y-m-d'),
      ));
    $query = $this->db->get();
    $data['list'] = $query->result();
    $this->template->partners('bcalendar', $data);
  }

  function booking($method = 'list', $artikel_id = '', $itinerary_id='', $booking_id = '') {
    $this->login_check();
    $this->login_check();
    $data = $this->data_primary();
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;

      $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      if ($method == 'list') {
        $data['title'] = 'Booking - ' . $tour->artikel_title;

       
        $data['search'] = array();
        $data['search']['s_from'] = date("Y-m-d");
        $data['search']['s_type'] = $this->input->post('s_type',TRUE) ? $this->input->post('s_type',TRUE) : 'all';
        if($data['search']['s_type']=='direct'){
          $this->db->where(array(
            'bookings.direct_booking'=>1
          ));
          //$data['search']['s_type'] = $this->input->post('s_type',TRUE);
        }elseif($data['search']['s_type']=='agent'){
          $this->db->where(array(
            'bookings.direct_booking'=>0
          ));
          //$data['search']['s_type'] = $this->input->post('s_type',TRUE);
        }

        if($this->input->post('s_from',TRUE)){
          $data['search']['s_from'] = $this->input->post('s_from',TRUE);
        }
        $this->db->where(array(
          'bookings.charter_from >='=>$data['search']['s_from']
        ));


        if($this->input->post('s_end',TRUE)){
          $this->db->where(array(
            'bookings.charter_from <='=>$this->input->post('s_end',TRUE)
          ));
          $data['search']['s_end'] = $this->input->post('s_end',TRUE);
        }

        $this->db->select('*');
        $this->db->from('bookings');
        $this->db->order_by('charter_from', 'ASC');
        $this->db->join('tb_artikel', 'bookings.itinerary_id = tb_artikel.artikel_id');
        $this->db->where(array(
            'bookings.artikel_id'=>$artikel_id
          ));
        $query = $this->db->get();
        $data['list'] = $query->result();

        

        $this->template->partners('booking', $data);

      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $data['title'] = 'New Booking - ' . $tour->artikel_title;
        $data['itinerary'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(5))->row();
        
        
        $data['negara'] = $this->load->view('user/negara_list', '', true);

        $this->template->partners('booking', $data);
      } elseif ($method == 'insert') {

        $this->load->helper('date');
        $passanger_name = $this->input->post('p_name');
        $passanger_id = $this->input->post('p_id');
        $passanger_age = $this->input->post('p_age_type');
        $passanger_nationality = $this->input->post('nationality');
        $passanger_stay = $this->input->post('p_stay');
        $passanger_flight = $this->input->post('p_flight');

        $adults = 0;
        $childs = 0;
        $infants = 0;
        $passanger_data = array();

        for($pi=0;$pi<sizeof($passanger_name);$pi++){
          if($passanger_name[$pi]<>""){
            $passanger_data[] = array(
              'name'=>$passanger_name[$pi],
              'id'=>$passanger_id[$pi],
              'age'=>$passanger_age[$pi],
              'nationality' => $passanger_nationality[$pi],
              'stay' => $passanger_stay[$pi],
              'flight' => $passanger_flight[$pi],
            );

            switch ($passanger_age[$pi]) {
              case 'adult':
                $adults = $adults+1;
                break;
              case 'child':
                $childs = $childs+1;
                break;
              default:
                $infants = $infants+1;
                break;
            }

          }
        }

        $data = array(
          'artikel_id' => $artikel_id,
          'itinerary_id' => $itinerary_id,
          'agent_name' => $this->input->post('agent_name'),
          'direct_booking' => $this->input->post('direct_booking') ? $this->input->post('direct_booking') : 0,
          'full_name' => $this->input->post('full_name'),
          'phone' => $this->input->post('phone'),
          'email' => $this->input->post('email'),
          'address' => $this->input->post('address'),
          'charter_from' => $this->input->post('charter_from'),
          'charter_end' => $this->input->post('charter_end'),
          'adults' => $adults,
          'childs' => $childs,
          'infants' => $infants,
          'ap_time' => $this->input->post('ap_time'),
          'ap_flight_name' => $this->input->post('ap_flight_name'),
          'hp_time' => $this->input->post('hp_time'),
          'hp_name' => $this->input->post('hp_name'),
          'passangers' => json_encode($passanger_data),
          'created_at' => date("Y-m-d"),
        );

        $this->mod_root->insert('bookings', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');

        $this->db->select('*');
        $this->db->from('bookings');
        $this->db->order_by('charter_from', 'DESC');
        $this->db->join('tb_artikel', 'bookings.itinerary_id = tb_artikel.artikel_id');
        $this->db->where(array(
            'bookings.booking_id'=> $this->uri->segment(5)
          ));
        $query = $this->db->get();
        $data['edit'] = $query->row();
//print_r($data['edit']);exit();
        $data['title'] = 'Edit Booking - ' . $tour->artikel_title;
        $this->template->partners('booking', $data);
      } elseif ($method == 'update') {
        $passanger_name = $this->input->post('p_name');
        $passanger_id = $this->input->post('p_id');
        $passanger_age = $this->input->post('p_age_type');
        $passanger_nationality = $this->input->post('nationality');
        $passanger_stay = $this->input->post('p_stay');
        $passanger_flight = $this->input->post('p_flight');
        //$booking_id =  $this->uri->segment(5);

        $adults = 0;
        $childs = 0;
        $infants = 0;
        $passanger_data = array();

        for($pi=0;$pi<sizeof($passanger_name);$pi++){
          if($passanger_name[$pi]<>""){
            $passanger_data[] = array(
              'name'=>$passanger_name[$pi],
              'id'=>$passanger_id[$pi],
              'age'=>$passanger_age[$pi],
              'nationality' => $passanger_nationality[$pi],
              'stay' => $passanger_stay[$pi],
              'flight' => $passanger_flight[$pi],
            );

            switch ($passanger_age[$pi]) {
              case 'adult':
                $adults = $adults+1;
                break;
              case 'child':
                $childs = $childs+1;
                break;
              default:
                $infants = $infants+1;
                break;
            }

          }
        }
        $this->load->helper('date');
        $data = array(
          'agent_name' => $this->input->post('agent_name'),
          'direct_booking' => $this->input->post('direct_booking') ? $this->input->post('direct_booking'):0,
          'full_name' => $this->input->post('full_name'),
          'address' => $this->input->post('address'),
          'phone' => $this->input->post('phone'),
          'email' => $this->input->post('email'),
          'address' => $this->input->post('address'),
          'adults' => $adults,
          'childs' => $childs,
          'infants' => $infants,
          'ap_time' => $this->input->post('ap_time'),
          'ap_flight_name' => $this->input->post('ap_flight_name'),
          'hp_time' => $this->input->post('hp_time'),
          'hp_name' => $this->input->post('hp_name'),
          'passangers' => json_encode($passanger_data),
        );
        $this->mod_root->update('bookings', 'booking_id', $booking_id, $data);
        echo json_encode(array('status' => 'success'));

      } elseif ($method == 'delete') {
        $this->mod_root->delete('bookings', 'booking_id', $this->uri->segment(5));
      } else {
        redirect();
      }
    
  }
}
