<?php

if (!defined('BASEPATH'))
    exit('Access no alowed');

class Nias extends CI_Controller
{
    public $webConfig = array();

    function __construct()
    {

        parent:: __construct();

        $this->load->helper(array('form', 'html', 'url'));
//
        $this->load->model(array('mod_web', 'mod_root'));
//  
        $this->load->library(array('template', 'session', 'base_value'));

        $this->webConfig = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row();

    }

    function toolbar($page = 'home')
    {

        $toolbar = $this->mod_web->toolbar($this->session->userdata('username'), $this->session->userdata('password'))->num_rows();

        //echo $this->session->userdata('username').' - '.$this->session->userdata('password').' - '.$toolbar;

        if ($this->session->userdata('status') == 'login' && $toolbar == 1) {

            $data['toolbar'] = 'yes';
        } else {

            $data['toolbar'] = 'no';
        }


        $menu = $page ? $page : 'home';


        if (!is_numeric($menu)) {

            $menu = $this->mod_web->select_by_id('tb_artikel', 'link', $menu)->row();

            $data['edit'] = "security/menu/edit/" . $menu->artikel_id;
        } else {

            $menu = $this->mod_web->select_by_id('tb_artikel', 'artikel_id', $menu)->row();

            $data['edit'] = "security/post/edit/" . $menu->posisi . '/' . $menu->artikel_id;
        }

        $data['page'] = current_url();


        return $data;
    }

    function sess_destroy()
    {
        $this->session->sess_destroy();
    }

    function send_mail($title = NULL, $from = NULL, $message = NULL)
    {

        $email = $this->mod_web->select_email_send()->result();


        $this->load->library('email');

        foreach ($email as $row) {

            $config['protocol'] = 'mail';

            $config['mailtype'] = 'html';

            $config['charset'] = 'utf-8';

            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $this->email->from($from);

            $this->email->to($row->config_akun);
            //$this->email->to("harrysudana@gmail.com");

            $this->email->subject($title);

            $this->email->message($message);
            $this->email->send();
        }
    }

    function send_mail_from_admin($title = NULL, $to = NULL, $message = NULL, $file = NULL)
    {
        $email = $this->mod_root->select_email_send()->result();
        $this->load->library('email');
        foreach ($email as $row) {
            $this->email->clear(TRUE);

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $this->email->initialize($config);

            $this->email->from($row->config_akun);
            $this->email->to($to);

            $this->email->subject($title);
            $this->email->message($message);

            if ($file != NULL) {
                $this->email->attach($file);
            }

            if (!$this->email->send()) {
                echo $this->email->print_debugger();
            }
        }
    }

    function send_mail_free($title = NULL, $from = NULL, $to = NULL, $message = NULL, $file = array())
    {
        $this->load->library('email');
        $this->load->model('mod_root');
        $this->mod_root->select_data($title, $from, $message, $file);
        $this->email->clear(TRUE);

        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $this->email->from($from);
        $this->email->to($to);

        $this->email->subject($title);
        $this->email->message($message);

        if (count($file) != 0) {
            foreach ($file as $r) {
                $this->email->attach($r);
            }
        }

        if (!$this->email->send()) {
            echo $this->email->print_debugger();
        }

    }


    function captcha()
    {

        $this->load->helper(array('captcha', 'string'));


        $vals = array(
            'img_path' => './uploaded/captcha_mwz/',
            'img_url' => base_url() . 'uploaded/captcha_mwz/',
            'font_path' => './assets/font/TBFIXTM.TTF',
            'img_width' => '165',
            'img_height' => 40,
            'border' => 0,
            'expiration' => 3600,
            'font_size' => 18,
            'word_length' => 6,
            'pool' => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 255, 255)
            )
        );


        // create captcha image

        $cap = create_captcha($vals);


        // store image html code in a variable

        $captcha = $cap['image'];


        // store the captcha word in a session

        $this->session->set_userdata('captcha_mwz', $cap['word']);


        return $captcha;
    }

    function pagination($jum_data = NULL, $jum_view = NULL, $page = 'testimonial', $uri_segment = 2)
    {

        //load class PAGINATION
        $this->load->library('pagination');

        //pengaturan pagination
        $config['base_url'] = base_url() . $page;
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");

        $config['total_rows'] = $jum_data;

        $config['per_page'] = $jum_view;

        $config['uri_segment'] = $uri_segment;

        $config['full_tag_open'] = '<ul class="pagination">';

        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = '&laquo; First';

        $config['first_tag_open'] = '<li class="page-item">';

        $config['first_tag_close'] = '</li>';


        $config['last_link'] = 'Last &raquo;';

        $config['last_tag_open'] = '<li class="page-item">';

        $config['last_tag_close'] = '</li>';


        $config['next_link'] = 'Next &raquo;';

        $config['next_tag_open'] = '<li class="page-item">';

        $config['next_tag_close'] = '</li>';


        $config['prev_link'] = '&laquo; Previous';

        $config['prev_tag_open'] = '<li class="page-item">';

        $config['prev_tag_close'] = '</li>';


        $config['cur_tag_open'] = '<li class="active page-item"><a class="page-link" href="">';

        $config['cur_tag_close'] = '</a></li>';


        $config['num_tag_open'] = '<li class="page-item">';

        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        //$config['page_query_string'] = TRUE;


        //inisialisasi config

        $this->pagination->initialize($config);


        //buat pagination

        $pagination = $this->pagination->create_links();


        return $pagination;
    }

    function data_primary($page = 'home')
    {

        //$config = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row();

        $data['visiting'] = $this;
        $data['no_hp'] = '+62 821-4599-7124';
        $data['email'] = "baliboatticket@gmail.com";


        $data['footer'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'footer')->row()->artikel_isi;

        $data['contact'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'contact_bar')->row();
//
//        $data['link'] = $this->mod_web->select_customer_service('Link')->result();
//
//        $data['yahoo'] = $this->mod_web->select_customer_service('Yahoo')->result();
//
//        $data['skype'] = $this->mod_web->select_customer_service('Skype')->result();
//
//        $data['advisor'] = $this->db->select('advisor')->where('config_id', 1)->get('tb_config')->row()->advisor;

        $data['favicon'] = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row()->config_images;
//        $data['note'] = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row()->config_note;

//        $data['board_desc'] = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row()->config_board;
//        $data['activities_desc'] = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row()->config_activities;

        //$data['footer_contact'] = $this->db->select('artikel_isi')->where('link', 'footer_contact')->get('tb_artikel')->row()->artikel_isi;

        //$data['footer_payment'] = $this->db->select('artikel_isi')->where('link', 'footer_payment')->get('tb_artikel')->row()->artikel_isi;

//        $data['kategori'] = $this->db->join('tb_kategori_seo ks', 'ks.kategori_id = k.kategori_id')
//            ->where(array(
//                'link'=>'tour',
//                'publish'=>'yes'
//            ))
//            ->order_by('k.kategori_nama', 'ASC')
//            ->get('tb_kategori k')
//            ->result();

        //$data['kategori_hotel'] = $this->db->join('tb_kategori_seo ks', 'ks.kategori_id = k.kategori_id')->where('link', 'hotel')->order_by('k.kategori_nama', 'ASC')->get('tb_kategori k')->result();
        $data['partner'] = $this->db->where('jenis', 1)->order_by('nama', 'ASC')->get('banner')->result();

        //$data['side_contact'] = $this->load->view('user/side_contact', $data, TRUE);

//        $data['header'] = $this->mod_web->select_by_id('tb_gambar', 'gambar_slider', 'yes')->result();
//        $data['search_words'] = !empty($_GET['search']) ? $_GET['search'] : '';
//        $data['about'] = $this->db->where(array(
//            'link'=>'about',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();
//        $data['tos'] = $this->db->where(array(
//            'link'=>'tos',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();
//        $data['contact'] = $this->db->where(array(
//            'link'=>'contact',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();

//print_r($this->config);
//echo $config->config_sidebar_boat_num;

//        $data['related_tour'] = $this->db->where('a.kategori_id', 12)
//            ->where('a.show', 'yes')
//            ->limit($this->webConfig->config_sidebar_boat_num)
//            ->order_by('id', 'RANDOM')
//            ->get('tb_artikel a')
//            ->result();
//
//        $data['sidebar'] = $this->load->view('user/sidebar', $data, TRUE);
//
//        $data['banner_web_left'] = $this->db->where('jenis', 6)
//            ->where('nama', 'LEFT')
//            ->where('publish', 'yes')
//            ->limit(1)
//            ->order_by('id', 'RANDOM')
//            ->get('banner')
//            ->row();
//
//        $data['banner_web_right'] = $this->db->where('jenis', 6)
//            ->where('nama', 'RIGHT')
//            ->where('publish', 'yes')
//            ->limit(1)
//            ->order_by('id', 'RANDOM')
//            ->get('banner')
//            ->row();
//
//        $data['tipe'] = '';


        $data['find'] = array(' ', '/', '&', '\\');
        $data['replace'] = array('-', '-', '-', '-');

//        $data['kategori_id_cabin'] = array(20);
//
//
//        $data['footer_contact_1'] = $this->db->where(array(
//            'link'=>'footer_contact_1',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();
//        $data['footer_contact_2'] = $this->db->where(array(
//            'link'=>'footer_contact_2',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();
//        $data['footer_support'] = $this->db->where(array(
//            'link'=>'footer_support',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();

        ////////////////////////////////////////////////////////////


        $data['reason'] = $this->db->where(array(
            'link' => 'reason',
            'posisi' => 'menu'
        ))
            ->get('tb_artikel')
            ->row();
        $data['experience'] = $this->db->where(array(
            'link' => 'experience',
            'posisi' => 'menu'
        ))
            ->get('tb_artikel')
            ->row();
        $data['support'] = $this->db->where(array(
            'link' => 'support',
            'posisi' => 'menu'
        ))
            ->get('tb_artikel')
            ->row();
//        $data['liveaboard'] = $this->db->where(array(
//            'link'=>'liveaboard',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();
//        $data['diving'] = $this->db->where(array(
//            'link'=>'diving',
//            'posisi'=>'menu'
//        ))
//            ->get('tb_artikel')
//            ->row();

        $data['bar_contact_us'] = $this->load->view('user/bar_contact_us', $data, TRUE);
        $data['footer_news'] = $this->db
            ->where(array(
                'posisi' => 'artikel',
                'link' => 'news'
            ))
            ->order_by('artikel_id', 'DESC')
            ->get('tb_artikel', 6, 0)
            ->result();
        $data['destination'] = $this->db->order_by('destination', 'ASC')->get('tb_destination')->result();

        $data['from'] = $this->session->userdata('from');
        $data['to'] = $this->session->userdata('to');
        $data['way'] = $this->session->userdata('way');
        $data['departure'] = $this->session->userdata('departure');
        $data['return'] = $this->session->userdata('return');
        $data['adults'] = $this->session->userdata('adults');
        $data['children'] = $this->session->userdata('children');
        $data['infant'] = $this->session->userdata('infant');

        return $data;
    }

    function index()
    {
        $data = $this->data_primary('home');

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'home')->row();

        if (!empty($_GET['search'])) {
            $data['search_list'] = $this->db->where(array(
                'link' => 'tour',
                'posisi' => 'artikel'
            ))
                ->like('artikel_title', $_GET['search'])
                ->get('tb_artikel')
                ->result();
            $this->template->user($data, 'search');
        } else {
            $data['tours'] = $this->db
                ->where(array(
                    'posisi' => 'artikel',
                    'link' => 'tour',
                    'publish' => 'yes'
                ))
                ->order_by('artikel_id', 'DESC')
                ->get('tb_artikel')
                ->result();
            $this->template->user($data, 'home');
        }
    }

    function pages($page, $id = '')
    {

        $data = $this->data_primary($page);

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', $page)->row();

        $data['page'] = $page;
        $data['legality'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'legality')->row();
        $data['team_type'] = $this->db->order_by('id', 'ASC')->get('team_type')->result();

        if ($page == 'fast_boats') {

            $data['tours'] = $this->db
                ->where(array(
                    'link' => 'tour',
                    'posisi' => 'artikel'
                ))
                ->order_by('artikel_id', 'DESC')
                ->get('tb_artikel')
                ->result();
            $data['post']->link = 'fast_boat';

        } else if ($page == 'activities') {

            $data['activities_filter'] = $this->load->view('user/activities_filter', [], TRUE);
            $data['activities_list'] = $this->db->join('tb_kategori k', 'k.kategori_id = a.kategori_id')
                ->where(array(
                    'a.posisi' => 'artikel',
                    'a.link' => 'activities',
                    'a.publish' => 'yes'
                ))
                ->order_by('a.artikel_title', 'ASC')
                ->get('tb_artikel a')
                ->result();
            $data['post']->link = 'activities';

        } else if ($page == 'activities_area') {
            $id = explode(',', $_GET['id']);
            $type = $id[0];
            $type_id = $id[1];

            $session = array(
                'type' => $type,
                'type_id' => $type_id
            );
            $this->session->set_userdata($session);

            if ($type == 'act') {
                $artikel_title = $this->db
                    ->select('artikel_title')
                    ->where('artikel_id', $type_id)
                    ->get('tb_artikel')
                    ->row()
                    ->artikel_title;

                redirect($this->base_value->permalink(array('activities', $artikel_title)));
            }

            $data['activities_filter'] = $this->load->view('user/activities_filter', [], TRUE);
            $data['post'] = $this->db->join('tb_kategori_seo ks', 'ks.kategori_id = k.kategori_id')
                ->where('k.kategori_id', $type_id)
                ->get('tb_kategori k')
                ->row();
            $data['post']->link = 'activities_area';
            $data['activities_list'] = $this->db
                ->where('kategori_id', $type_id)
                ->get('tb_artikel')
                ->result();

        } else if ($page == 'activities_search') {
            $data['activities_filter'] = $this->load->view('user/activities_filter', [], TRUE);
            $data['activities_category'] = $this->db->order_by('kategori_nama', 'ASC')->get('tb_kategori_activities')->result();


            $kategori_id_data = $id;

            $data['price_low_stick'] = $this->db->select('artikel_harga')
                ->where('kategori_id', $kategori_id_data)
                ->order_by('artikel_harga', 'ASC')
                ->get('tb_artikel')
                ->row()
                ->artikel_harga;
            $data['price_high_stick'] = $this->db->select('artikel_harga')
                ->where('kategori_id', $kategori_id_data)
                ->order_by('artikel_harga', 'DESC')
                ->get('tb_artikel')
                ->row()
                ->artikel_harga;

            $sort = !empty($_GET['sort']) ? $_GET['sort'] : 'popular';
            $price_low = !empty($_GET['price_low']) ? $_GET['price_low'] : $data['price_low_stick'];
            $price_high = !empty($_GET['price_high']) ? $_GET['price_high'] : $data['price_high_stick'];
            $kategori_id = !empty($_GET['kategori_id']) ? $_GET['kategori_id'] : 'all';


            $data['sort'] = $sort;
            $data['price_low'] = $price_low;
            $data['price_high'] = $price_high;
            $data['kategori_id'] = $kategori_id;

            $data['post'] = $this->db->join('tb_kategori_seo ks', 'ks.kategori_id = k.kategori_id')
                ->where('k.kategori_id', $kategori_id_data)
                ->get('tb_kategori k')
                ->row();
            $data['post']->link = 'activities_search';


            $activities_list = $this->db
                ->where('kategori_id', $kategori_id_data);

            switch ($sort) {
                case "popular":
                    $activities_list = $activities_list->order_by('artikel_view', 'DESC');
                    break;
                case "price_high":
                    $activities_list = $activities_list->order_by('artikel_harga', 'DESC');
                    break;
                case "price_low":
                    $activities_list = $activities_list->order_by('artikel_harga', 'ASC');
                    break;
            }

            if ($kategori_id != 'all') {
                $activities_list = $activities_list->where('kategori_activities_id', $kategori_id);
            }

            $activities_list = $activities_list
                ->where('artikel_harga >=', $price_low)
                ->where('artikel_harga <=', $price_high)
                ->get('tb_artikel')
                ->result();

            $data['activities_list'] = $activities_list;

        } else if ($page == 'activities_detail') {

            $data['activities_filter'] = $this->load->view('user/activities_filter', [], TRUE);

        } else if ($page == 'activities_book') {

            $artikel_id = !empty($_GET['id']) ? $_GET['id'] : '';
            $adult = !empty($_GET['adult']) ? $_GET['adult'] : '';
            $child = !empty($_GET['child']) ? $_GET['child'] : '';
            $date = !empty($_GET['date']) ? $_GET['date'] : '';


            $data['activity'] = $this->db
                ->where('artikel_id', $artikel_id)
                ->get('tb_artikel')
                ->row();
            $data['adult'] = $adult;
            $data['child'] = $child;
            $data['date'] = $date;
            $data['total_visitor'] = $adult + $child;
            $data['total_price'] = $data['activity']->artikel_harga * ($adult+$child);
            $data['captcha'] = $this->captcha();

        } elseif ($page == 'testimonial' || $page == 'contact') {

            //$data['captcha'] = $this->captcha();


            $jum_data = $this->mod_root->select_by_id('tb_comment', 'comment_publish', 'yes')->num_rows();

            $jum_view = $this->mod_root->select_by_id('tb_config', 'config_id', '1')->row()->config_akun;
            $data['pagination'] = $this->pagination($jum_data, $jum_view, 'guest-comment');

            $data['comment'] = $this->mod_root->get_travel($jum_view, $this->uri->segment(2));

            $data['captcha'] = $this->captcha();

        } elseif ($page == 'ticket') {

            $this->load->helper('datepicker');

            //$data['captcha'] = $this->captcha();
        } elseif ($page == 'travel_news') {
            /*            $jum_data = $this->mod_root->select_by_id('tb_artikel', 'link', 'news')->num_rows();
                        $jum_view = $this->mod_root->select_by_id('tb_config', 'config_id', '1')->row()->config_title;
                        $data['pagination'] = $this->pagination($jum_data, $jum_view, 'travel-news');*/

            $year_month = $this->db
                ->distinct()
                ->select('YEAR(artikel_waktu) AS year, MONTH(artikel_waktu) AS month')
                ->where(array(
                    'link' => 'news',
                    'posisi' => 'artikel'
                ))
                ->order_by('year', 'DESC')
                ->order_by('month', 'DESC')
                ->get('tb_artikel')
                ->result();
            $blogs = array();
            foreach ($year_month as $k => $r) {
                $blog = $this->db
                    ->where(array(
                        'YEAR(artikel_waktu)' => $r->year,
                        'MONTH(artikel_waktu)' => $r->month,
                        'link' => 'news',
                        'posisi' => 'artikel'
                    ))
                    ->order_by('artikel_id', 'DESC')
                    ->get('tb_artikel')
                    ->result();

                $blogs[] = array(
                    'date' => $this->month_label($r->month) . ' ' . $r->year,
                    'blog' => $blog
                );
            }

            //echo json_encode($blogs);
            //exit;

            $data['blog'] = $blogs;

        } elseif ($page == 'galeri_foto') {

            $this->load->helper('lightbox');

            $data['galeri'] = $this->mod_web->select('tb_galeri', 'galeri_id', 'ASC')->result();
        }

        /*

          if($page=='booking' || $page=='testimonial')

          {

          $id_ar = $this->mod_web->select_by_id('tb_artikel', 'tipe', $page)->row()->artikel_id;

          $data['comment'] = $this->mod_web->select_comment($id_ar)->result();

          $data['comment_jum'] = $this->mod_web->select_comment($id_ar)->num_rows();

          } */

        $this->template->user($data, $page);
    }

    function month_label($month)
    {
        switch ($month) {
            case "1":
                return 'January';
                break;
            case "2":
                return 'February';
                break;
            case "3":
                return 'March';
                break;
            case "4":
                return 'April';
                break;
            case "5":
                return 'May';
                break;
            case "6":
                return 'June';
                break;
            case "7":
                return 'July';
                break;
            case "8":
                return 'August';
                break;
            case "9":
                return 'September';
                break;
            case "10":
                return 'October';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'December';
                break;
        }
    }

    function change_to_date()
    {
        $artikel_waktu = $this->db->select('artikel_waktu, artikel_id')->get('tb_artikel')->result();
        foreach ($artikel_waktu as $r) {
            $date = date('Y-m-d H:i:s', strtotime($r->artikel_waktu));
            $this->db
                ->where('artikel_id', $r->artikel_id)
                ->update('tb_artikel', array('artikel_waktu' => $date));
        }
    }

    function fast_boat_detail()
    {
        $data = $this->data_primary();

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'home')->row();

        $this->template->user($data, 'fast_boat_detail');
    }

    function boat_detail()
    {
        $data = $this->data_primary();

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'home')->row();

        $this->template->user($data, 'boat_detail');
    }

    function search_boat()
    {
        $pagination_view = 1;
        $data = $this->data_primary();
        $from = isset($_GET['from']) ? $_GET['from'] : $this->session->userdata('from');
        $to = isset($_GET['to']) ? $_GET['to'] : $this->session->userdata('to');
        $way = isset($_GET['way']) ? $_GET['way'] : $this->session->userdata('way');
        $departure = isset($_GET['departure']) ? $_GET['departure'] : $this->session->userdata('departure');
        $return = isset($_GET['return']) ? $_GET['return'] : $this->session->userdata('return');
        $adults = isset($_GET['adults']) ? $_GET['adults'] : $this->session->userdata('adults');
        $children = isset($_GET['children']) ? $_GET['children'] : $this->session->userdata('children');
        $infant = isset($_GET['infant']) ? $_GET['infant'] : $this->session->userdata('infant');
        $from_label = $this->db->select('destination')->where('id', $from)->get('tb_destination')->row()->destination;
        $to_label = $this->db->select('destination')->where('id', $to)->get('tb_destination')->row()->destination;
        $filter_departure = $this->session->userdata('filter_departure') ? $this->session->userdata('filter_departure') : 'time_early';
        $filter_destination = $this->session->userdata('filter_destination') ? $this->session->userdata('filter_destination') : 'time_early';


        $departure_list = $this->db
            ->join('tb_artikel a', 'a.artikel_id = h.artikel_id')
            ->where(array(
                'h.destination_id_departure' => $from,
                'h.destination_id_destination' => $to
            ));
        if ($filter_departure == 'time_early') {
            $departure_list = $departure_list->order_by('h.time', 'ASC');
        } else if ($filter_departure == 'time_last') {
            $departure_list = $departure_list->order_by('h.time', 'DESC');
        } else if ($filter_departure == 'lowest') {
            $departure_list = $departure_list->order_by('h.harga', 'ASC');
        } else if ($filter_departure == 'highest') {
            $departure_list = $departure_list->order_by('h.harga', 'DESC');
        }
        $departure_list = $departure_list->get('tb_harga h')
            ->result();


        $destination_list = $this->db
            ->join('tb_artikel a', 'a.artikel_id = h.artikel_id')
            ->where(array(
                'h.destination_id_departure' => $to,
                'h.destination_id_destination' => $from
            ));

        if ($filter_destination == 'time_early') {
            $destination_list = $destination_list->order_by('h.time', 'ASC');
        } else if ($filter_destination == 'time_last') {
            $destination_list = $destination_list->order_by('h.time', 'DESC');
        } else if ($filter_destination == 'lowest') {
            $destination_list = $destination_list->order_by('h.harga', 'ASC');
        } else if ($filter_destination == 'highest') {
            $destination_list = $destination_list->order_by('h.harga', 'DESC');
        }

        $destination_list = $destination_list
            ->get('tb_harga h')
            ->result();

        $data['from'] = $from;
        $data['to'] = $to;
        $data['way'] = $way;
        $data['departure'] = $departure;
        $data['return'] = $return;
        $data['adults'] = $adults;
        $data['children'] = $children;
        $data['infant'] = $infant;
        $data['from_label'] = $from_label;
        $data['to_label'] = $to_label;
        $data['departure_list'] = $departure_list;
        $data['destination_list'] = $destination_list;
        $data['filter_departure'] = $filter_departure;
        $data['filter_destination'] = $filter_destination;

        $data['way_label'] = $way == 'return' ? 'Return' : 'One Way';

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'home')->row();
        $data['post']->link = 'search_boat';

        $this->session->set_userdata(array(
            'from' => $from,
            'to' => $to,
            'way' => $way,
            'departure' => $departure,
            'return' => $return,
            'adults' => $adults,
            'children' => $children,
            'infant' => $infant,
        ));

        $data['col_departure'] = 'col-xs-12 col-sm-4 offset-sm-1 boat-list-search';
        $data['col_destination'] = 'col-xs-12 col-sm-4 boat-list-search';

        if ($way != 'return') {
            $data['col_departure'] = 'col-xs-12 col-sm-8 offset-sm-1 boat-list-search';
            $data['col_destination'] = 'col-xs-12 col-sm-4 boat-list-search hidden';
        }

        $this->template->user($data, 'search_boat');
    }

    function filter_search_sess()
    {
        $params = $this->input->post('params');
        $value = $this->input->post('value');

        $this->session->set_userdata(array(
            'filter_' . $params => $value
        ));
    }

    function select_boat()
    {
        $type = $this->input->post('type');
        $artikel_id = $this->input->post('artikel_id');
        $harga_id = $this->input->post('harga_id');

        $this->session->set_userdata(array(
            'artikel_id_' . $type => $artikel_id,
            'harga_id_' . $type => $harga_id
        ));

        //echo json_encode($this->session->all_userdata());
    }

    function clear_session()
    {
        $this->session->sess_destroy();
    }

    function all_session()
    {
        echo json_encode($this->session->all_userdata());
    }

    function reservation_boat()
    {
        $data = $this->data_primary();

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'home')->row();
        $data['post']->link = 'reservation';

        //echo json_encode($this->session->all_userdata());
        //exit;

        $harga_id_departure = $this->session->userdata('harga_id_departure');
        $harga_id_destination = $this->session->userdata('harga_id_destination');
        $from = $this->session->userdata('from');
        $to = $this->session->userdata('to');
        $way = $this->session->userdata('way');
        $departure = $this->session->userdata('departure');
        $return = $this->session->userdata('return');
        $adults = $this->session->userdata('adults');
        $children = $this->session->userdata('children');
        $infant = $this->session->userdata('infant');
        $from_label = $this->db->select('destination')->where('id', $from)->get('tb_destination')->row()->destination;
        $to_label = $this->db->select('destination')->where('id', $to)->get('tb_destination')->row()->destination;
        $harga_departure = $this->db->where('harga_id', $harga_id_departure)->get('tb_harga')->row();
        $harga_destination = $this->db->where('harga_id', $harga_id_destination)->get('tb_harga')->row();

        $artikel_id_departure = $this->session->userdata('artikel_id_departure');
        $artikel_id_destination = $this->session->userdata('artikel_id_destination');
        $boat_departure = $this->db->where('artikel_id', $artikel_id_departure)->get('tb_artikel')->row();
        $boat_destination = $this->db->where('artikel_id', $artikel_id_destination)->get('tb_artikel')->row();

        $total_price = ($adults * $harga_departure->price) + ($children * $harga_departure->price) + ($infant * $harga_departure->price);
        if ($way == 'return') {
            $total_price += ($adults * $harga_destination->price) + ($children * $harga_destination->price) + ($infant * $harga_destination->price);
        }

        if ($way == 'one') {
            $boat_title = $boat_departure->artikel_title;
        } else {
            $boat_title = $boat_departure->artikel_title . ' and ' . $boat_destination->artikel_title;
        }

        $data['from'] = $from;
        $data['to'] = $to;
        $data['way'] = $way;
        $data['departure'] = $departure;
        $data['return'] = $return;
        $data['adults'] = $adults;
        $data['children'] = $children;
        $data['infant'] = $infant;
        $data['from_label'] = $from_label;
        $data['to_label'] = $to_label;
        $data['way_label'] = $way == 'return' ? 'One Way & Return' : 'One Way';
        $data['captcha'] = $this->captcha();
        $data['harga_departure'] = $harga_departure;
        $data['harga_destination'] = $harga_destination;
        $data['boat_title'] = $boat_title;
        $data['total_price'] = $total_price;

        $this->template->user($data, 'reservation_boat');
    }

    function reservation_boat_send()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('country', 'Country', 'trim|required');
        $this->form_validation->set_rules('hotel', 'Hotel', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Attention',
                'message' => 'Fill form correctly',
                'form' => array(
                    'name' => form_error('name'),
                    'email' => form_error('email'),
                    'phone' => form_error('phone'),
                    'country' => form_error('country'),
                    'hotel' => form_error('hotel'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha')
                )
            ));
        } else {
            $artikel_id_departure = $this->session->userdata('artikel_id_departure');
            $artikel_id_destination = $this->session->userdata('artikel_id_destination');
            $harga_id_departure = $this->session->userdata('harga_id_departure');
            $harga_id_destination = $this->session->userdata('harga_id_destination');
            $from = $this->session->userdata('from');
            $to = $this->session->userdata('to');
            $way = $this->session->userdata('way');
            $departure = $this->session->userdata('departure');
            $return = $this->session->userdata('return');
            $adults = $this->session->userdata('adults');
            $children = $this->session->userdata('children');
            $infant = $this->session->userdata('infant');
            $from_label = $this->db->select('destination')->where('id', $from)->get('tb_destination')->row()->destination;
            $to_label = $this->db->select('destination')->where('id', $to)->get('tb_destination')->row()->destination;
            $boat_departure = $this->db->where('artikel_id', $artikel_id_departure)->get('tb_artikel')->row();
            $boat_destination = $this->db->where('artikel_id', $artikel_id_destination)->get('tb_artikel')->row();
            $way_label = $way == 'return' ? 'One Way & Return' : 'One Way';
            $harga_departure = $this->db->where('harga_id', $harga_id_departure)->get('tb_harga')->row();
            $harga_destination = $this->db->where('harga_id', $harga_id_destination)->get('tb_harga')->row();

            if ($way == 'one') {
                $boat_title = $boat_departure->artikel_title;
            } else {
                $boat_title = $boat_departure->artikel_title . ' and ' . $boat_destination->artikel_title;
            }

            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $country = $this->input->post('country');
            $hotel = $this->input->post('hotel');
            //$pickup_point = $this->input->post('pickup_point');
            $room_number = $this->input->post('room_number');
            $message = $this->input->post('message');
            $token = bin2hex(openssl_random_pseudo_bytes(64));

            $total_price = ($adults * $harga_departure->price) + ($children * $harga_departure->price) + ($infant * $harga_departure->price);
            if ($way == 'return') {
                $total_price += ($adults * $harga_destination->price) + ($children * $harga_destination->price) + ($infant * $harga_destination->price);
            }

            $data_insert = array(
                'artikel_id_departure' => $artikel_id_departure,
                'artikel_id_destination' => $artikel_id_destination,
                'harga_id_departure' => $harga_id_departure,
                'harga_id_destination' => $harga_id_destination,
                'destination_id_from' => $from,
                'destination_id_to' => $to,
                'boat_title' => $boat_title,
                'way' => $way,
                'departure_date' => $departure,
                'destination_date' => $return,
                'adults' => $adults,
                'children' => $children,
                'infant' => $infant,
                'from_label' => $from_label,
                'to_label' => $to_label,
                'boat_departure' => $boat_departure->artikel_title,
                'way_label' => $way_label,
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'country' => $country,
                'hotel' => $hotel,
                'message' => $message,
                'boat_departure_price' => $boat_departure->artikel_harga,
                'boat_total_price' => $total_price,
                'token' => $token
            );

            if ($way == 'return') {
                $data['boat_destination'] = $boat_destination->artikel_title;
                $data['boat_destination_price'] = $boat_destination->artikel_harga;
            }

            $this->db->insert('tb_booking', $data_insert);


            $messages = '
<link rel="stylesheet" href="' . base_url() . 'assets/template/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="' . base_url() . 'assets/template/style.css" type="text/css" />
	<link rel="stylesheet" href="' . base_url() . 'assets/template/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="' . base_url() . 'assets/template/css/font-icons.css" type="text/css" />


Dear Reservation,<br /><br />

I have browsed your website and I would like<br />

to book - ' . $boat_title . ' and<br />

my reservation details as follows:<br /><br />



--------------- RESERVATION FORM ---------------<br />

Full Name : ' . $name . '<br>
Email : ' . $email . '<br>
Phone Number : ' . $phone . '<br>
Nationality : ' . $country . '<br><br>

--------------- PICKUP AND DROP POINT ---------------<br />
Hotel : ' . $hotel . '<br />
Room Number : ' . $room_number . '<br /><br />

--------------- RESERVATION DATA ---------------<br />

Way Type : ' . $way_label . '<br />
From : ' . $from_label . '<br />
To : ' . $to_label . '<br />
Departure Date : ' . $departure . '<br />
Departure Time : ' . $harga_departure->time . '<br />';

            if ($way == 'return') {
                $messages .= 'Return Date : ' . $return . '<br />
                             Return Time : ' . $harga_destination->time . '<br />';
            }

            $messages .= 'Boat Departure Used : ' . $boat_departure->artikel_title . '<br />
              Boat Departure Price : $' . number_format($harga_departure->price) . '<br />';
            if ($way == 'return') {
                $messages .= 'Boat Destination Used : ' . $boat_destination->artikel_title . '<br />
                              Boat Destination Price : $' . number_format($harga_destination->price) . '<br />';
            }

            $messages .= '
Guest Hotel : ' . $hotel . '<br />
Room Number : ' . $room_number . '<br />
Adults : ' . $adults . '<br />
Children : ' . $children . '<br />
Infant : ' . $infant . '<br />
Price Total : $' . number_format($total_price) . '<br /><br />


--------------- SPECIAL MESSAGE ---------------<br />

Message : ' . $message . '<br /><br />



Regarding<br /><br />

 

' . $name . '<br />

' . $this->input->post('country') . '<br />
<br />

<a href="' . base_url('nias/check_boat_available/yes/' . $token) . '" class="button button-3d button-rounded button-green"><i class="icon-ok"></i> Available</a>
<a href="' . base_url('nias/check_boat_available/no/' . $token) . '" class="button button-3d button-rounded button-red"><i class="icon-remove"></i> Not Available</a> ';

            $email_admin = $this->db
                ->where(array(
                    'config_posisi' => 'Email',
                    'config_publish' => 'yes'
                ))
                ->get('tb_config')
                ->result();

            foreach ($email_admin as $r) {
                $this->base_value->mailer_auth($r->config_akun, $r->config_title, 'Booking - ' . $boat_title, $messages);
            }


            $messages_user = '
                Dear, ' . $name . '<br /><br />
                
                Please wait a moment, we checking your date fast boat reservation right now.<br />
                We will confirm you immediately.<br />
                And your reservation with detail as below:<br /><br />
                
--------------- RESERVATION FORM ---------------<br />
Full Name : ' . $name . '<br>
Email : ' . $email . '<br>
Phone Number : ' . $phone . '<br>
Nationality : ' . $country . '<br><br />

--------------- PICKUP AND DROP POINT ---------------<br />
Hotel : ' . $hotel . '<br />
Room Number : ' . $room_number . '<br /><br />

--------------- RESERVATION DATA ---------------<br />
Way Type : ' . $way_label . '<br />
From : ' . $from_label . '<br />
To : ' . $to_label . '<br />
Departure Date : ' . $departure . '<br />';
            if ($way == 'return') {
                $messages_user .= 'Return Date : ' . $return . '<br />';
            }
            $messages_user .= 'Boat Departure Used : ' . $boat_departure->artikel_title . '<br />
              Boat Departure Price : $' . number_format($boat_departure->artikel_harga) . '<br />';
            if ($way == 'return') {
                $messages_user .= 'Boat Destination Used : ' . $boat_destination->artikel_title . '<br />
                              boat Destination Price : $' . number_format($boat_destination->artikel_harga) . '<br />';
            }
            $messages_user .= '
Price Total : $' . number_format($total_price) . '<br />
Adults : ' . $adults . '<br />
Children : ' . $children . '<br />
Infant : ' . $infant . '<br />
Guest Hotel : ' . $hotel . '<br /><br />


--------------- SPECIAL MESSAGE ---------------<br />

Message : ' . $message . '<br /><br />

Thank you.<br />
Bali Boat Ticket Administrator';

            $this->base_value->mailer_auth($email, $name, 'Booking - ' . $boat_title, $messages_user);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Success',
                'message' => 'Your booking has been send, please wait for our reply',

            ));


            //$this->send_mail('Booking '.$boat_title, $this->input->post('email'), $messages);
        }
    }

    function reservation_activity_send() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');

        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Attention',
                'message' => 'Fill form correctly',
                'form' => array(
                    'name' => form_error('name'),
                    'email' => form_error('email'),
                    'phone' => form_error('phone'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha')
                )
            ));
        } else {
            $date = $this->input->post('date');
            $total_visitor = $this->input->post('total_visitor');
            $total_price = $this->input->post('total_price');
            $adult = $this->input->post('adult');
            $child = $this->input->post('child');
            $artikel_title = $this->input->post('artikel_title');
            $artikel_harga = $this->input->post('artikel_harga');

            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');

            $adult_title = $this->input->post('adult_title');
            $adult_name = $this->input->post('adult_name');
            $adult_nationality = $this->input->post('adult_nationality');
            $child_name = $this->input->post('child_name');
            $child_nationality = $this->input->post('child_nationality');
            $child_birthday = $this->input->post('child_birthday');






            $messages = '


Dear Reservation,<br /><br />

I have browsed your website and I would like<br />

to Book Activities - ' . $artikel_title . ' and<br />

my reservation details as follows:<br /><br />



--------------- CONTACT DETAILS ---------------<br />

Name : ' . $name. '<br>
Email : ' . $email . '<br>
Phone Number : ' . $phone . '<br>
Message : ' . $message . '<br><br>

--------------- ACTIVITIES TICKET  -------------------<br />
Activities Name : '.$artikel_title.'<br />
Activities Price : '.$artikel_harga.'<br />
Visit Date : '.$date.'<br />
Total Visitor : '.$total_visitor.'<br />
Adult Visitor : '.$adult.'<br />
Adult Child : '.$child.'<br />
Total Price : USD '.number_format($total_price).'<br /><br />

--------------- VISITOR / TRAVELER DETAIL --------------------<br />';


            $no = 1;
            foreach($adult_title as $index => $val) {
                $messages .= '<h4 style="margin:0">Adult '.$no++.'</h4>
                    Adult Title : '.$adult_title[$index].'<br />
                    Adult Name : '.$adult_name[$index].'<br />
                    Adult Nationality : '.$adult_nationality[$index].'<br /><br />';
            }

            $no = 1;
            foreach($child_name as $index => $val) {
                $messages .= '<h4 style="margin:0">Child '.$no++.'</h4>
                    Child Name : '.$child_name[$index].'<br />
                    Child Nationality : '.$child_nationality[$index].'<br />
                    Child Birthday : '.$child_birthday[$index].'<br /><br />';
            }

            $messages .= '<br /><br />';

            $messages .= '
Regarding<br /><br />

 

' . $name . '<br />';

            $email_admin = $this->db
                ->where(array(
                    'config_posisi' => 'Email',
                    'config_publish' => 'yes'
                ))
                ->get('tb_config')
                ->result();

            foreach ($email_admin as $r) {
                $this->base_value->mailer_auth($r->config_akun, $r->config_title, 'Activity Booking - ' . $artikel_title, $messages);
            }


            $messages_user = '
                Dear, ' . $name . '<br /><br />
                
                Please wait a moment, we checking your date fast boat reservation right now.<br />
                We will confirm you immediately.<br />
                And your reservation with detail as below:<br /><br />';

            $messages_user .= '--------------- CONTACT DETAILS ---------------<br />

Name : ' . $name. '<br>
Email : ' . $email . '<br>
Phone Number : ' . $phone . '<br>
Message : ' . $message . '<br><br>

--------------- ACTIVITIES TICKET  -------------------<br />
Activities Name : '.$artikel_title.'<br />
Activities Price : '.$artikel_harga.'<br />
Visit Date : '.$date.'<br />
Total Visitor : '.$total_visitor.'<br />
Adult Visitor : '.$adult.'<br />
Adult Child : '.$child.'<br />
Total Price : USD '.number_format($total_price).'<br /><br />

--------------- VISITOR / TRAVELER DETAIL --------------------<br />';

            $no = 1;
            foreach($adult_title as $index => $val) {
                $messages_user .= '<h4 style="margin:0">Adult '.$no++.'</h4>
                    Adult Title : '.$adult_title[$index].'<br />
                    Adult Name : '.$adult_name[$index].'<br />
                    Adult Nationality : '.$adult_nationality[$index].'<br /><br />';
            }

            $no = 1;
            foreach($child_name as $index => $val) {
                $messages_user .= '<h4 style="margin:0">Child '.$no++.'</h4>
                    Child Name : '.$child_name[$index].'<br />
                    Child Nationality : '.$child_nationality[$index].'<br />
                    Child Birthday : '.$child_birthday[$index].'<br /><br />';
            }

            $messages_user .= '<br /><br />';


$messages_user .= 'Thank you.<br />
Bali Boat Ticket Administrator';

            $this->base_value->mailer_auth($email, $name, 'Activity Booking - ' . $artikel_title, $messages_user);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Success',
                'message' => 'Your booking has been send, please wait for our reply',

            ));


            //$this->send_mail('Booking '.$boat_title, $this->input->post('email'), $messages);
        }
    }

    function check_boat_available($status, $token)
    {
        $this->db->where('token', $token)->update('tb_booking', array('status_available' => $status));
        $booking = $this->db->where('token', $token)->get('tb_booking')->row();

        if ($status == 'yes') {
            $message_user = '
                Dear ' . $booking->name . '<br />
            Your reservation is <strong>Available</strong><br />
            Please wait follow up from us.<br /><Br />
            
            Thank You<Br />
            Bali Boat Ticket Administrator';
        } else {
            $message_user = '
                Dear ' . $booking->name . '<br />
            Sorry, your reservation is <strong>Not Available</strong><br /><br />
            
            Thank You<br />
            Bali Boat Ticket Administrator';
        }

        $this->base_value->mailer_auth($booking->email, $booking->name, 'Booking Confirmation ', $message_user);

        redirect();
    }

    function comment_all($error = NULL, $success = NULL)
    {

        $data = $this->data_primary('comment');

        $data['error'] = $error ? $error : NULL;

        $data['success'] = $success ? $success : NULL;

        //$data['captcha'] = $this->captcha();


//----- Inisialisasi PAGINATION
        // mengambil Pagging dari URI

        $id = $this->uri->segment(2);

        // mengambil jumlah comment yang diperlihatkan

        $jum_testi = $this->mod_web->select_by_id('tb_comment', 'comment_publish', 'yes')->num_rows();

        // mengambil jumlah comment perhalaman

        $jum_page = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row()->config_akun;

        // mengambil data per jumlah halaman

        $view_data = $this->mod_web->get_testimonial_pagging($jum_page, $id)->result();


//-- Ekstrak PAGINATION Value

        $pagination = $this->pagination($jum_testi, $jum_page, $view_data);

        $data['pagination'] = $pagination['pagination'];

        $data['comment'] = $pagination['view_data'];


        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'testimonial')->row();

        $this->template->user($data, 'testimonial');
    }

    function testimonial()
    {

        $page = 'testimonial';

        $data = $this->data_primary();


        //$data['captcha'] = $this->captcha();

        $data['post'] = $this->mod_root->select_by_id('tb_artikel', 'link', $page)->row();

        $data['testimonial'] = $this->mod_root->select_by_id('tb_comment', 'comment_publish', 'yes', 'comment_id', 'DESC')->result();

        $data['pagination'] = $this->mod_root->select_by_id('tb_config', 'config_id', '1')->row()->config_akun;


        $this->template->user($data, $page);
    }

    function testimonial_send()
    {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Full Name', 'trim|required');

        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('url', 'URL / Homepage', 'trim');

        $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');


        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');


        if ($this->form_validation->run() === FALSE) {

            $this->form_validation->set_error_delimiters('- ', '');

            $error = form_error('name') .
                form_error('address') .
                form_error('email') .
                form_error('comment') .
                form_error('captcha') .
                form_error('url');


            $this->form_validation->set_error_delimiters('', '');

            $alert = '=== Warning ! === ' . $error;

            echo json_encode(array(
                'status' => 'error',
                'alert' => $alert,
                'name' => form_error('name'),
                'address' => form_error('address'),
                'email' => form_error('email'),
                'url' => form_error('url'),
                'comment' => form_error('comment'),
                'captcha' => form_error('captcha')
            ));
        } else {

            $id = $this->mod_root->increment_id('tb_comment', 'comment_id');

            $this->load->helper('date');

            $data = array(
                'comment_id' => $id,
                'comment_name' => $this->input->post('name'),
                'comment_address' => $this->input->post('address'),
                'comment_email' => $this->input->post('email'),
                'comment_url' => $this->input->post('url'),
                'comment_comment' => $this->input->post('comment'),
                'comment_date' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
                'comment_publish' => 'no'
            );


            $message = "

Full Name : " . $this->input->post('name') . "<br />

Address : " . $this->input->post('address') . "	<br />

Email : " . $this->input->post('email') . "<br />

URL / Homepage : " . $this->input->post('url') . "<br />

Comment : " . $this->input->post('comment') . "<br />



If you want to publish this comment,  you must login to " . base_url() . "www";


            $this->mod_root->insert('tb_comment', $data);

            $this->send_mail('Comment ' . $this->input->post('artikel'), $this->input->post('email'), $message);


            $alert = 'Thanks for your comment - ' . $this->input->post('name');

            echo json_encode(array('status' => 'success', 'alert' => $alert));
        }
    }

    function captcha_check($str)
    {
        if ($str != $this->session->userdata('captcha_mwz')) {
            $this->form_validation->set_message('captcha_check', 'Security Code was Wrong, Please Try Again');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function review_send()
    {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Full Name', 'trim|required');

        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('rate', 'Rate', 'trim');

        $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');


        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');


        if ($this->form_validation->run() === FALSE) {

            $this->form_validation->set_error_delimiters('- ', '');

            $error = form_error('name') .
                form_error('address') .
                form_error('email') .
                form_error('comment') .
                form_error('captcha') .
                form_error('rate');


            $this->form_validation->set_error_delimiters('', '');

            $alert = '=== Warning ! === ' . $error;

            echo json_encode(array(
                'status' => 'error',
                'alert' => $alert,
                'name' => form_error('name'),
                'address' => form_error('address'),
                'email' => form_error('email'),
                'url' => form_error('url'),
                'comment' => form_error('comment'),
                'captcha' => form_error('captcha')
            ));
        } else {

            $id = $this->mod_root->increment_id('tb_comment', 'comment_id');

            $this->load->helper('date');

            $data = array(
                'comment_id' => $id,
                'comment_name' => $this->input->post('name'),
                'comment_address' => $this->input->post('address'),
                'comment_email' => $this->input->post('email'),
                'rate' => $this->input->post('rate'),
                'artikel_id' => $this->input->post('artikel_id'),
                'comment_comment' => $this->input->post('comment'),
                'comment_date' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
                'comment_publish' => 'no'
            );


            $message = "

Full Name : " . $this->input->post('name') . "<br />

Address : " . $this->input->post('address') . "	<br />

Email : " . $this->input->post('email') . "<br />

Rate : " . $this->input->post('rate') . "<br />

Comment : " . $this->input->post('comment') . "<br />

Tour : " . $this->input->post('artikel_title') . "<br />

Tour URL : " . $this->input->post('current_url') . "<Br /><Br />



If you want to publish this comment,  you must login to " . base_url() . "www";


            $this->mod_root->insert('tb_comment', $data);

            $this->send_mail('Review - ' . $this->input->post('artikel_title'), $this->input->post('email'), $message);


            $alert = 'Thanks for your comment - ' . $this->input->post('name');

            echo json_encode(array('status' => 'success', 'alert' => $alert));
        }
    }

    function ticket()
    {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'trim|required');

        $this->form_validation->set_rules('f_name', 'First Name', 'trim|required');

        $this->form_validation->set_rules('l_name', 'Last Name', 'trim|required');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('phone', 'Telephone', 'trim|required');

        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $this->form_validation->set_rules('code', 'Postal Code', 'trim|required');

        $this->form_validation->set_rules('fax', 'Fax', 'trim|required');

        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');


        if ($this->form_validation->run() === FALSE) {

            $this->form_validation->set_error_delimiters('- ', '');

            $error = form_error('title') .
                form_error('f_name') .
                form_error('l_name') .
                form_error('email') .
                form_error('phone') .
                form_error('address') .
                form_error('code') .
                form_error('fax') .
                form_error('captcha');


            $this->form_validation->set_error_delimiters('', '');

            $alert = '=== Warning !! === ' . $error;

            echo json_encode(array(
                'status' => 'error',
                'alert' => $alert,
                'title' => form_error('title'),
                'f_name' => form_error('f_name'),
                'l_name' => form_error('l_name'),
                'email' => form_error('email'),
                'phone' => form_error('phone'),
                'address' => form_error('address'),
                'code' => form_error('code'),
                'fax' => form_error('fax'),
                'captcha' => form_error('captcha'),
            ));
        } else {

            $message = "

Dear Boat Charter KOmodo,<br /><br />

 

I have browsing your website and I would like to asking<br />

something to you. My asking as details bellow :<br /><br />



--------------- PERSONAL DATAS ---------------<br />

Full Name : " . $this->input->post('title') . " " . $this->input->post('f_name') . " " . $this->input->post('l_name') . "<br />

Telephone : " . $this->input->post('phone') . "<br />

Address : " . $this->input->post('address') . "<br />

Postal Code : " . $this->input->post('code') . "<br />

Fax : " . $this->input->post('fax') . "<br />

From : " . $this->input->post('from') . "<br />

To : " . $this->input->post('to') . "<br />

Trip Type : " . $this->input->post('trip_type') . "<br />

Departure : " . $this->input->post('departure') . "<br />

Return : " . $this->input->post('return') . "<br /><br />



--------------- MESSAGE DETAILS ---------------<br />

Message : " . $this->input->post('comment') . "<br /><br />



" . $this->input->post('title') . " " . $this->input->post('f_name') . " " . $this->input->post('l_name') . "<br />

From: " . $this->input->post('city') . " - " . $this->input->post('negara') . "<br />

Contact URL : " . base_url() . "contact-us

";


            $this->send_mail('Contact KWT Tours', $this->input->post('email'), $message);

            $alert = 'your message has been sending to KWT Tours';

            echo json_encode(array('status' => 'success', 'alert' => $alert));
        }
    }

    function contact_all($error = NULL, $success = NULL)
    {

        $data = $this->data_primary('contact');

        $data['error'] = $error ? $error : NULL;

        $data['success'] = $success ? $success : NULL;

        //$data['captcha'] = $this->captcha();

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'link', 'contact')->row();

        $this->template->user($data, 'contact');
    }

    function contact_send()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
        $this->form_validation->set_error_delimiters("<span class='form-error'>", "</span>");
        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Fill Form Correctly',
                'form' => array(
                    'name' => form_error('name'),
                    'email' => form_error('email'),
                    'subject' => form_error('subject'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha'),
                )
            ));
        } else {
            $message_admin = "

Dear BaliBoatTicket.com,<br /><br />
I have browsed your website and I would like to asking<br />
something to you. My asking details as follows :<br /><br />
---------------- PERSONAL DATA ----------------<br />
Name : " . $this->input->post('name') . " <br />
Subject : " . $this->input->post('subject') . "<br /><br />
---------------- MESSAGE DETAILS ----------------<br />
Message : " . $this->input->post('message') . "<br /><br />

Best regard<br /><br />

From: " . $this->input->post('name') . " <br />
URL : " . base_url();

            $message_user = "

Dear ".$this->input->post('name').",<br /><br />
Thank you for contact Bali Boat Ticket, We will follow up you as soon as possible<br />
here your detail when you contact us :<br /><br />
---------------- PERSONAL DATA ----------------<br />
Name : " . $this->input->post('name') . " <br />
Subject : " . $this->input->post('subject') . "<br /><br />
---------------- MESSAGE DETAILS ----------------<br />
Message : " . $this->input->post('message') . "<br /><br />

Best regard<br /><br />

From: " . $this->input->post('name') . " <br />
URL : " . base_url().'<br /><br />

by : Bali Boat Ticket System';


            $this->base_value->mailer_auth($this->input->post('email'), $this->input->post('name'), 'Contact Us', $message_user);

            $email_admin = $this->db
                ->where(array(
                    'config_posisi' => 'Email',
                    'config_publish' => 'yes'
                ))
                ->get('tb_config')
                ->result();
            foreach($email_admin as $r) {
                $this->base_value->mailer_auth($r->config_akun, $r->config_title, 'Contact Us', $message_admin);
            }

            echo json_encode(array(
                'status' => 'success',
                'message' => 'Message Success Send.'
            ));
        }
    }

    function inquiry_send()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone/WA', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required');
        $this->form_validation->set_error_delimiters("<span class='form-error'>", "</span>");
        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'form' => array(
                    'name' => form_error('name'),
                    'email' => form_error('email'),
                    'phone' => form_error('phone'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha'),
                )
            ));
        } else {
            $message = "

Dear Reservation Komodo Liveaboard,<br /><br />

I have browsed your website and I would like to request<br />
" . $this->input->post('boat_name') . ". My request details as follow :<br /><br />

--------------- PERSONAL DATA ---------------<br />
Full Name : " . $this->input->post("name") . "<br />
Email : " . $this->input->post('email') . "<br />
Phone/WA : " . $this->input->post('phone') . "<br />
Boat Name : " . $this->input->post('boat_name') . "<br /><br />

--------------- DETAIL INQUIRY ---------------<br />
Message : " . $this->input->post('message') . "<br /><br /> 

From: " . $this->input->post("name") . "<br />
Request from : " . $this->input->post('url') . "<br /><br />";
            $this->send_mail('Inquiry ' . $this->input->post('boat_name'), $this->input->post('email'), $message);
            echo json_encode(array(
                'status' => 'success'
            ));
        }
    }

    function news_detail($artikel_id = '')
    {

        $data = $this->data_primary();

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'artikel_id', $artikel_id)->row();
        $data['post']->link = 'detail_news';

        $data['galeris'] = $this->db->where('artikel_id', $artikel_id)
            ->order_by('galeri_id', 'DESC')
            ->get('tb_galeri')
            ->result();

        $data['related_news'] = $this->db
            ->where(array('posisi' => 'artikel', 'link' => 'news'))
            ->where_not_in('artikel_id', array($artikel_id))
            ->order_by('artikel_id', 'DESC')
            ->get('tb_artikel', 8, 0)
            ->result();

        $this->template->user($data, 'detail_news');
    }

    function news($page)
    {

        $artikel = $this->mod_web->select('tb_artikel')->result();

        $find = array(' ', '/', '&', '\\');

        $replace = array('-', '-', '-', '-');

        foreach ($artikel as $row) {

            $artikel_title = str_replace($find, $replace, strtolower($row->artikel_title));

            if ($artikel_title == $page) {

                $id_ar = $row->artikel_id;
            }
        }

        $data = $this->data_primary($id_ar);

        $data['page'] = 'yes';

        $data['reservation'] = 'yes';

        $data['post'] = $this->mod_web->select_by_id('tb_artikel', 'artikel_id', $id_ar)->row();

        $this->template->user($data, 'detail');
    }

    function reservation_tour($artikel_id = '')
    {

        $this->load->helper('datepicker');

        $data = $this->data_primary();


        $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();

        //$data['captcha'] = $this->captcha();

        $data['post'] = $this->mod_root->select_by_id('tb_artikel', 'link', 'reservation')->row();

        $data['title'] = $tour->artikel_title;

        $data['open_trip_date'] = $tour->open_trip_date;


        switch ($tour->kategori_id) {

            case '21' :
                $data['reserv_type'] = 'hotel';
                break;

            case '0' :
                $data['reserv_type'] = 'open_trip';
                break;

            default :
                $data['reserv_type'] = 'tour';
                break;
        }


        if ($tour->link == 'open_trip') {

            $data['related_tour'] = $this->db->where_not_in('artikel_id', array($artikel_id))
                ->where(array(
                    'posisi' => 'artikel',
                    'link' => 'open_trip',
                ))
                ->order_by('artikel_id', 'ASC')
                ->get('tb_artikel')
                ->result();
        } else {

            $data['related_tour'] = $this->db->where_not_in('artikel_id', array($artikel_id))
                ->where(array(
                    'posisi' => 'artikel',
                    'link' => 'tour',
                    'kategori_id' => '21'
                ))
                ->order_by('artikel_id', 'ASC')
                ->get('tb_artikel')
                ->result();
        }


        $data['sidebar_tour'] = $this->load->view('user/sidebar_tour', $data, TRUE);

        $data['sidebar_open_trip'] = $this->load->view('user/sidebar_open_trip', $data, TRUE);

        $this->template->user($data, 'reservation_tour');
    }

    function reserv_harga()
    {
        $adult = $this->input->post('adult');
        $children = $this->input->post('children');
        $infant = $this->input->post('infant');
        $artikel_id = $this->input->post('artikel_id');
        $price_tipe = $this->input->post('price_tipe');
        $config = $this->db->where('config_id', '1')->get('tb_config')->row();
        $config_children = $config->config_children;
        $config_infant = $config->config_infant;
        $pre_payment = $config->config_pre_payment;
        $usd_to_idr = $config->config_idr_to_usd;
        $harga_plus = $config->config_harga_plus;


        $price = $this->db->where(array(
            'artikel_id' => $artikel_id,
            'count_adult' => $adult
        ))
            ->get('tb_payment')
            ->row();


        if ($price_tipe == 'idr') {
            $price_base = $usd_to_idr * $price->price_adult;
            $harga_plus = $usd_to_idr * $harga_plus;
        } else {
            $price_base = $price->price_adult;
        }

        $price_adult = ($price_base * $adult) + ($harga_plus * $adult);

        $price_adult_base = $price_adult / $adult;

        $price_child = ceil(($price_adult_base * ($config_children / 100)) * $children);
        $price_infant = ceil(($price_adult_base * ($config_infant / 100)) * $infant);
        $harga_total = $price_adult + $price_child + $price_infant;

        $pay_now = ($pre_payment / 100) * $harga_total;
        $next_payment = $harga_total - $pay_now;

        $response = array(
            'pre_payment' => number_format($pre_payment),
            'pay_now' => number_format($pay_now),
            'next_payment' => number_format($next_payment),
            'harga_total' => number_format($harga_total),
            'harga_adult' => $price_adult,
            'harga_children' => $price_child,
            'harga_infant' => $price_infant
        );

        echo json_encode($response);

    }

    function reservation($artikel_id = '', $artikel_id_itinerary = '')
    {
        //$this->load->helper('datepicker');

        $data = $this->data_primary();

        $boat = $this->db->where('artikel_id', $artikel_id_itinerary)->get('tb_artikel')->row();


        $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();

        //$data['captcha'] = $this->captcha();

        $data['post'] = $this->mod_root->select_by_id('tb_artikel', 'link', 'reservation')->row();

        $data['title'] = $tour->artikel_title;
        $data['artikel_id'] = $artikel_id;
        $data['artikel_id_itinerary'] = $artikel_id_itinerary;
        $data['config'] = $this->db->where('config_id', '1')->get('tb_config')->row();

        $data['open_trip_date'] = $tour->open_trip_date;

        switch ($tour->kategori_id) {

            case '21' :
                $data['reserv_type'] = 'hotel';
                break;

            case '0' :
                $data['reserv_type'] = 'open_trip';
                break;

            default :
                $data['reserv_type'] = 'tour';
                break;
        }


        if ($tour->link == 'open_trip') {

            $data['related_tour'] = $this->db->where_not_in('artikel_id', array($artikel_id))
                ->where(array(
                    'posisi' => 'artikel',
                    'link' => 'open_trip',
                ))
                ->order_by('artikel_id', 'ASC')
                ->get('tb_artikel')
                ->result();
        } else {

            $data['related_tour'] = $this->db->where_not_in('artikel_id', array($artikel_id))
                ->where(array(
                    'posisi' => 'artikel',
                    'link' => 'tour',
                    'kategori_id' => '21'
                ))
                ->order_by('artikel_id', 'ASC')
                ->get('tb_artikel')
                ->result();
        }

        $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
            ->get('tb_calendar')
            ->result();
        $tanggal = array();
        foreach ($data['data_list'] as $r) {
            $tanggal[] = $r->date;
        }

        $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
        $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);

        $data['date'] = array();
        $no = 0;
        $date_now = date('Y-m-d');
        foreach ($daterange as $date) {
            $date_cal = $date->format('Y-m-d');

            /*if ($date_cal < $date_now) {
              $status = 'not_available';
            } elseif (in_array($date_cal, $tanggal)) {
              $status = 'not_available';
            } else {
              $status = 'available';
            }*/

            /*$status = 'available';
            if ($date_cal < $date_now) {
              $status = 'not_available';
            }else{
              $query = $this->db->where('artikel_id', $artikel_id)
                ->where('date', $date_cal)
                ->get('tb_calendar');
                if ($query->num_rows() > 0){
                  $row = $query->row();
                  if($row->opentrip==0)
                    $status = 'not_available';
                  else
                    $status = 'open_trip';
                }
            }*/

            $status = 'available';
            if ($date_cal < $date_now) {
                $status = 'not_available';
            } else {
                $query = $this->db->where('artikel_id', $artikel_id)
                    ->where('date', $date_cal)
                    ->get('tb_calendar');
                if ($query->num_rows() > 0) {
                    $status = 'not_available';
                } else {
                    if ($artikel_id_itinerary > 0) {
                        $query = $this->db->where('artikel_id', $artikel_id_itinerary)
                            ->where('date', $date_cal)
                            ->get('tb_calendar');
                        if ($query->num_rows() > 0) {
                            $row = $query->row();
                            if ($row->opentrip == 0)
                                $status = 'not_available';
                            else
                                $status = 'open_trip';
                        }
                    }
                }
            }

            $data['date'][$date_cal]['status'] = $status;
            //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
            //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

            $no++;
        }

        $payment = $this->db->distinct()
            ->select('count_adult')
            ->where(array(
                'artikel_id' => $artikel_id_itinerary
            ))
            ->get('tb_payment')
            ->result();
        $data['adult'] = array();
        foreach ($payment as $r) {
            $data['adult'][] = $r->count_adult;
        }

        $data['sidebar_tour'] = $this->load->view('user/sidebar_tour', $data, TRUE);

        $data['sidebar_open_trip'] = $this->load->view('user/sidebar_open_trip', $data, TRUE);

        $data['itinerary'] = $this->db->where('artikel_id', $artikel_id_itinerary)->get('tb_artikel')->row();

        if ($tour->link == 'open_trip') {
            $this->load->helper('datepicker');
            $this->template->user($data, 'reservation_tour');
        } else {
            $this->template->user_2($data, 'reservation');
        }
    }

    function reservation_send()
    {

        $reserv_type = $this->input->post('reserv_type');

        if ($reserv_type == 'tour') {

            $valid_date = 'Charter Date';
        } else {

            $valid_date = 'Tour Date';
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'trim');

        $this->form_validation->set_rules('f_name', 'First Name', 'trim|required');

        $this->form_validation->set_rules('l_name', 'Last Name', 'trim|required');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('phone', 'Telephone', 'trim|required');

        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $this->form_validation->set_rules('city', 'City', 'trim|required');

        $this->form_validation->set_rules('country', 'Country', 'trim');

        //$this->form_validation->set_rules('code', 'Postal Code', 'trim|required');

        $this->form_validation->set_rules('date_in', $valid_date, 'trim|required');

        /* $this->form_validation->set_rules('date_in', 'Tour End', 'trim|required'); */

        $this->form_validation->set_rules('adult', 'Number of Adult', 'trim|required');

        $this->form_validation->set_rules('children', 'Children', 'trim|required');

        $this->form_validation->set_rules('comment', 'Comment', 'trim');

        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');


        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');


        if ($this->form_validation->run() === FALSE) {

            $alert = 'Please Completly the Form';

            echo json_encode(array(
                'status' => 'error',
                'alert' => $alert,
                'title' => form_error('title'),
                'f_name' => form_error('f_name'),
                'l_name' => form_error('l_name'),
                'email' => form_error('email'),
                'phone' => form_error('phone'),
                'address' => form_error('address'),
                'city' => form_error('city'),
                'country' => form_error('country'),
                //'code' => form_error('code'),
                'date_in' => form_error('date_in'),
                /* 'date_out'=>form_error('date_out'), */
                'adult' => form_error('adult'),
                'children' => form_error('children'),
                'comment' => form_error('comment'),
                'captcha' => form_error('captcha'),
            ));
        } else {

            $artikel_id_itinerary = $this->input->post('artikel_id_itinerary');
            $itinerary = $this->db->where('artikel_id', $artikel_id_itinerary)->get('tb_artikel')->row();

            if ($reserv_type == 'tour') {

                $label_subject = 'Booking';

                $label_date = 'Charter Date';

                $label_title = 'Boat Name';
            } elseif ($reserv_type == 'open_trip') {

                $label_subject = 'Open Trip';

                $label_date = 'Open Trip Date';

                $label_title = 'Open Trip Package';
            } else {

                $label_subject = 'Booking';

                $label_date = 'Tour Date';

                $label_title = 'Tour Programe';
            }

            $itinerary = $this->db->where('artikel_id', $this->input->post('artikel_id_itinerary'))->get('tb_artikel')->row();

            //lainnya

            $messages = '

Dear Reservation,<br /><br />

 

I have browsed your website and I would like<br />

to book - ' . $this->input->post('artikel_nama') . ' - ' . $itinerary->artikel_title . ' and<br />

my reservation details as follows:<br /><br />



--------------- PERSONAL DATA ---------------<br />

Full Name : ' . $this->input->post('title') . ' ' . $this->input->post('f_name') . ' ' . $this->input->post('l_name') . '<br>

Email : ' . $this->input->post('email') . '<br>

Telephone : ' . $this->input->post('phone') . '<br>

Address : ' . $this->input->post('address') . '<br>

City : ' . $this->input->post('city') . '<br>

Country : ' . $this->input->post('country') . '<br><br />



--------------- RESERVATION DATA ---------------<br />

' . $label_title . ' : ' . $this->input->post('artikel_nama') . '<br />
Package : ' . $itinerary->artikel_title . '<br />

' . $label_date . ' : ' . date('d F Y', strtotime($this->input->post('date_in'))) . '<br>

Number of adult : ' . $this->input->post('adult') . '<br>
Number of child: ' . $this->input->post('children') . '<br />
Number of infant : ' . $this->input->post('infant') . '<br><br />



--------------- SPECIAL MESSAGE ---------------<br />

Comment : ' . $this->input->post('comment') . '<br /><br />



Regarding<br /><br />

 

' . $this->input->post('title') . ' ' . $this->input->post('f_name') . ' ' . $this->input->post('l_name') . '<br />

' . $this->input->post('city') . ' - ' . $this->input->post('country') . '<br />

Reservation URL : ' . $this->input->post('link');;


            $this->send_mail($label_subject . ' - ' . $this->input->post('artikel_nama') . ' ' . $this->input->post('artikel_desti'), $this->input->post('email'), $messages);

            /*
            $data = array(
              'artikel_id'=>$itinerary->itinerary_artikel_id,
              'itinerary_id'=>$artikel_id_itinerary,
              'agent_name'=>'website',
              'guest_name'=>$this->input->post('title') . ' ' . $this->input->post('f_name') . ' ' . $this->input->post('l_name'),
              'guest_nationality'=>$this->input->post('country'),
              'phone'=>$this->input->post('phone'),
              'address'=>$this->input->post('address').' '.$this->input->post('city'),
              'charter_date'=>$this->input->post('date_in'),
              'adults'=>$this->input->post('adult'),
              'childs'=>$this->input->post('children'),
              'infants'=>$this->input->post('infant'),
              'log'=>json_encode($_POST),
              'created_at'=> date('Y-m-d')
            );
            $this->mod_root->insert('bookings', $data);
            */

            $data_payment = array(
                'artikel_id' => $this->input->post('artikel_id'),
                'artikel_id_itinerary' => $this->input->post('artikel_id_itinerary'),
                'artikel_nama' => $this->input->post('artikel_nama'),
                'itinerary_title' => $this->input->post('itinerary_artikel'),
                'adult' => $this->input->post('adult'),
                'children' => $this->input->post('children'),
                'infant' => $this->input->post('infant'),
                'price_tipe' => $this->input->post('price_tipe'),
                'total' => $this->input->post('total'),
                'persen_dp' => $this->input->post('persen_dp'),
                'persen_nominal' => $this->input->post('persen_nominal'),
                'pay_now' => $this->input->post('pay_now'),
                'next_payment' => $this->input->post('next_payment'),
                'title' => $this->input->post('title'),
                'first_name' => $this->input->post('f_name'),
                'last_name' => $this->input->post('l_name'),
                'email' => $this->input->post('email'),
                'telephone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'country' => $this->input->post('country'),
                'inquiry' => $this->input->post('comment'),
                'payment_method' => $this->input->post('payment_method'),
                'date_insert' => date('Y-m-d H:i:s'),
                'date_in' => $this->input->post('date_in'),
                'harga_adult' => $this->input->post('harga_adult'),
                'harga_children' => $this->input->post('harga_children'),
                'harga_infant' => $this->input->post('harga_infant')
            );

            $this->db->insert('tb_booking', $data_payment);
            $payment_id = $this->db->insert_id();
            $this->session->set_userdata(array('payment_id' => $payment_id));


            $artikel_id_partner = $this->db->get('tb_partner')
                ->result();
            $prt_nama = '';
            $artikel_id = $this->input->post('artikel_id');
            $artikel = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
            $artikel->artikel_remark = $this->remark_list($artikel_id);
            $email_owner_kapal = '';
            foreach ($artikel_id_partner as $r) {
                $artikel_id_arr = json_decode($r->artikel_id, TRUE);
                if (in_array($artikel_id, $artikel_id_arr)) {
                    $prt_nama = $r->prt_nama;
                    $email_owner_kapal = $r->prt_email;
                }
            }


            $message_owner_kapal = '
        Dear ' . $artikel->artikel_title . '<br />
Att: Mr./Mrs: ' . $prt_nama . '<br /><br />

Warmest greeting from Boat Charter Komodo.<Br /><Br />

Boat Charter Komodo have new request for ' . $artikel->artikel_title . ', with details as follows:<br /><br />

Boat Name : ' . $artikel->artikel_title . '<br />
Package : ' . $itinerary->artikel_title . '<br />
Periode : ' . date('d F Y', strtotime($this->input->post('date_in'))) . '<br />
Guest Name: ' . $this->input->post('f_name') . ' ' . $this->input->post('l_name') . '<br />
Adult : ' . $this->input->post('adult') . ' Pax - Child : ' . $this->input->post('children') . ' Pax - Infant : ' . $this->input->post('infant') . ' Pax <br /><br />
  
    ' . $artikel->artikel_remark . '<br /><BR />
  Tour Program:<br />
    ' . $itinerary->artikel_isi . '
  <Br /><br />
    Thank you and looking forward to hearing from you.<Br /><Br />
      Marcelino Sunjaya<Br />
      Operation Manager<Br />
    <Br />
    <a href="http://www.boatcharterkomodo.com" target="_blank"><img src="' . base_url() . 'assets/img/user/boatcharter.png"></a><Br />
    <strong>HEAD OFFICE:</strong><br />
    Jalan Soekarno Hatta Labuan Bajo Flores<br />
    <strong>W</strong>: <a href="http://www.boatcharterkomodo.com" target="_blank">www.boatcharterkomodo.com</a> | <strong>E</strong> : <a href="mailto:info@komodotours.co.id">info@komodotours.co.id</a><br />
    <strong>F</strong> : <a href="https://www.facebook.com/komodotoursadventure" target="_blank">https://www.facebook.com/komodotoursadventure</a><br />
    <strong>T</strong> : <a href="http://visitkomodotours.tumblr.com" target="_blank">http://visitkomodotours.tumblr.com</a> <strong>Twitter</strong>: <a href="https://twitter.com/komodoboattours">@komodoboattours</a><br />
    <strong>M</strong> : +62.812399.22222 <strong>WA</strong>: +62.81236016914  <strong>PIN BB</strong>: 5AF83360 <strong>Skype</strong>: indonesiarooms<br />';

            $this->send_mail_from_admin('Reservation ' . $artikel->artikel_title, $email_owner_kapal, $message_owner_kapal);

            $data_max = array(
                'max_adult' => $this->input->post('adult'),
                'max_children' => $this->input->post('children'),
                'max_infant' => $this->input->post('infant'),
                'date_in' => $this->input->post('date_in'),
                'title' => $this->input->post('title'),
                'first_name' => $this->input->post('f_name'),
                'last_name' => $this->input->post('l_name'),
                'user_email' => $this->input->post('email'),
            );
            $this->session->set_userdata($data_max);

            $alert = 'Thank for your reservation, we will reply your reservation';


            echo json_encode(array(
                'status' => 'success',
                'alert' => $alert,
            ));
        }
    }

    function reservation_send_tour()
    {

        $reserv_type = $this->input->post('reserv_type');

        if ($reserv_type == 'tour') {

            $valid_date = 'Charter Date';
        } else {

            $valid_date = 'Tour Date';
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'trim');

        $this->form_validation->set_rules('f_name', 'First Name', 'trim|required');

        $this->form_validation->set_rules('l_name', 'Last Name', 'trim|required');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('phone', 'Telephone', 'trim|required');

        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $this->form_validation->set_rules('city', 'City', 'trim|required');

        $this->form_validation->set_rules('country', 'Country', 'trim');

        $this->form_validation->set_rules('code', 'Postal Code', 'trim|required');

        $this->form_validation->set_rules('date_in', $valid_date, 'trim|required');

        /* $this->form_validation->set_rules('date_in', 'Tour End', 'trim|required'); */

        $this->form_validation->set_rules('adult', 'Number of Adult', 'trim|required');

        $this->form_validation->set_rules('children', 'Children', 'trim|required');

        $this->form_validation->set_rules('comment', 'Comment', 'trim');

        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');


        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');


        if ($this->form_validation->run() === FALSE) {

            $alert = 'Please Completly the Form';

            echo json_encode(array(
                'status' => 'error',
                'alert' => $alert,
                'title' => form_error('title'),
                'f_name' => form_error('f_name'),
                'l_name' => form_error('l_name'),
                'email' => form_error('email'),
                'phone' => form_error('phone'),
                'address' => form_error('address'),
                'city' => form_error('city'),
                'country' => form_error('country'),
                'code' => form_error('code'),
                'date_in' => form_error('date_in'),
                /* 'date_out'=>form_error('date_out'), */
                'adult' => form_error('adult'),
                'children' => form_error('children'),
                'comment' => form_error('comment'),
                'captcha' => form_error('captcha'),
            ));
        } else {


            if ($reserv_type == 'tour') {

                $label_subject = 'Booking';

                $label_date = 'Charter Date';

                $label_title = 'Boat Name';
            } elseif ($reserv_type == 'open_trip') {

                $label_subject = 'Open Trip';

                $label_date = 'Open Trip Date';

                $label_title = 'Open Trip Package';
            } else {

                $label_subject = 'Booking';

                $label_date = 'Tour Date';

                $label_title = 'Tour Programe';
            }

            //lainnya

            $messages = '

Dear Reservation,<br /><br />

 

I have browsed your website and I would like<br />

to book - ' . $this->input->post('artikel_nama') . '&nbsp;' . $this->input->post('artikel_desti') . ' and<br />

my reservation details as follows:<br /><br />



--------------- PERSONAL DATA ---------------<br />

Full Name : ' . $this->input->post('title') . ' ' . $this->input->post('f_name') . ' ' . $this->input->post('l_name') . '<br>

Email : ' . $this->input->post('email') . '<br>

Telephone : ' . $this->input->post('phone') . '<br>

Address : ' . $this->input->post('address') . '<br>

City : ' . $this->input->post('city') . '<br>

Country : ' . $this->input->post('country') . '<br>

Postal Code : ' . $this->input->post('code') . '<br /><br />



--------------- RESERVATION DATA ---------------<br />

' . $label_title . ' : ' . $this->input->post('artikel_nama') . '<br />

' . $label_date . ' : ' . date('d F Y', strtotime($this->input->post('date_in'))) . '<br>

Number of adult : ' . $this->input->post('adult') . '<br>

Number of Child: ' . $this->input->post('children') . '<br /><br />



--------------- SPECIAL MESSAGE ---------------<br />

Comment : ' . $this->input->post('comment') . '<br /><br />



Regarding<br /><br />

 

' . $this->input->post('title') . ' ' . $this->input->post('f_name') . ' ' . $this->input->post('l_name') . '<br />

' . $this->input->post('city') . ' - ' . $this->input->post('country') . '<br />

Reservation URL : ' . $this->input->post('link');


            $this->send_mail($label_subject . ' - ' . $this->input->post('artikel_nama') . ' ' . $this->input->post('artikel_desti'), $this->input->post('email'), $messages);


            $alert = 'Thank for your reservation, we will reply your reservation';


            echo json_encode(array(
                'status' => 'success',
                'alert' => $alert,
                'type' => 'open_trip'
            ));
        }
    }

    function payment($artikel_id, $artikel_id_itinerary)
    {
        $max_adult = $this->session->userdata('max_adult');
        $max_children = $this->session->userdata('max_children');
        $max_infant = $this->session->userdata('max_infant');
        $date_in = $this->session->userdata('date_in');
        $title = $this->session->userdata('title');
        $first_name = $this->session->userdata('first_name');
        $last_name = $this->session->userdata('last_name');
        $user_email = $this->session->userdata('user_email');
        $payment_id = $this->session->userdata('payment_id');
        $booking = $this->db->where('payment_id', $payment_id)->get('tb_booking')->row();

//		echo $artikel_id.' - '.$max_adult.' - '.$max_children;

        $data = $this->data_primary();

        $data['max_adult'] = $max_adult;
        $data['max_children'] = $max_children;
        $data['max_infant'] = $max_infant;
        $data['date_in'] = $date_in;
        $data['title_name'] = $title;
        $data['first_name'] = $first_name;
        $data['last_name'] = $last_name;
        $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $payment = $this->db->where(array(
            'artikel_id' => $artikel_id_itinerary,
            //'count_child'=>$max_children,
            'count_adult' => $max_adult,
        ))
            ->get('tb_payment');
        //echo $artikel_id;
        $config = $this->db->where(array('config_id' => '1'))->get('tb_config')->row();
        if ($payment->num_rows() == 0) {
            $price_adult = 0;
        } else {
            $price_adult = $payment->row()->price_adult + $config->config_harga_plus;
        }
        $data['price_adult'] = $price_adult * $max_adult;
        $data['price_child'] = ($price_adult * ($config->config_children / 100)) * $max_children;
        $data['price_infant'] = ($price_adult * ($config->config_infant / 100)) * $max_infant;

        $data['tour'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $artikel_id)->row();
        $data['tour']->artikel_remark = $this->remark_list_email($artikel_id);
        $data['package'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $artikel_id_itinerary)->row();
        $data['post'] = $this->mod_root->select_by_id('tb_artikel', 'link', 'payment')->row();
        $data['title'] = $tour->artikel_title;
        $data['paypal_fee'] = $this->db->where('config_id', '1')->get('tb_config')->row()->config_akun;
        $data['pre_payment'] = $this->db->where('config_id', '1')->get('tb_config')->row()->config_pre_payment;

        $data['total_pax'] = $data['price_adult'] + $data['price_child'] + $data['price_infant'];
        $data['total_payment'] = round($data['total_pax'], 2);
        $data['total_pre_payment'] = round($data['total_payment'] * ($data['pre_payment'] / 100), 2);
        $data['total_paypal'] = $data['total_pre_payment'] * ($data['paypal_fee'] / 100);
        $data['total_pre_payment'] = round($data['total_pre_payment'] + $data['total_paypal'], 2);
        $data['booking'] = $booking;
        $data['config'] = $config;


        $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();

        switch ($tour->kategori_id) {

            case '21' :
                $data['reserv_type'] = 'hotel';
                break;

            case '0' :
                $data['reserv_type'] = 'open_trip';
                break;

            default :
                $data['reserv_type'] = 'tour';
                break;
        }

        $data['related_tour'] = $this->db->where_not_in('artikel_id', array($artikel_id))
            ->where(array(
                'posisi' => 'artikel',
                'link' => 'tour',
                'kategori_id' => '21'
            ))
            ->order_by('artikel_id', 'ASC')
            ->get('tb_artikel')
            ->result();

        $messages = $this->load->view('user/payment_email', $data, TRUE);

        $this->send_mail_from_admin('Booking - ' . $data['tour']->artikel_title, $user_email, $messages);

        $this->template->user_2($data, 'payment');
    }

    function payment_send()
    {
        //$this->load->helper(array('captcha', 'string'));

        $max_adult = $this->input->post('max_adult');
        $max_child = $this->input->post('max_children');
        $max_infant = $this->input->post('max_infant');
        $price_adult = $this->input->post('price_adult');
        $price_child = $this->input->post('price_children');
        $price_infant = $this->input->post('price_infant');
        $artikel_title = $this->input->post('artikel_title');

        $session = array(
            'paypal_val' => $this->input->post(NULL)
        );
        $this->session->set_userdata($session);

        $paypal_account = $this->db->where('config_id', '1')->get('tb_config')->row()->config_sub_kategori;

        $config['business'] = $paypal_account;
        $config['cpp_header_image'] = ''; //Image header url [750 pixels wide by 90 pixels high]
        $config['return'] = base_url('nias/paypal_success');
        $config['cancel_return'] = base_url();
        $config['notify_url'] = 'process_payment.php'; //IPN Post
        $config['production'] = TRUE; //Its false by default and will use sandbox
        $config["invoice"] = random_string('numeric', 8); //The invoice id

        $this->load->library('paypal', $config);

        #$this->paypal->add(<name>,<price>,<quantity>[Default 1],<code>[Optional]);
        $fee_paypal = $this->db->where('config_id', '1')->get('tb_config')->row()->config_akun;
        $pre_payment = $this->db->where('config_id', '1')->get('tb_config')->row()->config_pre_payment;
        $total_price = $price_adult + $price_child + $price_infant;
        $total_pre_payment = $total_price * ($pre_payment / 100);
        $persen_paypal = $total_pre_payment * ($fee_paypal / 100);
        $price_send = round(($persen_paypal + $total_pre_payment), 2);


        $this->paypal->add($artikel_title, $price_send); //First item
        //$this->paypal->add('Price of Children', $price_child); //First item
        //	$this->paypal->add('Pants',40); 	  //Second item
        //$this->paypal->add('Blowse',10,10,'B-199-26'); //Third item with code

        $this->paypal->pay(); //Proccess the payment
    }

    function paypal_success()
    {
        $payment_id = $this->session->userdata('payment_id');
        $no_invoice = $this->no_invoice($payment_id);
        $data = $this->data_primary('home');
        $data['booking'] = $this->db->where('payment_id', $payment_id)->get('tb_booking')->row();

        $from = 'sales@boatcharterkomodo.com';
        $this->pdf_detail_tour();
        $this->pdf_invoice();
        $this->pdf_payment_receipt();

        $file_all = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            'file/BCK-' . $no_invoice . '-payment_receipt.pdf',
            //'file/BCK-'.$payment_id.'-next_payment.pdf',
        );
        $file_pemesan = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            'file/BCK-' . $no_invoice . '-payment_receipt.pdf',
            'file/BCK-' . $no_invoice . '-invoice.pdf'
        );
        $file_bck = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            'file/BCK-' . $no_invoice . '-payment_receipt.pdf',
            'file/BCK-' . $no_invoice . '-invoice.pdf'
        );
        $file_one = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
        );

        $artikel_id_partner = $this->db->get('tb_partner')
            ->result();
        $prt_nama = '';
        $artikel_id = $data['booking']->artikel_id;
        //$artikel = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $email_owner_kapal = '';
        foreach ($artikel_id_partner as $r) {
            $artikel_id_arr = json_decode($r->artikel_id, TRUE);
            if (in_array($artikel_id, $artikel_id_arr)) {
                $prt_nama = $r->prt_nama;
                $email_owner_kapal = $r->prt_email;
            }
        }

        // Booking Pemesan Kapal
        $message_pemesan = '
        Dear ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '<br /><br />
        
        Warmest greeting from Boat Charter Komodo.<br /><br />
        
        Thank you for your reservation and make pre-payment with us. Please see on the attachment file for your detail tour, payment receipt and next invoice payment.<br /><br />

        Thank you and looking forward to hearing from you<br /><br /><br />


        Marcelino Sunjaya<br />
        Operation Manager
    ';
        $message_pemilik_kapal = '
        Dear ' . $data['booking']->artikel_nama . '<br />
        Att: Mr./Mrs.: ' . $prt_nama . '<br /><br />
        
        Please look at  attachment file for detail tour of ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '<br /><br /><br />

        Regard <Br /><Br />
        
        Marcelino Sunjaya<br />
        Operation Manager
    ';
        $message_bck = '
        Dear Reservation Boat Charter Komodo,<br /><br />
        
        Warmest greeting from ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '<br /><br />
        
        Please check attachment (detail tour, payment receipt and invoice) of my reservation<br /><br /><br />

        Regard <Br /><Br />
        
        ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '
    ';

        $this->send_mail_free('Booking - ' . $data['booking']->artikel_nama, $from, $data['booking']->email, $message_pemesan, $file_pemesan);
        $this->send_mail_free('Booking - ' . $data['booking']->artikel_nama, $from, $from, $message_bck, $file_bck);
        $this->send_mail_free('Booking - ' . $data['booking']->artikel_nama, $from, $email_owner_kapal, $message_pemilik_kapal, $file_one);

        $this->delete_pdf($no_invoice);

        $data['config'] = $this->db->where('config_id', 1)->get('tb_config')->row();

        $date = new DateTime($data['booking']->date_insert);
        $date->modify("+" . $data['config']->config_plus_hour . " hours");
        $data['plus_date'] = $date->format("Y-m-d - h:i:s A");
        $data['artikel'] = $this->db->where('artikel_id', $data['booking']->artikel_id)->get('tb_artikel')->row();
        $data['itinerary'] = $this->db->where('artikel_id', $data['booking']->artikel_id_itinerary)->get('tb_artikel')->row();
        $data['sidebar'] = $this->load->view('user/sidebar', $data, TRUE);
        //echo json_encode($booking);
        $this->template->user_2($data, 'paypal_success');
    }

    function no_invoice($payment_id)
    {
        return str_pad($payment_id, 8, '0', STR_PAD_LEFT);
    }

    function bank_transfer()
    {
        $payment_id = $this->session->userdata('payment_id');
        $no_invoice = $this->no_invoice($payment_id);
        $data = $this->data_primary('home');
        $data['booking'] = $this->db->where('payment_id', $payment_id)->get('tb_booking')->row();
        $data['remark'] = $this->remark_list($data['booking']->artikel_id);
        $data['sidebar'] = $this->load->view('user/sidebar', $data, TRUE);

        $from = 'sales@boatcharterkomodo.com';
        $this->pdf_detail_tour();
        $this->pdf_invoice();
        $this->pdf_payment_receipt();

        $file_all = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            'file/BCK-' . $no_invoice . '-payment_receipt.pdf',
            //'file/BCK-'.$payment_id.'-next_payment.pdf',
        );
        $file_pemesan = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            //'file/BCK-'.$no_invoice.'-payment_receipt.pdf',
            'file/BCK-' . $no_invoice . '-invoice.pdf'
        );
        $file_bck = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            //'file/BCK-'.$no_invoice.'-payment_receipt.pdf',
            'file/BCK-' . $no_invoice . '-invoice.pdf'
        );
        $file_one = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
        );

        $artikel_id_partner = $this->db->get('tb_partner')
            ->result();
        $prt_nama = '';
        $artikel_id = $data['booking']->artikel_id;
        //$artikel = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $email_owner_kapal = '';
        foreach ($artikel_id_partner as $r) {
            $artikel_id_arr = json_decode($r->artikel_id, TRUE);
            if (in_array($artikel_id, $artikel_id_arr)) {
                $prt_nama = $r->prt_nama;
                $email_owner_kapal = $r->prt_email;
            }
        }

        // Booking Pemesan Kapal
        $message_pemesan = '
        Dear ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '<br /><br />
        
        Warmest greeting from Boat Charter Komodo.<br /><br />
        
        Thank you for your reservation. To secure your reservation, please make pre-payment. Look at attachment file for detail tour and pre-payment invoice.<br /><br />

        Thank you and looking forward to hearing from you<br /><br /><br />


        Marcelino Sunjaya<br />
        Operation Manager
    ';
        $message_pemilik_kapal = '
        Dear ' . $data['booking']->artikel_nama . '<br />
        Att: Mr./Mrs.: ' . $prt_nama . '<br /><br />
        
        Please look at  attachment file for detail tour of ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '<br /><br /><br />

        Regard <Br /><Br />
        
        Marcelino Sunjaya<br />
        Operation Manager
    ';
        $message_bck = '
        Dear Reservation Boat Charter Komodo,<br /><br />
        
        Warmest greeting from ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '<br /><br />
        
        Please check attachment (detail tour and invoice) of my reservation<br /><br /><br />

        Regard <Br /><Br />
        
        ' . $data['booking']->title . ' ' . $data['booking']->first_name . ' ' . $data['booking']->last_name . '
    ';

        $this->send_mail_free('Booking - ' . $data['booking']->artikel_nama, $from, $data['booking']->email, $message_pemesan, $file_pemesan);
        $this->send_mail_free('Booking - ' . $data['booking']->artikel_nama, $from, $from, $message_bck, $file_bck);
        $this->send_mail_free('Booking - ' . $data['booking']->artikel_nama, $from, $email_owner_kapal, $message_pemilik_kapal, $file_one);

        $this->delete_pdf($no_invoice);

        $this->template->user_2($data, 'bank_transfer');
    }

    function delete_pdf($no_invoice)
    {
        $path = array(
            'file/BCK-' . $no_invoice . '-detail_tour.pdf',
            'file/BCK-' . $no_invoice . '-payment_receipt.pdf',
            'file/BCK-' . $no_invoice . '-invoice.pdf'
        );

        foreach ($path as $r) {
            unlink($r);
        }
    }


    function pdf_payment_receipt()
    {
        ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
        //$html = $this->load->view('pdf_report', $data, true); // render the view into HTML

        $payment_id = $this->session->userdata('payment_id');
        $no_invoice = $this->no_invoice($payment_id);
        $filename = 'BCK-' . $no_invoice . '-payment_receipt.pdf';
        unlink('file/' . $filename);
        $pdfFilePath = FCPATH . "/file/$filename";
        $data['booking'] = $this->db->where('payment_id', $payment_id)->get('tb_booking')->row();
        $data['config'] = $this->db->where('config_id', 1)->get('tb_config')->row();

        $date = new DateTime($data['booking']->date_insert);
        $date->modify("+" . $data['config']->config_plus_hour . " hours");
        $data['con'] = $this;
        $data['plus_date'] = $date->format("d F Y H:i:s");
        $data['artikel'] = $this->db->where('artikel_id', $data['booking']->artikel_id)->get('tb_artikel')->row();
        $data['itinerary'] = $this->db->where('artikel_id', $data['booking']->artikel_id_itinerary)->get('tb_artikel')->row();
        $html = $this->load->view('user/pdf_payment_receipt', $data, TRUE);

        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        //$pdf->showImageErrors = true;
        //$pdf->SetFooter(base_url().'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">

        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->SetHTMLFooter('
          <div class="separate-2"></div>
      BOAT CHARTER KOMODO | www.boatcharterkomodo.com | Phone: +62.812399.22222 | WA: +6281236016914
      ');

        $pdf->Output($pdfFilePath, 'F');
    }

    function pdf_invoice()
    {
        ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
        //$html = $this->load->view('pdf_report', $data, true); // render the view into HTML

        $payment_id = $this->session->userdata('payment_id');
        $no_invoice = $this->no_invoice($payment_id);

        $filename = 'BCK-' . $no_invoice . '-invoice.pdf';
        unlink('file/' . $filename);
        $pdfFilePath = FCPATH . "/file/$filename";
        $data['con'] = $this;
        $data['booking'] = $this->db->where('payment_id', $payment_id)->get('tb_booking')->row();
        $data['config'] = $this->db->where('config_id', 1)->get('tb_config')->row();

        $date = new DateTime($data['booking']->date_insert);
        $date->modify("+" . $data['config']->config_plus_hour . " hours");
        $data['plus_date'] = $date->format("d F Y - h:i:s A");
        $data['artikel'] = $this->db->where('artikel_id', $data['booking']->artikel_id)->get('tb_artikel')->row();
        $data['itinerary'] = $this->db->where('artikel_id', $data['booking']->artikel_id_itinerary)->get('tb_artikel')->row();
        $html = $this->load->view('user/pdf_next_payment', $data, TRUE);

        $this->load->library('pdf');
        $pdf = $this->pdf->load();


        $pdf->SetHTMLHeader('
        <table width="100%">
            <tr>
                <td>
                    <img src="' . base_url('assets/img/template/bck-text.png') . '"><br />
                      Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />
                      Phone : +62.81239922222 Whatsapp: +62.81236016914<br/> 
                      sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />
                </td>
                <td>
                   <img src="' . base_url('assets/img/template/BCK.png') . '" width="150">  
                </td>
            </tr>
        </table>
      ');
        $pdf->WriteHTML($html);
        $pdf->SetHTMLFooter('
          <div class="separate-2"></div>
      BOAT CHARTER KOMODO | www.boatcharterkomodo.com | Phone: +62.812399.22222 | WA: +6281236016914
      ');
        // write the HTML into the PDF
        //$pdf->showImageErrors = true;
        //$pdf->SetFooter(base_url().'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">

        $pdf->Output($pdfFilePath, 'F');

        /*      $pdf->SetHTMLHeader('
        <div style="text-align: right; font-weight: bold;">
            My document
        </div>');
              $pdf->SetHTMLFooter('
        <table width="100%">
            <tr>
                <td width="33%">{DATE j-m-Y}</td>
                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right;">My document</td>
            </tr>
        </table>');

              $pdf->WriteHTML('Hello World');

              $pdf->Output();*/
    }

    function pdf_detail_tour()
    {
        ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
        //$html = $this->load->view('pdf_report', $data, true); // render the view into HTML

        $payment_id = $this->session->userdata('payment_id');
        $no_invoice = $this->no_invoice($payment_id);


        $filename = 'BCK-' . $no_invoice . '-detail_tour.pdf';
        unlink('file/' . $filename);
        $pdfFilePath = FCPATH . "/file/$filename";
        $data['con'] = $this;
        $data['booking'] = $this->db->where('payment_id', $payment_id)->get('tb_booking')->row();
        $data['config'] = $this->db->where('config_id', 1)->get('tb_config')->row();

        $date = new DateTime($data['booking']->date_insert);
        $date->modify("+" . $data['config']->config_plus_hour . " hours");
        $data['plus_date'] = $date->format("Y-m-d H:i:s");
        $data['artikel'] = $this->db->where('artikel_id', $data['booking']->artikel_id)->get('tb_artikel')->row();
        $data['artikel']->artikel_remark = $this->remark_list_pdf($data['booking']->artikel_id);
        $data['itinerary'] = $this->db->where('artikel_id', $data['booking']->artikel_id_itinerary)->get('tb_artikel')->row();


        $html = $this->load->view('user/pdf_detail_tour', $data, TRUE);

        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        //$pdf->showImageErrors = true;

        $stylesheet = file_get_contents('assets/css/remark.css'); // external css
        $pdf->WriteHTML($stylesheet, 1);

        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->SetHTMLFooter('
          <div class="separate-2"></div>
      BOAT CHARTER KOMODO | www.boatcharterkomodo.com | Phone: +62.812399.22222 | WA: +6281236016914
      ');
        $pdf->Output($pdfFilePath, 'F');
    }

    function kategori($kategori_id, $page = 1)
    {
        $data = $this->data_primary();

        $kategori = $this->db->join('tb_kategori k', 'k.kategori_id = ks.kategori_id')
            ->where('ks.kategori_id', $kategori_id)->get('tb_kategori_seo ks')->row();

        $data['post'] = $kategori;
        $data['boats'] = $this->db
            ->where(array(
                'link' => 'tour',
                'posisi' => 'artikel'
            ))
            ->get('tb_artikel')
            ->result();
        $this->template->user($data, 'boat_kategori');
    }

    function search_keywords($keyword)
    {
        $data = $this->data_primary('home');
        $keyword = str_replace('%20', ' ', $keyword);

        $this->session->set_userdata(array('keyword' => $keyword));

        if ($keyword == '') {
            redirect();
        }

        $post = $this->mod_web->select_by_id('tb_artikel', 'link', 'home')->row();
        $post->link = 'reservation';
        $data['tipe'] = 'search';

        $data['keyword'] = $keyword;
        $data['post'] = $post;

        $data['tours_search'] = $this->db->join('tb_kategori k', 'k.kategori_id = a.kategori_id', 'left')
            ->where(array(
                'a.posisi' => 'artikel',
            ))
            ->where_in('a.link', array('tour', 'news_footer'))
            ->like('a.artikel_title', $keyword)
            ->or_like('a.meta_title', $keyword)
            ->or_like('a.artikel_isi', $keyword)
            ->or_like('a.meta_title', $keyword)
            ->or_like('a.meta_keywords', $keyword)
            ->order_by('a.artikel_id', 'ASC')
            ->get('tb_artikel a')
            ->result();

        $data['component_search'] = $this->load->view('user/component_search', array(), TRUE);
        $this->template->user_2($data, 'search');
    }

    function data_kalkulasi($tipe = '1')
    {
        $param = 'data_' . $tipe;
        $this->mod_root->$param();
    }

    function open_trips($artikel_id)
    {

        $data = $this->data_primary();

        $data['post'] = $this->db->where(array(
            'link' => 'open_trip',
            'posisi' => 'menu'
        ))
            ->get('tb_artikel')->row();
        $data['post']->link = 'reservation';

        $data['tour_trips'] = $this->db->where(array(
            'link' => 'open_trip',
            'posisi' => 'artikel'
        ))
            ->order_by('artikel_id', 'ASC')
            ->get('tb_artikel')
            ->result();


        $data['component_search'] = $this->load->view('user/component_search', array(), TRUE);
        $this->template->user($data, 'open_trip');
    }

    function boat_destination($destination_id = '')
    {
        $label = '';
        if ($destination_id) {
            $label .= '<div class="label-boat-destination">';
            $label .= '<img src="' . base_url('assets/template/images/google-map.png') . '" width="30"> <strong>Operated</strong> : ';

            $destination_id = json_decode($destination_id, TRUE);
            $destination = $this->db->where_in('id', $destination_id)->get('tb_destination')->result();

            $count_destination = count($destination);
            $no = 1;

            foreach ($destination as $r) {
                if ($no == $count_destination) {
                    $separator = '.';
                } else {
                    $separator = ', ';
                }

                $label .= $r->destination . $separator;
                $no++;
            }

            $label .= '</div>';
        }

        return $label;
    }

    function remark_list($artikel_id)
    {
        $artikel = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $include_id = json_decode($artikel->include_id, TRUE);
        $exclude_id = json_decode($artikel->exclude_id, TRUE);
        $bring_id = json_decode($artikel->bring_id, TRUE);
        $include = array();
        $exclude = array();
        $bring = array();

        if ($include_id) {
            $include = $this->db->where_in('id', $include_id)->get('tb_remark')->result();
        }
        if ($exclude_id) {
            $exclude = $this->db->where_in('id', $exclude_id)->get('tb_remark')->result();
        }
        if ($bring_id) {
            $bring = $this->db->where_in('id', $bring_id)->get('tb_remark')->result();
        }

        $data['include'] = $include;
        $data['exclude'] = $exclude;
        $data['bring'] = $bring;

        return $this->load->view('user/remark_list', $data, TRUE);

    }

    function remark_list_pdf($artikel_id)
    {
        $artikel = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $include_id = json_decode($artikel->include_id, TRUE);
        $exclude_id = json_decode($artikel->exclude_id, TRUE);
        $bring_id = json_decode($artikel->bring_id, TRUE);
        $include = array();
        $exclude = array();
        $bring = array();

        if ($include_id) {
            $include = $this->db->where_in('id', $include_id)->get('tb_remark')->result();
        }
        if ($exclude_id) {
            $exclude = $this->db->where_in('id', $exclude_id)->get('tb_remark')->result();
        }
        if ($bring_id) {
            $bring = $this->db->where_in('id', $bring_id)->get('tb_remark')->result();
        }

        $data['include'] = $include;
        $data['exclude'] = $exclude;
        $data['bring'] = $bring;

        return $this->load->view('user/remark_list_pdf', $data, TRUE);

    }

    function remark_list_email($artikel_id)
    {
        $artikel = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $include_id = json_decode($artikel->include_id, TRUE);
        $exclude_id = json_decode($artikel->exclude_id, TRUE);
        $bring_id = json_decode($artikel->bring_id, TRUE);
        $include = array();
        $exclude = array();
        $bring = array();

        if ($include_id) {
            $include = $this->db->where_in('id', $include_id)->get('tb_remark')->result();
        }
        if ($exclude_id) {
            $exclude = $this->db->where_in('id', $exclude_id)->get('tb_remark')->result();
        }
        if ($bring_id) {
            $bring = $this->db->where_in('id', $bring_id)->get('tb_remark')->result();
        }

        $data['include'] = $include;
        $data['exclude'] = $exclude;
        $data['bring'] = $bring;

        return $this->load->view('user/remark_list_email', $data, TRUE);

    }

    function tour($artikel_id = '')
    {
        $data = $this->data_primary();

        $this->load->helper('lightbox');

        $data['post'] = $this->db
            ->where('a.artikel_id', $artikel_id)
            ->get('tb_artikel a')
            ->row();


        $data['galeri'] = $this->db->where(array('artikel_id' => $artikel_id, 'galeri_tipe' => 'artikel'))->order_by('galeri_id', 'ASC')->get('tb_galeri')->result();

        $data['related_tour'] = $this->db->where(array(
            'a.kategori_id' => $data['post']->kategori_id,
            'a.show' => 'yes',
        ))
            ->where_not_in('a.artikel_id', $artikel_id)
            ->limit($this->webConfig->config_sidebar_boat_num)
            ->order_by('a.artikel_id', 'RANDOM')
            ->get('tb_artikel a')
            ->result();

        $this->template->user($data, 'tour');
    }

    function activities_detail($artikel_id = '')
    {

        $adult = !empty($_GET['adult']) ? $_GET['adult'] : 0;
        $child = !empty($_GET['child']) ? $_GET['child'] : 0;
        $date = !empty($_GET['date']) ? $_GET['date'] : 0;

        if ($adult > 0 || $child > 0) {
            redirect(base_url('activities/book?adult=' . $adult . '&child=' . $child . '&id=' . $artikel_id.'&date='.$date));
        }


        $data = $this->data_primary();

        $this->load->helper('lightbox');

        $data['post'] = $this->db
            ->select('a.*, k.*, ka.kategori_nama AS kategori_activities_nama')
            ->join('tb_kategori k', 'k.kategori_id = a.kategori_id')
            ->join('tb_kategori_activities ka', 'ka.kategori_id = a.kategori_activities_id')
            ->where('a.artikel_id', $artikel_id)
            ->get('tb_artikel a')
            ->row();
        $data['post']->link = 'activities_detail';

        $data['activities_filter'] = $this->load->view('user/activities_filter', [], TRUE);


        $data['galeri'] = $this->db->where(array('artikel_id' => $artikel_id, 'galeri_tipe' => 'artikel'))->order_by('galeri_id', 'ASC')->get('tb_galeri')->result();

        $artikel_view = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row()->artikel_view;
        $artikel_view += 1;

        $this->db->where('artikel_id', $artikel_id)->update('tb_artikel', array('artikel_view' => $artikel_view));


//        $data['related_tour'] = $this->db->where(array(
//            'a.kategori_id' => $data['post']->kategori_id,
//            'a.show' => 'yes',
//        ))
//            ->where_not_in('a.artikel_id', $artikel_id)
//            ->limit($this->webConfig->config_sidebar_boat_num)
//            ->order_by('a.artikel_id', 'RANDOM')
//            ->get('tb_artikel a')
//            ->result();

        $this->template->user($data, 'activities_detail');
    }

    function widget_tour($artikel_id = '')
    {
        $data['find'] = array(' ', '/', '&', '\\');
        $data['replace'] = array('-', '-', '-', '-');

        $data['post'] = $this->db->join('tb_kategori k', 'k.kategori_id = a.kategori_id')
            ->where('a.artikel_id', $artikel_id)
            ->get('tb_artikel a')
            ->row();

        $data['itinerary'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id,
            'publish' => 'yes'
        ))
            ->order_by('artikel_title', 'ASC')
            ->get('tb_artikel')
            ->result();

        $this->load->view('user/widget_tour', $data);
    }

    function widget_tour_calendar($artikel_id = '')
    {
        $data['post'] = $this->db->join('tb_kategori k', 'k.kategori_id = a.kategori_id')
            ->where('a.artikel_id', $artikel_id)
            ->get('tb_artikel a')
            ->row();
        $data['artikel_id'] = $artikel_id;
        $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
            ->get('tb_calendar')
            ->result();


        $tanggal = array();
        foreach ($data['data_list'] as $r) {
            $tanggal[] = $r->date;
        }

        $data['itinerary'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id,
            'publish' => 'yes'
        ))
            ->order_by('artikel_title', 'ASC')
            ->get('tb_artikel')
            ->result();

        $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
        $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);

        $data['date'] = array();
        $no = 0;
        $date_now = date('Y-m-d');
        foreach ($daterange as $date) {
            $date_cal = $date->format('Y-m-d');
            $status = 'available';
            if ($date_cal < $date_now) {
                $status = 'not_available';
            } else {
                $query = $this->db->where('artikel_id', $artikel_id)
                    ->where('date', $date_cal)
                    ->get('tb_calendar');
                if ($query->num_rows() > 0) {
                    $status = 'not_available';
                } else {
                    $itinerary = $data['itinerary'];
                    foreach ($itinerary as $key => $value) {
                        if ($value->artikel_id > 0) {
                            $query = $this->db->where('artikel_id', $value->artikel_id)
                                ->where('date', $date_cal)
                                ->get('tb_calendar');
                            if ($query->num_rows() > 0) {
                                $row = $query->row();
                                if ($row->opentrip == 0)
                                    $status = 'not_available';
                                else
                                    $status = 'open_trip';
                            }
                        }
                    }

                }
            }

            $data['date'][$date_cal]['status'] = $status;
            $no++;
        }

        /*foreach ($daterange as $date) {
          $date_cal = $date->format('Y-m-d');

          if ($date_cal < $date_now) {
            $status = 'not_available';
          } elseif (in_array($date_cal, $tanggal)) {
            $status = 'not_available';
          } else {
            $status = 'available';
          }

          $data['date'][$date_cal]['status'] = $status;
          //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
          //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

          $no++;
        }*/

        $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $data['title'] = $boat->artikel_title;
        $data['list'] = $this->db->where('artikel_id', $artikel_id)
            ->get('tb_calendar')
            ->result();
        $this->load->library('parser');


        $data['find'] = array(' ', '/', '&', '\\');
        $data['replace'] = array('-', '-', '-', '-');

        $data['post'] = $this->db->join('tb_kategori k', 'k.kategori_id = a.kategori_id')
            ->where('a.artikel_id', $artikel_id)
            ->get('tb_artikel a')
            ->row();

        $data['itinerary'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id,
            'publish' => 'yes'
        ))
            ->order_by('artikel_title', 'ASC')
            ->get('tb_artikel')
            ->result();


        $this->load->view('user/widget_boat_calendar', $data);
    }

    function boat_detail_price($artikel_id)
    {
        $data['artikel'] = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $data['payment'] = $this->db->where('artikel_id', $artikel_id)->order_by('count_adult', 'ASC')->get('tb_payment')->result();
        $data['config'] = $this->db->where('config_id', '1')->get('tb_config')->row();
        $table = $this->load->view('user/boat_detail_price', $data, TRUE);
        echo json_encode(array(
            'table' => $table
        ));
    }

    function open_trip_detail($artikel_id = '')
    {
        $data = $this->data_primary();

        $this->load->helper('lightbox');

        $data['post'] = $this->db->where('a.artikel_id', $artikel_id)
            ->get('tb_artikel a')
            ->row();


        $package_tour = $this->db->where('artikel_id', $data['post']->open_trip_artikel_id)->get('tb_artikel')->row();

        $data['post']->artikel_remark = $package_tour->artikel_remark;
        $data['post']->artikel_remark = $this->remark_list($artikel_id);


        $data['galeri'] = $this->db->where(array('artikel_id' => $data['post']->open_trip_artikel_id, 'galeri_tipe' => 'artikel'))->get('tb_galeri')->result();

        $data['related_tour'] = $this->db->where(array(
            'link' => 'open_trip',
            'posisi' => 'artikel'
        ))
            ->where_not_in('a.artikel_id', $artikel_id)
            ->order_by('a.artikel_id', 'ASC')
            ->get('tb_artikel a')
            ->result();

        $data['component_search'] = $this->load->view('user/component_search', array(), TRUE);
        $this->template->user_2($data, 'open_trip_detail');
    }

    function comment_2()
    {

        $this->load->helper('date');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('komen', 'Komentar', 'trim|required');


        if ($this->form_validation->run() === FALSE) {

            echo validation_errors('- ', ' ');
        } else {

            $id = $this->mod_web->increment_id('tb_customer', 'customer_id');

            $data = array(
                'customer_id' => $id,
                'artikel_id' => $this->input->post('id'),
                'customer_nama' => $this->input->post('nama'),
                'customer_email' => $this->input->post('email'),
                'customer_website' => $this->input->post('website'),
                'customer_komen' => $this->input->post('komen'),
                'date' => mdate("%d %F %Y, %H:%m:%s", time() - 60 * 60 * 2),
                'customer_publish' => 'no'
            );

            $message = "

Nama Halaman Website : " . $this->input->post('link') . "

Nama Pengunjung : " . $this->input->post('nama') . "

Website : " . $this->input->post('website') . "

Komentar : " . $this->input->post('komen') . "



Web Manager : " . base_url() . "security" . "

";

            $this->send_mail('Komentar ' . $this->input->post('artikel'), $this->input->post('email'), $message);

            $this->mod_web->insert('tb_customer', $data);

            echo '

Terima Kasih :)

Komentar anda akan kami lihat dan tampilkan nanti, Terima Kasih :)';
        }
    }

    function inquiry1_send()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('guest_name', 'Guest Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');

        if ($this->form_validation->run() === FALSE) {
            $alert = 'Please Completly the Form';
            echo json_encode(array(
                'status' => 'error',
                'alert' => $alert,
                'guest_name' => form_error('guest_name'),
                'email' => form_error('email'),
                'subject' => form_error('subject'),
                'message' => form_error('message'),
            ));
        } else {
            if ($this->input->post('captcha') === $this->session->userdata('captcha_mwz')) {

                $message = "
Dear Boat Charter Komodo,<br /><br />

I have browsed your website and I would like to request<br />
" . $this->input->post('boat_name') . ". My request details as follow :<br /><br />

--------------- PERSONAL DATA ---------------<br />
Full Name : " . $this->input->post('guest_name') . "<br />
Email : " . $this->input->post('email') . "<br />
Boat Name : " . $this->input->post('boat_name') . "<br />
Subject : " . $this->input->post('subject') . "<br /><br />


--------------- DETAIL INQUIRY ---------------<br />
Message : " . $this->input->post('message') . "<br /><br />

From: " . $this->input->post('guest_name') . "<br />
Inquiry from : " . $this->input->post('boat_url') . "
";

                $this->send_mail('Inquiry - ' . $this->input->post('boat_name'), $this->input->post('email'), $message);

                $alert = 'Thanks for your inquiry - ' . $this->input->post('guest_name');

                echo json_encode(array('status' => 'success', 'alert' => $alert));
            } else {
                echo json_encode(array(
                    'status' => 'error',
                    'alert' => 'Invalid security code.',
                    'captcha' => form_error('captcha`'),
                ));
            }
        }


    }

    function widget($artikel_id)
    {

        $data['artikel_id'] = $artikel_id;
        $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
            ->get('tb_calendar')
            ->result();


        $tanggal = array();
        foreach ($data['data_list'] as $r) {
            $tanggal[] = $r->date;
        }

        $data['itinerary'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id,
            'publish' => 'yes'
        ))
            ->order_by('artikel_title', 'ASC')
            ->get('tb_artikel')
            ->result();

        $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
        $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);

        $data['date'] = array();
        $no = 0;
        $date_now = date('Y-m-d');
        foreach ($daterange as $date) {
            $date_cal = $date->format('Y-m-d');
            $status = 'available';
            if ($date_cal < $date_now) {
                $status = 'not_available';
            } else {
                $query = $this->db->where('artikel_id', $artikel_id)
                    ->where('date', $date_cal)
                    ->get('tb_calendar');
                if ($query->num_rows() > 0) {
                    $status = 'not_available';
                } else {
                    $itinerary = $data['itinerary'];
                    foreach ($itinerary as $key => $value) {
                        if ($value->artikel_id > 0) {
                            $query = $this->db->where('artikel_id', $value->artikel_id)
                                ->where('date', $date_cal)
                                ->get('tb_calendar');
                            if ($query->num_rows() > 0) {
                                $row = $query->row();
                                if ($row->opentrip == 0)
                                    $status = 'not_available';
                                else
                                    $status = 'open_trip';
                            }
                        }
                    }

                }
            }

            $data['date'][$date_cal]['status'] = $status;
            $no++;
        }

        /*foreach ($daterange as $date) {
          $date_cal = $date->format('Y-m-d');

          if ($date_cal < $date_now) {
            $status = 'not_available';
          } elseif (in_array($date_cal, $tanggal)) {
            $status = 'not_available';
          } else {
            $status = 'available';
          }

          $data['date'][$date_cal]['status'] = $status;
          //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
          //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

          $no++;
        }*/

        $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
        $data['title'] = $boat->artikel_title;
        $data['list'] = $this->db->where('artikel_id', $artikel_id)
            ->get('tb_calendar')
            ->result();
        $this->load->library('parser');
        echo $this->parser->parse('user/widget_calendar', $data);
        exit();
    }

}
