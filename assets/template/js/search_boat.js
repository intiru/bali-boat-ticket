$(document).ready(function () {
    var way = $('#data-value').data('way');
    var top = parseInt($(".content-wrap").offset().top) - parseInt(40);
    var base_url = $('#base-value').data('base-url');

    $('html, body').animate({
        scrollTop: top
    }, 1000);

    $('.btn-book-departure').click(function () {

        $('.btn-book-departure').text('select');
        $(this).html('<i class="icon-ok"></i> selected');

        var way = $('#data-value').data('way');
        var artikel_id = $(this).data('artikel-id');
        var harga = $(this).data('harga');
        var harga_id = $(this).data('harga-id');
        var base_url = $('#base-value').data('base-url');

        $('#data-value').data('artikel-id-departure', artikel_id);
        $('#data-value').data('harga-id-departure', harga_id);
        $('#data-value').data('harga-departure', harga);

        $.ajax({
            url: base_url+"nias/select_boat",
            type: "post",
            data: {
                type: "departure",
                artikel_id: artikel_id,
                harga_id: harga_id
            },
            success: function() {
                //window.location.href = "<?php echo base_url("reservation-boat") ?>";
            }
        });

        if(way == 'one') {
            var base_url = $('#base-value').data('base-url');
            window.location.href = base_url+"reservation-boat";
        }

        $('.wrapper-button-book').addClass('hidden');

        $('.wrapper-disable-destination').each(function(i, obj) {
            var artikel_id_destination = $(this).data('artikel-id');

            $(this).removeClass('disable-destination');

            if(artikel_id !== artikel_id_destination) {
                $(this).addClass('disable-destination');
            }
        });

        //window.location.href = "<?php echo base_url("reservation-boat") ?>";
    });

    $('.btn-book-destination').click(function () {
        var way = $('#data-value').data('way');
        var artikel_id = $(this).data('artikel-id');
        var harga_id_destination = $(this).data('harga-id');
        var harga_destination = $(this).data('harga');
        var artikel_id_departure = $('#data-value').data('artikel-id-departure');
        var harga_departure = $('#data-value').data('harga-departure');
        var harga_id_departure = $('#base-value').data('harga-id-departure');

        if(typeof artikel_id_departure == "undefined") {
            Swal.fire({
                title: "Attention",
                text: 'Please select boat departure first',
                type: "warning"
            });
            return false;
        }

        var total_harga = parseInt(harga_departure) + parseInt(harga_destination);

        $('#data-value').data('artikel-id-destination', artikel_id);
        $('#data-value').data('harga-id-destination', harga_id_destination);
        $('.total-harga').html(total_harga)

        $('.wrapper-button-book').addClass('hidden');

        $('.wrapper-button-book[data-harga-id="'+harga_id_destination+'"]').removeClass('hidden');

        $.ajax({
            url: base_url+"nias/select_boat",
            type: "post",
            data: {
                type: "destination",
                artikel_id: artikel_id,
                harga_id: harga_id_destination
            },
            success: function() {

            }
        });
    });

    $('.btn-book-now').click(function() {
        window.location.href = base_url+"reservation-boat";
    });

    $("div.holder-departure").jPages({
        containerID: "list-departure",
        perPage: 3,
        startPage: 1,
        startRange: 1,
        midRange: 5,
        endRange: 1
    });
    $("div.holder-destination").jPages({
        containerID: "list-destination",
        perPage: 3,
        startPage: 1,
        startRange: 1,
        midRange: 5,
        endRange: 1
    });

    $('[name="filter-departure"], [name="filter-destination"]').change(function() {
        var value = $(this).val();
        var params = $(this).data('params');
        $.ajax({
            url: base_url+"nias/filter_search_sess",
            type: "post",
            data: {
                params: params,
                value: value
            },
            success: function() {
                window.location.reload();
            }
        })
    });

});