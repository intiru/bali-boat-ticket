$(document).ready(function (e) {
  $('.carousel').carousel();

  var base_url = $('#base-value').data('base-url');

  $("#datepicker-example7-end").focusout(function (e) {
    var base_url = $("#base_url").attr("title");
    $.ajax({
      url: base_url + 'shop/date_range',
      type: 'POST',
      data: {check_in: $("#datepicker-example7-start").val(), check_out: $("#datepicker-example7-end").val()},
      success: function (data) {
        $("#night").val(data);
      }
    });
    return false;

  });

  $("#testimonial").submit(function (e) {
    e.preventDefault();
    vardata = $(this).serialize();
    $.ajax({
      url: $(this).attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {

      },
      success: function (data) {
        var json = JSON.parse(data);
        var ke = "ke";
        if (json.status == 'error') {
          alert(json.alert);
          $("span.form-error:eq(0)").html(json.name).fadeIn("normal");
          $("span.form-error:eq(1)").html(json.address).fadeIn("normal");
          $("span.form-error:eq(2)").html(json.email).fadeIn("normal");
          $("span.form-error:eq(3)").html(json.comment).fadeIn("normal");
          $("span.form-error:eq(4)").html(json.captcha).fadeIn("normal");
        } else if (json.status == 'success') {
          alert(json.alert);
          $("input[type='text']").val("");
          $("textarea").val("");
          $("span.form-error").fadeOut("fast");
        }
      }
    });
    return false;
  });

  $("#contact").submit(function (e) {
    e.preventDefault();
    vardata = $(this).serialize();
    $.ajax({
      url: $(this).attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {

      },
      success: function (data) {
        var json = JSON.parse(data);
        var ke = "ke";
        if (json.status == 'error') {
          alert(json.alert);
          $("span.form-error:eq(0)").html(json.name).fadeIn("normal");
          $("span.form-error:eq(1)").html(json.address).fadeIn("normal");
          $("span.form-error:eq(2)").html(json.city).fadeIn("normal");
          $("span.form-error:eq(3)").html(json.phone).fadeIn("normal");
          $("span.form-error:eq(4)").html(json.email).fadeIn("normal");
          $("span.form-error:eq(5)").html(json.message).fadeIn("normal");
          $("span.form-error:eq(6)").html(json.captcha).fadeIn("normal");
        } else if (json.status == 'success') {
          alert(json.alert);
          $("input[type='text']").val("");
          $("textarea").val("");
          $("span.form-error").fadeOut("fast");
        }
      }
    });
    return false;
  });

  $("#ticket").submit(function (e) {
    e.preventDefault();
    vardata = $(this).serialize();
    $.ajax({
      url: $(this).attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {

      },
      success: function (data) {
        var json = JSON.parse(data);
        var ke = "ke";
        if (json.status == 'error') {
          alert(json.alert);
          $("span.form-error:eq(0)").html(json.title).fadeIn("normal");
          $("span.form-error:eq(1)").html(json.f_name).fadeIn("normal");
          $("span.form-error:eq(2)").html(json.l_name).fadeIn("normal");
          $("span.form-error:eq(3)").html(json.email).fadeIn("normal");
          $("span.form-error:eq(4)").html(json.phone).fadeIn("normal");
          $("span.form-error:eq(5)").html(json.address).fadeIn("normal");
          $("span.form-error:eq(6)").html(json.code).fadeIn("normal");
          $("span.form-error:eq(7)").html(json.fax).fadeIn("normal");
          $("span.form-error:eq(8)").html(json.captcha).fadeIn("normal");
        } else if (json.status == 'success') {
          alert(json.alert);
          $("input[type='text']").val("");
          $("textarea").val("");
          $("span.form-error").fadeOut("fast");
        }
      }
    });
    return false;
  });

  $("#reservation").submit(function (e) {
    e.preventDefault();
    vardata = $(this).serialize();
    $.ajax({
      url: $(this).attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {

      },
      success: function (data) {
        var json = JSON.parse(data);
        var ke = "ke";
        if (json.status == 'error') {
          alert(json.alert);
          $("span.form-error:eq(0)").html(json.title).fadeIn("normal");
          $("span.form-error:eq(1)").html(json.f_name).fadeIn("normal");
          $("span.form-error:eq(2)").html(json.l_name).fadeIn("normal");
          $("span.form-error:eq(3)").html(json.email).fadeIn("normal");
          $("span.form-error:eq(4)").html(json.phone).fadeIn("normal");
          $("span.form-error:eq(5)").html(json.address).fadeIn("normal");
          $("span.form-error:eq(6)").html(json.city).fadeIn("normal");
          $("span.form-error:eq(7)").html(json.country).fadeIn("normal");
          $("span.form-error:eq(8)").html(json.code).fadeIn("normal");
          $("span.form-error:eq(9)").html(json.date_in).fadeIn("normal");
          $("span.form-error:eq(10)").html(json.adult).fadeIn("normal");
          $("span.form-error:eq(11)").html(json.children).fadeIn("normal");
          $("span.form-error:eq(12)").html(json.comment).fadeIn("normal");
          $("span.form-error:eq(13)").html(json.captcha).fadeIn("normal");
        } else if (json.status == 'success') {
          
          if(json.type == 'open_trip') {
            alert(json.alert);
            $("input[type='text']").val("");
            $("textarea").val("");
            $("span.form-error").fadeOut("fast");
          } else {
            var uri_2 = $('[name="uri_2"]').val();
            var uri_3 = $('[name="uri_3"]').val();

            window.location.href = base_url + 'payment/'+uri_2+'/'+uri_3;
          }
        }
      }
    });
    return false;
  });

});

function susu()
{
  var base_url = $("#base_url").attr('title');
  var kategori_id = $(".destinasi").val();
  $.ajax({
    url: base_url + 'shop/sub_area/' + kategori_id,
    success: function (data) {
      $(".sub_area").html(data);
      var sub_area = $(".sub_area").val();

      $.ajax({
        url: base_url + 'shop/nama_hotel/' + sub_area,
        success: function (data_2) {
          $(".nama_hotel_reservation").html(data_2);
        }
      });
    }
  });
}

function susu_2()
{
  var base_url = $("#base_url").attr('title');
  var sub_id = $(".sub_area").val();

  $.ajax({
    url: base_url + 'shop/nama_hotel/' + sub_id,
    success: function (data_2) {
      $(".nama_hotel_reservation").html(data_2);
    }
  });
}