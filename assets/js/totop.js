$(document).ready(function(e) {
	$(window).scroll(function () {
		if ($(this).scrollTop() > 220) {
			$("#totop").fadeIn("fast");
		} else {
			$("#totop").fadeOut("fast");
		}
	});
	$("#totop").click(function(e) {
        $("html, body").animate({ scrollTop: 0 }, "fast");
    });
});