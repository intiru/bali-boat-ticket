$(document).ready(function(e) {
	
	var l = window.location;
	var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
	
    $("#cart_add_satu").submit(function(e){	
		var produk_id   = $(".produk_id").val();
		var produk_jum  = $(".produk_jum").val();
		var base = $(this).attr("class");
		vardata = $("#cart_add_satu").serialize();
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: vardata,
            beforeSend:function(){
                $("#loading-satu").html('<img src="'+base+'/assets/img/root/loading_small.gif">').fadeIn("normal");
            },
            success:function(data){
                var json = JSON.parse(data);
                setTimeout(function(){
                    $("#loading-satu").fadeOut("slow");
                    alert(json.info);
			        $(".total_items").html(json.total_items);
                }, 1500);
            }
        });
		
		return false;
    });
	
	$("#destroy").click(function(e) {
		var conf = confirm('Yakin kosongkan keranjang belanja ?');
		if(conf === true)
        {
        	$.get(base_url+'/keranjang-belanja/hapus-semua', function(){
				window.location.reload(true);
			});
		}
    });
	
});

function direct(ke) {
	window.location.href = ke;
}

function cart_add(base, identitas) {
	
	$.post(base+'cart_add', {id : identitas}).done( function(data){
		var json = JSON.parse(data);
		$(".loading"+identitas).html('<img src="'+base+'/assets/img/root/loading_small.gif">').fadeIn("normal");
		setTimeout(function(){	
			$(".loading"+identitas).fadeOut("slow");
			alert(json.info);
			$(".total_items").html(json.total_items);
		}, 1500);
	});
}