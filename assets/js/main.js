$(document).ready(function (e) {
  var base_root = "www";

  $("#login").submit(function (e) {
    e.preventDefault();
    var base_url = $('#base_url').data('base-url');
    vardata = $("#login").serialize();
    $.ajax({
      url: $("#login").attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {
        $("#loading").html('<img src="' + base_url + '/assets/img/root/loading_small.gif">').fadeIn("normal");
      },
      success: function (data) {
        var json = JSON.parse(data);
        if (json.status == 'error') {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#error").fadeIn("fast");
            $("#username").html(json.username).fadeIn("fast");
            $("#password").html(json.password).fadeIn("fast");
            setTimeout(function () {
              $("#error").fadeOut("fast");
              setTimeout(function () {
                $("#username").fadeOut("fast").html("");
                $("#password").fadeOut("fast").html("");
              }, 10000);
            }, 1000);
          }, 2000);
        }
        else if (json.status == 'kosong') {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#error").fadeIn("fast");
            $("#username").html(json.message).fadeIn("fast");
            setTimeout(function () {
              $("#error").fadeOut("fast");
              setTimeout(function () {
                $("#username").fadeOut("fast").html("");
              }, 10000);
            }, 1000);
          }, 2000);
        }
        else
        {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#success").fadeIn("normal");
            $("input[type='text']").val("");
            $("input[type='password']").val("");
            setTimeout(function () {
              $("#success").fadeOut("normal");
              setTimeout(function () {
                window.location.href = base_url + base_root;
              }, 1000);
            }, 1000);
          }, 2000);
        }
      }
    });
    return false;
  });

  $("#admin").submit(function (e) {
    e.preventDefault();
    vardata = $("#admin").serialize();
    $.ajax({
      url: $("#admin").attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
        $("#loading").html('<img src="' + base_url + '/assets/img/root/loading_small.gif">').fadeIn("normal");
      },
      success: function (data) {
        var json = JSON.parse(data);
        var ke = "ke";
        if (json.status == 'error') {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#error").fadeIn("fast");
            $("span#form_error:eq(0)").html(json.username).fadeIn("normal");
            $("span#form_error:eq(1)").html(json.password).fadeIn("normal");
            $("span#form_error:eq(2)").html(json.password_conf).fadeIn("normal");
            $("span#form_error:eq(3)").html(json.email).fadeIn("normal");
          }, 1000);
          setTimeout(function () {
            $("#error").fadeOut("fast");
          }, 3000);
          setTimeout(function () {
            $("span#form_error").fadeOut("fast");
          }, 12000);
        } else if (json.status == 'success') {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#success").fadeIn("normal");
            $("input[type='text']").val("");
            $("input[type='password']").val("");
            setTimeout(function () {
              $("span#form_error").fadeOut("fast");
            }, 1000);
          }, 2000);
          setTimeout(function () {
            $("#success").fadeOut("normal");
            window.location.href = $("#admin").attr("title");
          }, 3000);
        }
      }
    });
    return false;
  });

  $("#email").submit(function (e) {
    e.preventDefault();
    vardata = $("#email").serialize();
    $.ajax({
      url: $("#email").attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
        $("#loading").html('<img src="' + base_url + '/assets/img/root/loading_small.gif">').fadeIn("normal");
      },
      success: function (data) {
        var json = JSON.parse(data);
        if (json.status == 'error') {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#error").fadeIn("fast");
            $("span#form_error:eq(0)").html(json.email).fadeIn("normal");
          }, 1000);
          setTimeout(function () {
            $("#error").fadeOut("fast");
          }, 3000);
          setTimeout(function () {
            $("span#form_error").fadeOut("fast");
          }, 12000);
        } else if (json.status == 'success') {
          setTimeout(function () {
            $("#loading").fadeOut("fast");
            $("#success").fadeIn("normal");
            $("input[type='text']").val("");
            $("input[type='password']").val("");
            setTimeout(function () {
              $("span#form_error").fadeOut("fast");
            }, 1000);
          }, 2000);
          setTimeout(function () {
            $("#success").fadeOut("normal");
            window.location.href = $("#email").attr("title");
          }, 3000);
        }
      }
    });
    return false;
  });



  $("#insert").submit(function (e) {
    if (!$("#insert").hasClass("normal")) {
      tinyMCE.triggerSave();
    }
    vardata = $("#insert").serialize();
    $.ajax({
      url: $("#insert").attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
        $("#loading").html('<img src="' + base_url + '/assets/img/root/loading_small.gif">').fadeIn("fast");
      },
      success: function () {
        setTimeout(function () {
          $("#loading").hide();
          $("input[type='text']").val("");
          $("input[type='password']").val("");
          $("textarea").val("");
          if (!$("#insert").hasClass("normal")) {
            tinyMCE.activeEditor.setContent('');
          }
          $("#success").fadeIn("fast");
          setTimeout(function () {
            $("#success").fadeOut("fast");
            window.location.href = $("#insert").attr("title");
          }, 1000);
        }, 2000);
      }
    });
    return false;
  });

  $("#update").submit(function (e) {
    if (!$("#update").hasClass("normal")) {
      tinyMCE.triggerSave();
    }
    vardata = $("#update").serialize();
    $.ajax({
      url: $("#update").attr("action"),
      type: 'POST',
      data: vardata,
      beforeSend: function () {
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
        $("#loading").html('<img src="' + base_url + '/assets/img/root/loading_small.gif">').fadeIn("fast");
      },
      success: function () {
        setTimeout(function () {
          $("#loading").hide();
          $("#success").fadeIn("fast");
          setTimeout(function () {
            $("#success").fadeOut("fast");
            window.location.href = $("#update").attr("title");
          }, 1000);
        }, 2000);
      }
    });
    return false;
  });

  $("#insert-file").click(function (e) {
    $("#insert-file").ajaxForm(options_insert);
  });

  $("#update-file").click(function (e) {
    $("#update-file").ajaxForm(options_update);
  });

  $("#profil-data").click(function (e) {
    $("#profil-data").ajaxForm(setting_profile);
  });

  $("#only_number").keydown(function (event) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
      // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
          (event.keyCode >= 35 && event.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
      }
      else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
          event.preventDefault();
        }
      }
    });

  $("#reset").click(function (e) {
    $("form").find("input[type=text], textarea, input[type=file]").val("");
  });

  $("#kategori").change(function (e) {
    var kategori_id = $("#kategori option:selected").val();
    var base_url = $("#base_url").attr('title');
    $.ajax({
      url: base_url + 'www/hotel/sub/' + kategori_id,
      success: function (data) {
        $("#sub").html(data);
      }
    });
  });
});

var setting_profile = {
  beforeSend: function () {
    $(".progress-bar-admin-base").show();
    $(".progress-bar-admin-base").width('0%');
    $("#percent").html("0%");
  },
  uploadProgress: function (event, position, total, percentComplete) {
    $(".progress-bar-admin-base").width(percentComplete + '%');
    $("#percent").html(percentComplete + '%');
  },
  complete: function (response) {
    var json = JSON.parse(response.responseText);
    alert(json.message);
    $('.help-block').remove();
    if (json.status == false) {
      $("#loading").fadeOut("fast");
      $('.form-group').removeClass('has-error');
      $.each(json.form, function (key, val) {
        if(val != '') {
          $('input[name="' + key + '"]').parents('.form-group').addClass('has-error');
        }
        $('input[name="' + key + '"]').parents('.input-group').after(val);
        $('input[name="' + key + '"]').parents('.form-group').after(val);
      });
    } else {

      var base_url = $('.base-value').data('base-url');
      $("#loading").fadeOut("fast");
      $("#success").fadeIn("normal");
      window.location.href = $("#profil-data").attr("title");
    }
  }
};

function hapus(ke, row) {
  var conf = confirm("Are you sure to delete ?");
  if (conf == true) {
    $.ajax({
      url: ke,
      success: function () {
        $("#success").fadeIn("fast");
        $('#row' + row).fadeOut("normal", function () {
          $(this).remove();
        });
        setTimeout(function () {
          $("#success").fadeOut("fast");
        }, 2000);
      }
    });
  }
}

function publish(ke, id, value, row) {
  $.ajax({
    url: ke + id + value,
    success: function () {
      $("#success").fadeIn("fast");
      if (value == "/yes") {
        $("#eye" + row).html('<span class="glyphicon glyphicon-eye-open eye-open" onclick="publish(\'testimonial/update/\', \'' + id + '\', \'/no\', \'' + row + '\')"></span></span>');
      } else if (value == "/no") {
        $("#eye" + row).html('<span class="glyphicon glyphicon-eye-close eye-close" onclick="publish(\'testimonial/update/\', \'' + id + '\', \'/yes\', \'' + row + '\')"></span></span>');
      }
      setTimeout(function () {
        $("#success").fadeOut("fast");
      }, 2000);
    }
  });
}

function show(ke, id, value, row) {
  $.ajax({
    url: ke + id + value,
    success: function () {
      $("#success").fadeIn("fast");
      if (value == "/yes") {
        $("#show" + row).html('<span class="glyphicon glyphicon-ok eye-open" title="Publish" data-toggle="tooltip" onclick="show(\'testimonial/update/\', \'' + id + '\', \'/no\', \'' + row + '\')"></span></span>');
      } else if (value == "/no") {
        $("#show" + row).html('<span class="glyphicon glyphicon-remove eye-close" title="Unpublish" data-toggle="tooltip" onclick="show(\'testimonial/update/\', \'' + id + '\', \'/yes\', \'' + row + '\')"></span></span>');
      }
      
      $('[data-toggle="tooltip"]').tooltip(); 
      
      setTimeout(function () {
        $("#success").fadeOut("fast");
      }, 2000);
    }
  });
}

function news_hot(ke, id, value, row) {
  $.ajax({
    url: ke + id + value,
    success: function () {
      $("#success").fadeIn("fast");
      if (value == "/yes") {
        $("#eye" + row).html('<span class="glyphicon glyphicon-eye-open eye-open" onclick="news_hot(\'news/hot/\', \'' + id + '\', \'/no\', \'' + row + '\')"></span></span>');
      } else if (value == "/no") {
        $("#eye" + row).html('<span class="glyphicon glyphicon-eye-close eye-close" onclick="news_hot(\'news/hot/\', \'' + id + '\', \'/yes\', \'' + row + '\')"></span></span>');
      }
      setTimeout(function () {
        $("#success").fadeOut("fast");
      }, 2000);
    }
  });
}

var options_insert = {
  beforeSend: function () {
    $(".progress-bar-admin-base").show();
    $(".progress-bar-admin-base").width('0%');
    $("#percent").html("0%");
  },
  uploadProgress: function (event, position, total, percentComplete) {
    $(".progress-bar-admin-base").width(percentComplete + '%');
    $("#percent").html(percentComplete + '%');
  },
  complete: function (response) {
    var json = JSON.parse(response.responseText);
    if (json.status == 'error') {
      setTimeout(function () {
        $(".progress-bar-admin-base").fadeOut("fast");
        $("#error").fadeIn("fast");
        $("#status-error").html(json.message).fadeIn("fast");
        setTimeout(function () {
          $("#error").fadeOut("fast");
          $(".progress-bar-admin-base").width('0%');
          setTimeout(function () {
            $("#status-error").fadeOut("fast");
          }, 8000);
        }, 2000);
      }, 1000);
    } else if (json.status == 'success') {
      $("form").find("input[type=text], textarea, input[type=file]").val("");
      if (!$("form").hasClass("normal")) {
        tinyMCE.triggerSave();
        tinyMCE.activeEditor.setContent('');
      }
      setTimeout(function () {
        $("#gambar").removeAttr("src");
        $("#gambar").removeAttr("class");
        $(".progress-bar-admin-base").fadeOut("fast");
        $("#success").fadeIn("fast");
        setTimeout(function () {
          $("#success").fadeOut("fast");
          $(".progress-bar-admin-base").width('0%');
          window.location.href = $("#insert-file").attr("title");
        }, 2000);
      }, 2000);
    }
  }
};

var options_update = {
  beforeSend: function () {
    $(".progress-bar-admin-base").show();
    $(".progress-bar-admin-base").width('0%');
    $("#percent").html("0%");
  },
  uploadProgress: function (event, position, total, percentComplete) {
    $(".progress-bar-admin-base").width(percentComplete + '%');
    $("#percent").html(percentComplete + '%');
  },
  complete: function (response) {
    var json = JSON.parse(response.responseText);
    if (json.status == 'error') {
      setTimeout(function () {
        $("form").find("input[type=file]").val("");
        $(".progress-bar-admin-base").hide();
        $("#error").fadeIn("fast");
        $("#status-error").html(json.message).fadeIn("fast");
        setTimeout(function () {
          $("#error").fadeOut("fast");
          setTimeout(function () {
            $("#status-error").fadeOut("fast");
          }, 8000);
        }, 2000);
      }, 1000);
    } else if (json.status == 'success') {
      if (!$("form").hasClass("normal")) {
        tinyMCE.triggerSave();
      }
      setTimeout(function () {
        $(".progress-bar-admin-base").hide();
        $("#success").fadeIn("fast");
        setTimeout(function () {
          $("#success").fadeOut("fast");
          $(".progress-bar-admin-base").width('0%');
          window.location.href = $("#update-file").attr("title");
        }, 1000);
      }, 1000);
    }
  }
};

function read_image(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#gambar')
        .attr('src', e.target.result)
        .css('max-width', '250px')
        .addClass('img-thumbnail');
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function read_image_customer(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#gambar')
        .attr('src', e.target.result)
        .css('max-width', '140px');
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function choose_disabled() {
  $("#kategori").attr('disabled', 'disabled');
  $("#remark").fadeOut("fast");
  $("#price").fadeOut("fast");
  $("#images").fadeOut("fast");
  $("#gambar").fadeOut("fast");
}
function choose_enabled() {
  $("#kategori").removeAttr('disabled');
  $("#remark").fadeIn("fast");
  $("#price").fadeIn("fast");
  $("#images").fadeIn("fast");
  $("#gambar").fadeIn("fast");
}

function sub_kategori(kategori_id)
{
  var base_url = $("#base_url").attr('title');
  $.ajax({
    url: base_url + 'www/hotel/sub/' + kategori_id,
    success: function (data) {
      $("#sub").html(data);
    }
  });
}
